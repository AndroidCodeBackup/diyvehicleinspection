package idsoft.inspectiondepot.vehicleinspection;

import com.parse.Parse;
import com.parse.ParseACL;

import com.parse.ParseUser;

import android.app.Application;

public class ParseApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		// Add your initialization code here
		
        Parse.initialize(this, "bPATR7MUQNk8x61ssKg4etb7xZUsFFHZnZmqF1Yn", "bWOgIMz7Zin6W4o8VrzaDYnfwX7R0b90CtfcpdOl");
		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
	    
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);
		
		ParseACL.setDefaultACL(defaultACL, true);
	}

}

