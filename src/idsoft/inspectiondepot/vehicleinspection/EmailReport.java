package idsoft.inspectiondepot.vehicleinspection;

import idsoft.inspectiondepot.vehicleinspection.supportclass.CommonFunction;

import idsoft.inspectiondepot.vehicleinspection.supportclass.CustomTextWatcher;
import idsoft.inspectiondepot.vehicleinspection.supportclass.DataBaseHelper;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.concurrent.TimeoutException;

import org.xmlpull.v1.XmlPullParserException;

import android.R.string;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EmailReport extends Activity {
	CheckBox ckcustomer, ckother, ckattachreport;
	EditText etcustomer, etother, etto, etsubject, etbody;
	CommonFunction cf;
	DataBaseHelper db;
	String UserName,UserId,UserEmail, srid, chklogin, path,to,ownersname;
	boolean report;
	int show_handler;
	LinearLayout llreport;
	ProgressDialog pd,pk;
	TextView tvpercentage;  
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState); 
		setContentView(R.layout.email_report);
		cf = new CommonFunction(this);
		db = new DataBaseHelper(this);
		
		ckcustomer = (CheckBox) findViewById(R.id.e_report2_ckcustomermailid);
		ckother = (CheckBox) findViewById(R.id.e_report2_ckothermailid);
		ckattachreport = (CheckBox) findViewById(R.id.e_report2_ckattachreport);
		etcustomer = (EditText) findViewById(R.id.e_report2_etcustomermail);
		etother = (EditText) findViewById(R.id.e_report2_etothermail);
		etto = (EditText) findViewById(R.id.e_report2_etto);
		etsubject = (EditText) findViewById(R.id.e_report2_etsubject);
		etbody = (EditText) findViewById(R.id.e_report2_etbody);
		llreport = (LinearLayout) findViewById(R.id.e_report2_llreport);
		tvpercentage=(TextView)findViewById(R.id.e_report2_tvpercentage);
		
		Intent intent=getIntent();
		Bundle bundle=intent.getExtras();
		ownersname=bundle.getString("ownersname");
		
			etbody.setText("DIY Vehicle Inspection Report for "+ownersname);
			etsubject.setText("Re:DIY Vehicle Inspection Report");
			llreport.setVisibility(View.VISIBLE);
			path=OnlineList.reportpath;
			System.out.println("The path is "+path);
		
		

		db.CreateTable(1);
		Cursor cur = db.vi_db.rawQuery("select * from " + db.Registration,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			UserName = cf.decode(cur.getString(cur.getColumnIndex("firstname")));
			UserName += " "+cf.decode(cur.getString(cur.getColumnIndex("lastname")));
			UserId = cf.decode(cur.getString(cur.getColumnIndex("userid")));
			UserEmail = cf.decode(cf.decode(cur.getString(cur
					.getColumnIndex("email"))));
	
		}
		cur.close();

		//etcustomer.setText(mailid);
	
		cf.Device_Information();

		ckcustomer.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				String customermailid=etcustomer.getText().toString();
				if (isChecked) {
					if(cf.eMailValidation(customermailid))
					{
						etto.append(customermailid + ",");
						etcustomer.setEnabled(false);
					}
					else
					{
						cf.show_toast("Customer Mail Id is in invalid format",1);
//						etcustomer.setText("");
						etcustomer.requestFocus();
						ckcustomer.setChecked(false);
					}
				} else {
					String agency=etto.getText().toString();
					String customer=etcustomer.getText().toString();
					if(agency.contains(customer+","))
					{
						agency=agency.replace(customer+",", "");
						etto.setText(agency);
						etcustomer.setEnabled(true);
					}
				}

			}
		});
		
		ckother.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub
				if (isChecked) {
					if(etother.getText().toString().trim().equals(""))
					{
						cf.show_toast("Please enter Mail Id",1);
//						etother.setText("");
						etother.requestFocus();
						ckother.setChecked(false);
					}
					else
					{
						String othermailid=etother.getText().toString();
						if(othermailid.contains(","))
						{
							String[] splitvalue=othermailid.split(",");
							String valid="";
							for(int i=0;i<splitvalue.length;i++)
							{
								if(cf.eMailValidation(splitvalue[i]))
								{
									valid +="true";
								}
								else
								{
									valid +="false";
								}
							}
							if(valid.contains("false"))
							{
								cf.show_toast("Mail Id is in invalid format",1);
//								etother.setText("");
								etother.requestFocus();
								ckother.setChecked(false);
							}
							else
							{
								etto.append(etother.getText().toString() + ",");
								etother.setEnabled(false);
							}
						}
						else
						{
							if(cf.eMailValidation(etother.getText().toString()))
							{
								etto.append(etother.getText().toString() + ",");
								etother.setEnabled(false);
							}
							else
							{
								cf.show_toast("Mail Id is in invalid format",1);
//								etother.setText("");
								etother.requestFocus();
								ckother.setChecked(false);
							}
						}
					}
				} else {
					String agency=etto.getText().toString();
					String other=etother.getText().toString();
					if(agency.contains(other+","))
					{
						agency=agency.replace(other+",", "");
						etto.setText(agency);
						etother.setEnabled(true);
					}
				}

			}
		});
		
		etcustomer.addTextChangedListener(new CustomTextWatcher(etcustomer));
		etother.addTextChangedListener(new CustomTextWatcher(etother));
		etsubject.addTextChangedListener(new CustomTextWatcher(etsubject));
		
		etbody.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if (etbody.getText().toString().startsWith(" "))
		        {
		            // Not allowed
		        	etbody.setText("");
		        }
				int length=etbody.getText().toString().length();
				if(length==0)
				{
					tvpercentage.setText("0%");
				}
				else
				{
					/*length=length/2;
					tvpercentage.setText(String.valueOf(length)+"%");*/
					int percent=(int) ((length)*(100.00/Double.parseDouble("300")));
				    if(length==Integer.parseInt("300"))
				    {
				    	tvpercentage.setText(Html.fromHtml("   "+percent+" % Exceed limit"));
				    	tvpercentage.setTextColor(Color.RED);
				    }
				    else
				    {
				    	tvpercentage.setText("   "+percent+" %");
				    	tvpercentage.setTextColor(Color.parseColor("#dddddd"));
				    }
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	public void clicker(View v) {
		switch (v.getId()) {
		
		case R.id.e_report2_btnsend:
			Check_Validation();
			break;

		case R.id.e_report2_btncancel:
			
				Intent intent=new Intent(EmailReport.this,OnlineList.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				startActivity(intent);
				finish();
			
			
			
			break;

		case R.id.e_report2_btnviewreport:
			String[] filenamesplit = path.split("/");
			final String filename = filenamesplit[filenamesplit.length - 1];
			System.out.println("The File Name is " + filename);
			File sdDir = new File(Environment.getExternalStorageDirectory()
					.getPath());
			File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
					+ filename);
			if(file.exists())
			{
				View_Pdf_File(filename);
			}
			else
			{
				if (cf.isInternetOn() == true) {
					cf.show_ProgressDialog("Downloading... Please wait.");
					new Thread() {
						public void run() {
							Looper.prepare();
							try {
								String extStorageDirectory = Environment
										.getExternalStorageDirectory()
										.toString();
								File folder = new File(
										extStorageDirectory,
										"DownloadedPdfFile");
								folder.mkdir();
								File file = new File(folder,
										filename);
								try {
									file.createNewFile();
									Downloader.DownloadFile(path,
											file);
								} catch (IOException e1) {
									e1.printStackTrace();
								}

								show_handler = 2;
								handler.sendEmptyMessage(0);

							} catch (Exception e) {
								// TODO Auto-generated catch block
								System.out.println("The error is "
										+ e.getMessage());
								e.printStackTrace();
								show_handler = 1;
								handler.sendEmptyMessage(0);

							}
						}

						private Handler handler = new Handler() {
							@Override
							public void handleMessage(Message msg) {
								pd.dismiss();
								// dialog1.dismiss();
								if (show_handler == 1) {
									show_handler = 0;
									cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

								} else if (show_handler == 2) {
									show_handler = 0;
	                              	View_Pdf_File(filename);

								}
							}
						};
					}.start();
				} else {
					cf.show_toast("Internet connection not available",1);

				}
			}
			break;

		case R.id.e_report2_toclear:
			ckcustomer.setChecked(false);
			ckother.setChecked(false);
			etto.setText("");
			break;

		case R.id.e_report2_home:
			Intent intenthome = new Intent(EmailReport.this, HomeScreen.class);
			startActivity(intenthome);
			finish();
			break;

		}
	}
	
	private void View_Pdf_File(String filename)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(EmailReport.this,
					ViewPdfFile.class);
			intentview.putExtra("path", this.path);
			startActivity(intentview);
			// finish();
        }
	}
	
	private void Check_Validation()
	{
		if(!etto.getText().toString().trim().equals(""))
		{
			if(!etsubject.getText().toString().trim().equals(""))
			{
				if(!etbody.getText().toString().equals(""))
				{
					Send_Mail();
				}
				else
				{
					cf.show_toast("Please enter Body",1);
				}
			}
			else
			{
				cf.show_toast("Please enter Subject",1);
			}
		}
		else
		{
			cf.show_toast("Please select Mail Id",1);
		}
	}
	
	private void Send_Mail()
	{
		db.CreateTable(1);
		Cursor cur1 = db.vi_db.rawQuery("select * from " + db.Registration, null);
		cur1.moveToFirst();
		if (cur1.getCount() >= 1) {
			UserId = cf
					.decode(cur1.getString(cur1.getColumnIndex("userid")));
		}
		cur1.close();
		
		to=etto.getText().toString();
		to=to.substring(0,to.length()-1);
		if(!ckattachreport.isChecked())
		{
			path="";
		}
		
			if (cf.isInternetOn() == true) {
				cf.show_ProgressDialog("Sending Mail... ");
				new Thread() {
					String chklogin;
					public void run() {
						Looper.prepare();
						try {
							chklogin = cf
									.Calling_WS_EmailReport(UserId,to,etsubject.getText().toString(),
											etbody.getText().toString(),path,"CUSTOMREPORTSREADYMAIL");
							System.out.println("response EmailReport"
									+ chklogin);
							show_handler = 5;
							handler.sendEmptyMessage(0);
						} catch (SocketException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (NetworkErrorException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (TimeoutException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (XmlPullParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 4;
							handler.sendEmptyMessage(0);

						}

					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							cf.pd.dismiss();
							if (show_handler == 3) {
								show_handler = 0;
								cf.show_toast("There is a problem on your Network. Please try again later with better Network.",1);

							} else if (show_handler == 4) {
								cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

							} else if (show_handler == 5) {
								show_handler = 0;
								
								if(chklogin.toLowerCase().equals("true"))
								{
									Intent intenthome = new Intent(EmailReport.this, HomeScreen.class);
									startActivity(intenthome);
									finish();
									cf.show_toast("Email sent successfully",1);
								}
								else
								{
									Intent intenthome = new Intent(EmailReport.this, HomeScreen.class);
									startActivity(intenthome);
									finish();
									cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);
								}
							}
						}
					};
				}.start();
			} else {
				cf.show_toast("Internet connection not available",1);
			}
	}

	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
			Intent intent=new Intent(EmailReport.this,OnlineList.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
			finish();
		
		
	}

}
