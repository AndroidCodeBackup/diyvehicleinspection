package idsoft.inspectiondepot.vehicleinspection;

import idsoft.inspectiondepot.vehicleinspection.supportclass.CommonFunction;

import idsoft.inspectiondepot.vehicleinspection.supportclass.DataBaseHelper;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;


import android.widget.TextView;

public class Info extends Activity{
	DataBaseHelper db;
	CommonFunction cf;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);
		
		db = new DataBaseHelper(this);
		cf = new CommonFunction(this);
		
		db.CreateTable(1);
		try
		{
			Cursor c = db.vi_db.rawQuery("select * from " + db.Registration,null);
			if(c.getCount()>0)
			{
				c.moveToFirst();
				
				((TextView) findViewById(R.id.info_firstname)).setText(cf.decode(c.getString(c.getColumnIndex("firstname"))));
				((TextView) findViewById(R.id.info_lastname)).setText(cf.decode(c.getString(c.getColumnIndex("lastname"))));
				((TextView) findViewById(R.id.info_phonenumber)).setText(cf.decode(c.getString(c.getColumnIndex("phonenum"))));
				((TextView) findViewById(R.id.info_email)).setText(cf.decode(c.getString(c.getColumnIndex("email"))));
				((TextView) findViewById(R.id.info_username)).setText(cf.decode(c.getString(c.getColumnIndex("username"))));
			}

		}catch (Exception e) {
			// TODO: handle exception
		}
		try {
			String vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			((TextView)findViewById(R.id.info_version)).setText("Version : "+vcode);

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		((TextView)findViewById(R.id.info_url)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(cf.isInternetOn())
				{
					startActivity(new Intent(Info.this,Webview.class));
					finish();
				}
				else
				{
					cf.show_toast("Internet connection not available", 1);
				}
			   
			}

		});
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(Info.this,HomeScreen.class));
			finish();
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
