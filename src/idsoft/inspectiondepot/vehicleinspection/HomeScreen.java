package idsoft.inspectiondepot.vehicleinspection;

import idsoft.inspectiondepot.vehicleinspection.supportclass.CommonFunction;
import idsoft.inspectiondepot.vehicleinspection.supportclass.CustomTextWatcher;
import idsoft.inspectiondepot.vehicleinspection.supportclass.DataBaseHelper;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeScreen extends Activity{
	Dialog dialog1;
	CommonFunction cf;
	DataBaseHelper db;
	String username="", password="", userid="", newversionname="",newversioncode="",versionname="";
	int show_handler,usercheck,total_bar,vcode;
	
	AlertDialog alertDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homescreen);
		cf = new CommonFunction(this);
		db = new DataBaseHelper(this);
		
		vcode = getcurrentversioncode();
		cf.versionname = cf.getdeviceversionname();
		
		
	}
	private int getcurrentversioncode() {
		// TODO Auto-generated method stub

		try {
			vcode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vcode;
	}
	public void clicker(View v)
	{
		switch(v.getId())
		{
		case R.id.homescreen_newvehicleinspection:
			startActivity(new Intent(HomeScreen.this,VehicleInspection.class));
			finish();
			break;
		case R.id.homescreen_completedvehicleinspection:
			startActivity(new Intent(HomeScreen.this,OnlineList.class));
			finish();
			break;
		case R.id.homescreen_resetpassword:
			reset_password();
			break;
		case R.id.homescreen_checkforupdates:
			check_for_updates();
		    break;
		case R.id.homescreen_userinfo:
			startActivity(new Intent(HomeScreen.this,Info.class));
			finish();
			break;
		case R.id.homescreen_logout:
			Delete_All_Tables();
            startActivity(new Intent(HomeScreen.this, LoginActivity.class));
			finish();
			break;
		}
	}
	private void check_for_updates() {
		// TODO Auto-generated method stub
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Checking for Updates..."
					+ " Please wait.</font></b>";
			final ProgressDialog pd = ProgressDialog.show(HomeScreen.this, "",
					Html.fromHtml(source), true);
			new Thread() {

				public void run() {
					Looper.prepare();
					try {
						cf.value = getversioncodefromweb();
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketTimeoutException s) {
						System.out.println("s " + s.getMessage());
						usercheck = 2;
						total_bar = 100;

					} catch (NetworkErrorException n) {
						System.out.println("n " + n.getMessage());
						usercheck = 2;
						total_bar = 100;

					} catch (IOException io) {
						System.out.println("io " + io.getMessage());
						usercheck = 2;
						total_bar = 100;

					} catch (XmlPullParserException x) {
						System.out.println("x " + x.getMessage());
						usercheck = 2;
						total_bar = 100;

					} catch (Exception e) {
						System.out.println("e " + e.getMessage());
						usercheck = 3;
						total_bar = 100;

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.",1);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

						} else if (show_handler == 5) {
							show_handler = 0;

							System.out.println("value is " + cf.value);
							if (cf.value == true) {
								// No update available
								cf.show_toast("No update available",1);
							} else {
								System.out.println("Inside else");
								// update available
								Update_Alert_Market();
							}

						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available",1);

		}
	}
	public boolean getversioncodefromweb() throws NetworkErrorException,
	SocketTimeoutException, IOException, XmlPullParserException,
	Exception {
	SoapObject request = new SoapObject(cf.NAMESPACE,
			"GETCUSTOMEVERSIONINFORMATION");
	SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
			SoapEnvelope.VER11);
	envelope.dotNet = true;
	envelope.setOutputSoapObject(request);
	System.out.println("The request for version information is " + request);
	HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
	androidHttpTransport.call(cf.NAMESPACE + "GETCUSTOMEVERSIONINFORMATION",
			envelope);
	SoapObject result = (SoapObject) envelope.getResponse();
	System.out.println("The result for version information is " + result);
	SoapObject obj = (SoapObject) result.getProperty(0);
	newversioncode = String.valueOf(obj.getProperty("VersionCode"));
	newversionname = String.valueOf(obj.getProperty("VersionName"));
	if (!"null".equals(newversioncode) && null != newversioncode
			&& !newversioncode.equals("")) {
		if (Integer.toString(vcode).equals(newversioncode)) {
							return true;
		} else {
			try {
				db.CreateTable(2);
				Cursor c12 = db.vi_db.rawQuery("select * from " + db.Version,
						null);
	
				if (c12.getCount() < 1) {
					try {
						db.vi_db.execSQL("INSERT INTO " + db.Version
								+ "(VersionCode,VersionName)"
								+ " VALUES ('" + newversioncode + "','"
								+ newversionname + "')");
	
					} catch (Exception e) {
	
					}
	
				} else {
					try {
						db.vi_db.execSQL("UPDATE " + db.Version
								+ " set VersionCode='" + newversioncode
								+ "',VersionName='" + newversionname
								+ "'");
					} catch (Exception e) {
	
					}
	
				}
				return false;
			} catch (Exception e) {
				return true;
	
			}
	
		}
	} else {
		return true;
	}
	
	}
	private void Update_Alert_Market() {
		// TODO Auto-generated method stub
		alertDialog = new AlertDialog.Builder(HomeScreen.this).create();
		alertDialog
				.setMessage("There is a New Version ( "
						+ newversionname
						+ " ) of Agent available. Update now to get the Latest Features and Improvements. ");
		alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						startActivity(new Intent(
								Intent.ACTION_VIEW,
								Uri.parse("https://play.google.com/store/apps/details?id=idsoft.inspectiondepot.vehicleinspection")));
					}
				});
		alertDialog.setButton(Dialog.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub

					}
				});

		alertDialog.show();
	}
	private void reset_password() {
		// TODO Auto-generated method stub

		dialog1 = new Dialog(HomeScreen.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog1.setContentView(R.layout.resetpassword);
		dialog1.setCancelable(false);
		//dialog1.setCanceledOnTouchOutside(true);
		dialog1.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		final EditText oldpassword;
		final EditText newpassword;
		final EditText confirmpassword;
		Button submit, cancel;
		oldpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_oldpassword);
		newpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_newpassword);
		confirmpassword = (EditText) dialog1
				.findViewById(R.id.resetpassword_confirmpassword);

		oldpassword.addTextChangedListener(new CustomTextWatcher(oldpassword));
		newpassword.addTextChangedListener(new CustomTextWatcher(newpassword));
		confirmpassword.addTextChangedListener(new CustomTextWatcher(
				confirmpassword));

		ImageView clo = (ImageView) dialog1
				.findViewById(R.id.resetpassword_close);
		submit = (Button) dialog1.findViewById(R.id.resetpassword_submit);
		cancel = (Button) dialog1.findViewById(R.id.resetpassword_cancel);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (oldpassword.getText().toString().equals("")) {
					cf.show_toast("Please enter Old Password",1);
					oldpassword.requestFocus();
				} else {

					if (newpassword.getText().toString().equals("")) {
						cf.show_toast("Please enter New Password",1);
						newpassword.requestFocus();
					} else {
						
						boolean upperFound = false;
						boolean specialcharacterFound = false;
						boolean numberFound = false;
						
						//Checks for edittext contains a Digit/Number
						String numberchecker=newpassword.getText().toString();
						if(numberchecker.matches(".*\\d.*")){
						    numberFound = true;
						} else{
						    numberFound = false;
						}
						
						//Checks for edittext contains a Special Character
						Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
						Matcher m = p.matcher(newpassword.getText().toString());
						boolean b = m.find();

						if (b)
						{
							specialcharacterFound=true;
						}
						else
						{
							specialcharacterFound=false;
						}
						
						//Checks for edittext contains a Upper Case Character
						String aString = newpassword.getText().toString();
						for (char c : aString.toCharArray()) {
						    if (Character.isUpperCase(c)) {
						        upperFound = true;
						       break;
						    }
						}
						
						if(numberFound&&specialcharacterFound&&upperFound)
						{
							if (confirmpassword.getText().toString().equals("")) {
								cf.show_toast("Please enter Confirm Password",1);
								confirmpassword.requestFocus();
							} else {
								if (newpassword
										.getText()
										.toString()
										.equals(confirmpassword.getText()
												.toString())) {
									db.CreateTable(1);
									Cursor cur = db.vi_db.rawQuery("select * from "
											+ db.Registration, null);
									cur.moveToFirst();
									if (cur.getCount() >= 1) {
										username = cf.decode(cur.getString(cur
												.getColumnIndex("username")));
										password = cf.decode(cur.getString(cur
												.getColumnIndex("password")));
										userid = cf.decode(cur.getString(cur
												.getColumnIndex("userid")));
									}
									cur.close();

									if (!password.equals(oldpassword.getText()
											.toString())) {
										cf.show_toast("Please enter correct Old Password",1);
										oldpassword.setText("");
										//newpassword.setText("");
										//confirmpassword.setText("");
										oldpassword.requestFocus();
									} else {
										call_reset_password_webservice(confirmpassword
												.getText().toString());
									}

								} else {
									cf.show_toast("New Password and Confirm Password do not match",1);
									newpassword.setText("");
									confirmpassword.setText("");
									newpassword.requestFocus();
								}

							}
						}
						else
						{
							cf.show_toast("please enter a valid password",1);
							newpassword.setText("");
							newpassword.requestFocus();
						}
						
						
					}

				}
			}
		});
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.dismiss();

			}
		});
		clo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog1.dismiss();
		}
		});

		dialog1.show();
	
	}
	protected void call_reset_password_webservice(final String newpass) {
		// TODO Auto-generated method stub
		if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Resetting Password..");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						String chkauth = Calling_WS_ChangePassword(username,
								password, newpass, "CHANGEPASSWORD");
						System.out.println("The Result is " + chkauth);
						if (chkauth.equals("true")) 
						{
							show_handler = 5;
							handler.sendEmptyMessage(0);

						} else /* USERAUTHENTICATION IS FALSE */
						{
							show_handler = 1;
							handler.sendEmptyMessage(0);

						}
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("The error is " + e.getMessage());
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private String Calling_WS_ChangePassword(String username,
						String password, String newpass, String string) throws SocketException,
						IOException, NetworkErrorException, TimeoutException,
						XmlPullParserException, Exception {
					// TODO Auto-generated method stub
					SoapObject request = new SoapObject(cf.NAMESPACE, string);
					SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
					envelope.dotNet = true;
					request.addProperty("Username", username);
					request.addProperty("Currentpwd", password);
					request.addProperty("Newpwd", newpass);
					System.out.println("Request is " + request);
					envelope.setOutputSoapObject(request);

					HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);
					androidHttpTransport.call(cf.NAMESPACE + string, envelope);
					String result = envelope.getResponse().toString();
					return result;
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						// dialog1.dismiss();
						if (show_handler == 1) {
							show_handler = 0;
							cf.show_toast("Invalid Password",1);
							
						} else if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.",1);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

						} else if (show_handler == 5) {
							db.CreateTable(1);
							db.vi_db.execSQL("delete from " + db.Registration);
							
							dialog1.dismiss();
							

							final Dialog dialog2 = new Dialog(
									HomeScreen.this,
									android.R.style.Theme_Translucent_NoTitleBar);
							dialog2.setContentView(R.layout.loginagain);
							dialog2.setCancelable(false);
							TextView ok = (TextView) dialog2
									.findViewById(R.id.redirect);
							ok.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									dialog2.dismiss();
									
									Delete_All_Tables();
									
									Intent intent = new Intent(HomeScreen.this,
											LoginActivity.class);
									startActivity(intent);
									finish();
								}

							
							});
							dialog2.show();
						}
					}
				};
			}.start();
		} else {
			cf.show_toast("Internet connection not available",1);

		}

	}
	private void Delete_All_Tables() {
		// TODO Auto-generated method stub
		db.CreateTable(1);
		db.vi_db.execSQL("delete from " + db.Registration);
		
		db.CreateTable(2);
		db.vi_db.execSQL("delete from " + db.Version);
		
		db.CreateTable(3);
		db.vi_db.execSQL("delete from " + db.VehicleInspection_Pdf);
		
		db.CreateTable(4);
		db.vi_db.execSQL("delete from " + db.AddAImage);
		
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
