package idsoft.inspectiondepot.vehicleinspection;

import idsoft.inspectiondepot.vehicleinspection.supportclass.CommonFunction;

import idsoft.inspectiondepot.vehicleinspection.supportclass.DataBaseHelper;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.ksoap2.serialization.SoapObject;
import org.xmlpull.v1.XmlPullParserException;

import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class OnlineList extends Activity {
	TextView status, noofdatas, inspectionname;
	AutoCompleteTextView etsearch;
	String strsearch, downloadidentifier = "0",status_check,UserId;
	Button search, cancel, home, placeorder;
	LinearLayout dynamic;
	CommonFunction cf;
	int i;
	String[] data, pdfpath, datasend;
	String stragentname, path, agentemail = "", agencyemail = "", filename;
	int show_handler;
	static OnlineList ol;
	static String strstatus,inspid,strinspectionname;
	AlertDialog alertDialog;
	DataBaseHelper db;
	ProgressDialog pd;
	static String reportpath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.onlinelist);
		cf = new CommonFunction(this);
		db = new DataBaseHelper(this);
		
		ol=this;
		
	    status = (TextView) findViewById(R.id.onlinelist_status);
		noofdatas = (TextView) findViewById(R.id.onlinelist_noofdatas);
		inspectionname = (TextView) findViewById(R.id.onlinelist_inspectionname);
		dynamic = (LinearLayout) findViewById(R.id.onlinelist_tablelayoutdynamic);
		etsearch = (AutoCompleteTextView) findViewById(R.id.onlinelist_etsearch);
		
		
		db.CreateTable(1);
		try
		{
			Cursor cur = db.vi_db.rawQuery("select * from " + db.Registration,
				null);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				UserId = cf.decode(cur.getString(cur.getColumnIndex("userid")));
				
			}
			cur.close();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		status.setText("Reports Ready");
		inspectionname.setText("Vehicle Inspection");
		etsearch.addTextChangedListener(new Text_Watcher());
		
		Call_VehicleInformationList();
		
	}
	
	public void Call_VehicleInformationList() {
		if (cf.isInternetOn() == true) {
			String source = "<b><font color=#00FF33>" + "Loading Vehicle Inspection... Please wait."
					+ "</font></b>";
			final ProgressDialog pd = ProgressDialog.show(OnlineList.this,
					"", Html.fromHtml(source), true);
			// show_ProgressDialog("Processing");
			
			new Thread() {
				SoapObject chklogin;
				public void run() {
					Looper.prepare();
					try {
						chklogin = cf
								.Calling_WS_LOADVEHICLEDETAILS(UserId,"LOADCUSTOMVEHICLEDETAILS");
						System.out.println("response LOADCUSTOMVEHICLEDETAILS"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.",1);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

						} else if (show_handler == 5) {
							show_handler = 0;
					
							Call_VehicleInformationList(chklogin);
							
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available",1);

		}
//	}
	}
	
	public void Call_VehicleInformationList(SoapObject objInsert) {
		db.CreateTable(3);
		db.vi_db.execSQL("delete from " + db.VehicleInspection_Pdf);
//		System.out.println("the response for dynamic list is "+objInsert);
		int propertycount=0;
		if(!String.valueOf(objInsert).equals("null"))
		{
			propertycount = objInsert.getPropertyCount();
			System.out.println("VehicleInspection_Pdf property count" + propertycount);
		if(propertycount>=1)
		{
		for (int i = 0; i < propertycount; i++) {
			SoapObject obj = (SoapObject) objInsert.getProperty(i);
			try {
				String filename = String.valueOf(obj.getProperty("PDFPath"));
				String FirstName = String.valueOf(obj.getProperty("Firstname"));
				String LastName = String.valueOf(obj.getProperty("Lastname"));
				String InspectionAddress1 = String.valueOf(obj.getProperty("Address1"));
				String InspectionAddress2 = String.valueOf(obj.getProperty("Address2"));
				String InspectionCity = String.valueOf(obj.getProperty("City"));
				String InspectionState = String.valueOf(obj.getProperty("State"));
				String InspectionCounty = String.valueOf(obj.getProperty("County"));
				String VehicleNumber = String.valueOf(obj.getProperty("Vehicleno"));
				String InspectionZip = String.valueOf(obj.getProperty("Zip"));
				String createddate = String.valueOf(obj.getProperty("Createddeate"));
				String createdtime = String.valueOf(obj.getProperty("Createdtime"));
				
				if(filename.contains(".pdf"))
				{
					db.vi_db.execSQL("insert into " + db.VehicleInspection_Pdf
//							+ " (filename) values('"+ cf.encode(filename)
					+ " (filename,FirstName,LastName,InspectionAddress1,InspectionAddress2,InspectionCity," +
					"InspectionState,InspectionCounty,VehicleNumber,InspectionZip,CreatedDate,CreatedTime) values('"+ cf.encode(filename) + "','"+ 
					cf.encode(FirstName) + "','"+ cf.encode(LastName) + "','"+ cf.encode(InspectionAddress1)
					+ "','"+ cf.encode(InspectionAddress2) + "','"+ cf.encode(InspectionCity) + "','"+ 
					cf.encode(InspectionState) + "','"+ cf.encode(InspectionCounty)
					+ "','"+ cf.encode(VehicleNumber) + "','"+cf.encode(InspectionZip)+"','"+createddate+"','"+createdtime+"');");
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		db.CreateTable(3);
		Cursor cur1 = db.vi_db.rawQuery("select * from "
				+ db.VehicleInspection_Pdf, null);
		int strnoofdatas=cur1.getCount();
		((RelativeLayout)findViewById(R.id.tableLayout2)).setVisibility(View.VISIBLE);
		((TextView)findViewById(R.id.note1)).setVisibility(View.GONE);
		DynamicList(cur1);
		cur1.close();
		
		}
		else
		{
			noofdatas.setText(String.valueOf(propertycount));
			((RelativeLayout)findViewById(R.id.tableLayout2)).setVisibility(View.GONE);
			((TextView)findViewById(R.id.note1)).setVisibility(View.VISIBLE);
		}
		}
		
	}
	
	class Text_Watcher implements TextWatcher
	{

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			try {
        		
  				Cursor cur2 = db.vi_db.rawQuery("select * from "
  						+ db.VehicleInspection_Pdf
  						+ " where FirstName like '%" + etsearch.getText().toString() + "%' order by CreatedDate,CreatedTime DESC", null);
  				String[] autofirstname = new String[cur2.getCount()];
  				cur2.moveToFirst();
  				if(cur2.getCount()!=0)
  				{
  					if (cur2 != null) {
  						int i = 0;
  						do {
  							autofirstname[i] = cf.decode(cur2.getString(cur2.getColumnIndex("FirstName")));
  							
  							if (autofirstname[i].contains("null")) {
  								autofirstname[i] = autofirstname[i].replace("null", "");
  							}
  							 
  							i++;
  						} while (cur2.moveToNext());
  					}
  					cur2.close();
  				}
//  				adapter = new ArrayAdapter<String>(OnlineList.this,R.layout.autocompletelist,autofirstname);
  				
  				Cursor cur3 = db.vi_db.rawQuery("select * from "
  						+ db.VehicleInspection_Pdf
  						+ " where LastName like '%" + etsearch.getText().toString() + "%' order by CreatedDate,CreatedTime DESC", null);
  				String[] autolastname = new String[cur3.getCount()];
  				cur3.moveToFirst();
  				if(cur3.getCount()!=0)
  				{
  					if (cur3 != null) {
  						int i = 0;
  						do {
  							autolastname[i] = cf.decode(cur3.getString(cur3.getColumnIndex("LastName")));
  							
  							if (autolastname[i].contains("null")) {
  								autolastname[i] = autolastname[i].replace("null", "");
  							}
  							 
  							i++;
  						} while (cur3.moveToNext());
  					}
  					cur3.close();
  				}
//  				adapter = new ArrayAdapter<String>(OnlineList.this,R.layout.autocompletelist,autolastname);
  				
  				List<String> images = new ArrayList<String>();
  				images.addAll(Arrays.asList(autofirstname));
  				images.addAll(Arrays.asList(autolastname));
  				
  				ArrayAdapter<String> adapter = new ArrayAdapter<String>(OnlineList.this,R.layout.autocompletelist,images);
  				
  				etsearch.setThreshold(1);
  				etsearch.setAdapter(adapter);
  				
  			}
  			catch(Exception e)
  			{
  				
  			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub
			if (etsearch.getText().toString().startsWith(" "))
	        {
	            // Not allowed
	        	etsearch.setText("");
	        }
		}
		
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.onlinelist_search:
			SearchList();
			break;

		case R.id.onlinelist_cancel:
			etsearch.setText("");
			db.CreateTable(3);
			Cursor cur1 = db.vi_db.rawQuery("select * from "
					+ db.VehicleInspection_Pdf +" order by CreatedDate,CreatedTime DESC", null);
			cf.hidekeyboard((EditText)etsearch);
			DynamicList(cur1);
			cur1.close();
			break;

		case R.id.onlinelist_home:
			Intent intent = new Intent(OnlineList.this, HomeScreen.class);
			startActivity(intent);
			finish();
			break;

		}

	}

	private void SearchList() {
		strsearch = cf.encode(etsearch.getText().toString());
		if (strsearch.equals("")) {
			cf.show_toast("Please enter the Name/Vehicle No to search.",1);
			etsearch.requestFocus();
		} else {
			dbquery();
		}
	}

	private void dbquery() {
		Cursor cur = null;
		if (!strsearch.trim().equals("")) {
			db.CreateTable(3);
			cur = db.vi_db.rawQuery("select * from "
					+ db.VehicleInspection_Pdf
					+ " where FirstName like '%" + strsearch
					+ "%' or LastName like '%" + strsearch
			        + "%' or VehicleNumber like '%" + strsearch+ "%' order by CreatedDate,CreatedTime DESC", null);
		}
		cur.moveToFirst();
		int rows = cur.getCount();
		if (cur.getCount() >= 1) {
			cf.hidekeyboard((EditText)etsearch);
			DynamicList(cur);
		} else {
			cf.show_toast("Sorry, No Results Available",1);
			cf.hidekeyboard((EditText)etsearch);
		}

	}

	private void DynamicList(Cursor cur) {
		dynamic.removeAllViews();
		ScrollView sv = new ScrollView(this);
		sv.setScrollBarStyle(ScrollView.SCROLLBARS_INSIDE_INSET);
		sv.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
		dynamic.addView(sv);

		final LinearLayout l1 = new LinearLayout(this);
		l1.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
		l1.setOrientation(LinearLayout.VERTICAL);
		sv.addView(l1);

		int rows = cur.getCount();
		noofdatas.setText(String.valueOf(rows));
		
		TextView[] tvstatus = new TextView[rows];
		Button[] view = new Button[rows];
		final Button[] pter = new Button[rows];
		RelativeLayout[] rl = new RelativeLayout[rows];
		final String[] ownersname=new String[rows];
		
		data = new String[rows];
		datasend = new String[rows];
		pdfpath = new String[rows];
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			i = 0;
			do {
				String file_name = cf.decode(cur.getString(cur
						.getColumnIndex("filename")));
				String FirstName = cf.decode(cur.getString(cur
						.getColumnIndex("FirstName")));
				ownersname[i] =FirstName+" ";
				data[i] = " " + FirstName + " | ";
				String LastName = cf.decode(cur.getString(cur
						.getColumnIndex("LastName")));
				ownersname[i] +=LastName;
				data[i] += LastName + " | ";
				String Address1 = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionAddress1")));
				data[i] += Address1 + " | ";
				String Address2 = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionAddress2")));
				data[i] += Address2 + " | ";
				String City = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionCity")));
				data[i] += City + " | ";
				String State = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionState")));
				data[i] += State + " | ";
				String County = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionCounty")));
				data[i] += County + " | ";
				String InspectionZip = cf.decode(cur.getString(cur
						.getColumnIndex("InspectionZip")));
				data[i] += InspectionZip + " | ";
				String VehicleNumber = cf.decode(cur.getString(cur
						.getColumnIndex("VehicleNumber")));
				data[i] += VehicleNumber + " | ";
				String CreatedDate = cf.decode(cur.getString(cur
						.getColumnIndex("CreatedDate")));
				data[i] += CreatedDate + " | ";
				String CreatedTime = cf.decode(cur.getString(cur
						.getColumnIndex("CreatedTime")));
				data[i] += CreatedTime ;
				datasend[i] = VehicleNumber;
				
				
				pdfpath[i] = file_name;
				
				System.out.println("The pdf path is "+filename);
				
				System.out.println("The Datas is " + data[i]);
				
				LinearLayout.LayoutParams llparams;
				RelativeLayout.LayoutParams tvparams;
				RelativeLayout.LayoutParams downloadparams,pterparams,viewparams;
				LinearLayout.LayoutParams lltxtparams;
				
				
				llparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.FILL_PARENT,
						ViewGroup.LayoutParams.MATCH_PARENT);
				llparams.setMargins(0, 0, 0, 0);

				tvparams = new RelativeLayout.LayoutParams(680,
						ViewGroup.LayoutParams.MATCH_PARENT);
				tvparams.addRule(RelativeLayout.CENTER_VERTICAL);
				tvparams.setMargins(0, 0, 0, 0);

				downloadparams = new RelativeLayout.LayoutParams(110,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				downloadparams.addRule(RelativeLayout.CENTER_VERTICAL);
				downloadparams.setMargins(830, 0, 0, 0);

				viewparams = new RelativeLayout.LayoutParams(
						ViewGroup.LayoutParams.WRAP_CONTENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				viewparams.addRule(RelativeLayout.CENTER_VERTICAL);
				viewparams.setMargins(700, 0, 0, 0);

				rl[i] = new RelativeLayout(this);
				l1.addView(rl[i], llparams);

				LinearLayout lltxt = new LinearLayout(this);
				lltxt.setLayoutParams(tvparams);
				lltxt.setPadding(20, 0, 20, 0);
				rl[i].addView(lltxt);

				lltxtparams = new LinearLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT,
						ViewGroup.LayoutParams.WRAP_CONTENT);
				lltxtparams.setMargins(0, 30, 0, 30);

				tvstatus[i] = new TextView(this);
				tvstatus[i].setLayoutParams(lltxtparams);
				tvstatus[i].setId(1);
				tvstatus[i].setText(data[i]);
				
				tvstatus[i].setTextSize(14);
				lltxt.addView(tvstatus[i]);

				LinearLayout lldownload = new LinearLayout(this);
				lldownload.setLayoutParams(downloadparams);
				rl[i].addView(lldownload);

				LinearLayout.LayoutParams downloadparams1 = new LinearLayout.LayoutParams(
						110, ViewGroup.LayoutParams.WRAP_CONTENT);
				downloadparams1.gravity = Gravity.CENTER_VERTICAL;


				LinearLayout llpter = new LinearLayout(this);
				llpter.setLayoutParams(downloadparams);
				rl[i].addView(llpter);

				pter[i] = new Button(this);
				pter[i].setLayoutParams(downloadparams1);
				pter[i].setId(2);
				pter[i].setText("Email Report");
				pter[i].setBackgroundResource(R.drawable.blackbutton);
				pter[i].setTextColor(0xffffffff);
				pter[i].setTextSize(14);
				pter[i].setTypeface(null, Typeface.BOLD);
				pter[i].setTag(i);
				pter[i].setTag(i + "&#40" + ownersname[i] + "&#40" + i);
				llpter.addView(pter[i]);

				LinearLayout llview = new LinearLayout(this);
				llview.setLayoutParams(viewparams);
				rl[i].addView(llview);

				view[i] = new Button(this);
				view[i].setLayoutParams(downloadparams1);
				view[i].setId(2);
				view[i].setText("View PDF");
				view[i].setBackgroundResource(R.drawable.blackbutton);
				view[i].setTextColor(0xffffffff);
				view[i].setTextSize(14);
				view[i].setTypeface(null, Typeface.BOLD);
				view[i].setTag(i);
				llview.addView(view[i]);

				path = pdfpath[i];
				String[] filenamesplit = path.split("/");
				final String filename = filenamesplit[filenamesplit.length - 1];
				System.out.println("the file name is" + filename);
				File sdDir = new File(Environment.getExternalStorageDirectory()
						.getPath());
				File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
						+ filename);

				if (file.exists()) {
					pter[i].setVisibility(View.VISIBLE);
					
				} else {
					pter[i].setVisibility(View.VISIBLE);
					
				}

				view[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Button b = (Button) v;
						String buttonvalue = v.getTag().toString();
						System.out.println("buttonvalue is" + buttonvalue);
						int s = Integer.parseInt(buttonvalue);
						path = pdfpath[s];
						String[] filenamesplit = path
								.split("/");
						final String filename = filenamesplit[filenamesplit.length - 1];
						System.out
								.println("The File Name is "
										+ filename);
						File sdDir = new File(Environment.getExternalStorageDirectory()
								.getPath());
						File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
								+ filename);
						
						if(file.exists())
						{
							View_Pdf_File(filename,path);
						}
						else
						{
							if (cf.isInternetOn() == true) {
								cf.show_ProgressDialog("Downloading..");
								new Thread() {
									public void run() {
										Looper.prepare();
										try {
											String extStorageDirectory = Environment
													.getExternalStorageDirectory()
													.toString();
											File folder = new File(
													extStorageDirectory,
													"DownloadedPdfFile");
											folder.mkdir();
											File file = new File(folder,
													filename);
											try {
												file.createNewFile();
												Downloader.DownloadFile(path,
														file);
											} catch (IOException e1) {
												e1.printStackTrace();
											}

											show_handler = 2;
											handler.sendEmptyMessage(0);

										} catch (Exception e) {
											// TODO Auto-generated catch block
											System.out.println("The error is "
													+ e.getMessage());
											e.printStackTrace();
											show_handler = 1;
											handler.sendEmptyMessage(0);

										}
									}

									private Handler handler = new Handler() {
										@Override
										public void handleMessage(Message msg) {
											cf.pd.dismiss();
											// dialog1.dismiss();
											if (show_handler == 1) {
												show_handler = 0;
												cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

											} else if (show_handler == 2) {
												show_handler = 0;
												
												View_Pdf_File(filename,path);

											}
										}
									};
								}.start();
							} else {
								cf.show_toast("Internet connection not available",1);

							}
						}
					}
				});


				pter[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						String buttonvalue = v.getTag().toString();
						String[] splitvalue=buttonvalue.split("&#40");
						int s = Integer.parseInt(splitvalue[0]);
						String name=splitvalue[1];
						path = pdfpath[s];
						reportpath=path;
						String[] filenamesplit = path.split("/");
						String filename = filenamesplit[filenamesplit.length - 1];
						
						
						String ivalue=splitvalue[2];
						System.out.println("ivalue ="+ivalue);
						int ival=Integer.parseInt(ivalue);
						String ivalsplit=datasend[ival];
						System.out.println("ivaluesplit ="+ivalsplit);
						String pn=datasend[ival];
						String status="";

						Intent intent=new Intent(OnlineList.this,EmailReport.class);
						
						intent.putExtra("ownersname", ownersname[s]);
						startActivity(intent);
						finish();

					}
				});

				if (i % 2 == 0) {
					rl[i].setBackgroundColor(Color.parseColor("#6E6E6E"));
					tvstatus[i].setTextColor(Color.WHITE);
				} else {
					rl[i].setBackgroundColor(Color.parseColor("#989898"));
					tvstatus[i].setTextColor(Color.BLACK);
				}

				i++;
			} while (cur.moveToNext());

			/*LinearLayout.LayoutParams viewparams2 = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT, 1);
			viewparams2.setMargins(20, 0, 20, 0);

			View v2 = new View(this);
			v2.setBackgroundResource(R.color.black);
			dynamic.addView(v2, viewparams2);*/

		}

	}
	
	private void View_Pdf_File(String filename,String filepath)
	{
		File sdDir = new File(Environment.getExternalStorageDirectory()
				.getPath());
		File file = new File(sdDir.getPath() + "/DownloadedPdfFile/"
				+ filename);
		Uri path = Uri.fromFile(file);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(path, "application/pdf");
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		
		try {
            startActivity(intent);
        } 
        catch (ActivityNotFoundException e) {
//            Toast.makeText(VehicleInspection.this, 
//                "No Application Available to View PDF", 
//                Toast.LENGTH_SHORT).show();
			Intent intentview = new Intent(OnlineList.this,
					ViewPdfFile.class);
			intentview.putExtra("path", filepath);
			startActivity(intentview);
			// finish();
        }
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		Intent inthome = new Intent(OnlineList.this, HomeScreen.class);
		startActivity(inthome);
		finish();
	}

}
