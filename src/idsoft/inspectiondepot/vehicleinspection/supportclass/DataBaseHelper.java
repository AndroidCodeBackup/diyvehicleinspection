package idsoft.inspectiondepot.vehicleinspection.supportclass;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.text.format.DateFormat;
import android.util.Log;

public class DataBaseHelper {
	public String MY_DATABASE_NAME = "VehicleDatabases.db";
	public static final String Registration = "Registration_Credentials";
	public static final String Version = "Version";
	public static final String VehicleInspection_Pdf = "VehicleInspection_Pdf";
	public static final String AddAImage = "AddAImage";
	
	private static String DB_PATH = "/data/idsoft.inspectiondepot.vehicleinspection/databases/";

	private static String DB_NAME = "State_County_DB";
	public static SQLiteDatabase vi_db;
    Context con;
   
    public DataBaseHelper(Context con) 
    {
    	vi_db = con.openOrCreateDatabase(MY_DATABASE_NAME, 1, null);
		this.con = con;
     
	}
    
	
	public void CreateTable(int i) {
		// TODO Auto-generated method stub
	switch (i) 
	{
	   case 1:
		/* Registration Credentials */
		try {
			vi_db.execSQL("create table if not exists "
					+ Registration
					+ " (rid integer primary key autoincrement,firstname varchar(50),lastname varchar2(50),phonenum varcahr(50),email varchar(50),userid varchar(50),username varchar(50),password varchar2(50))");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		break;
		
		case 2:
			try {
				vi_db.execSQL("create table if not exists "
						+ Version
						+ " (VersionCode varchar2,VersionName varchar2)");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
		case 3:
			try {
				vi_db.execSQL("create table if not exists "
						+ VehicleInspection_Pdf
				 + " (id integer primary key autoincrement,filename varchar2," +
				 "FirstName varchar2,LastName varchar2,InspectionAddress1 varchar2,InspectionAddress2 varchar2," +
				 "InspectionCity varchar2,InspectionState varchar2," +
				 "InspectionCounty varchar2,VehicleNumber varchar2,InspectionZip varchar2,CreatedDate varchar,CreatedTime varchar)");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
			
	  case 4:
			try {
				vi_db.execSQL("create table if not exists "
						+ AddAImage
						+ " (id varchar2(50),caption varchar2(50),elevation varchar2(50),filepath varchar2)");
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			break;
		
	}
		
	}
	public Cursor SelectTablefunction(String inspectorlogin2, String string) {
		// TODO Auto-generated method stub
		Cursor cur = vi_db.rawQuery("select * from " + inspectorlogin2 + " " + string, null);
		return cur;
	}
	public String encode(String oldstring) 
	{
		if(oldstring==null)
		{
			oldstring="";
		}
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return oldstring;

	}
	public String decode(String newstring) 
	{
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newstring;
	}
	
	public boolean check_value_inTable(String tbl_name,String Where)
	{
		Cursor c =SelectTablefunction(tbl_name, Where);
		if(c.getCount()>0)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}


	public void Delete_all_tables() {
		// TODO Auto-generated method stub
		CreateTable(1);
		vi_db.execSQL("delete from " + Registration);
	}
}
