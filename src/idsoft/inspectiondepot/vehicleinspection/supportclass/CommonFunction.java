package idsoft.inspectiondepot.vehicleinspection.supportclass;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import idsoft.inspectiondepot.vehicleinspection.R;
import android.accounts.NetworkErrorException;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.provider.Settings;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CommonFunction {

	public String NAMESPACE = "http://tempuri.org/";
	public String URL = "http://vehicleinspection.paperlessinspectors.com/AgencyWeb.asmx";
			//"http://vehicleinspection.paperlessinspectors.com/vehicleinspectionservice.asmx"; //LIVE
			//"http://72.15.221.153:92/AgencyWeb.asmx";//LIVE
Context  con;
public String deviceId,model,manuf,devversion,apiLevel,versionname;
public ProgressDialog pd;
int ipAddress;
public boolean value=true;

public static Integer[] mThumbIds = {R.drawable.phonenum,R.drawable.inspectiondepotimage};

	public CommonFunction(Context cont)
	{
		con=cont;
		
	}
	public String decode(String newstring) {
		try {
			newstring = URLDecoder.decode(newstring, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newstring;

	}
	public String encode(String oldstring) {
		if (oldstring == null) {
			oldstring = "";
		}
		try {
			oldstring = URLEncoder.encode(oldstring, "UTF-8");

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return oldstring;

	}
	public Bitmap ShrinkBitmap(String file, int width, int height) {
		Bitmap bitmap = null;
		try {
			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) height);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) width);

			if (heightRatio > 1 || widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions);
			return bitmap;
		} catch (Exception e) {
			// cf.errorlogmanage("Problme in opening the image in sring  error="
			// + e.getMessage());
			return bitmap;
		}
	}
	public void Device_Information()
	{
		deviceId= Settings.System.getString(this.con.getContentResolver(), Settings.System.ANDROID_ID);
		model = android.os.Build.MODEL;
		manuf = android.os.Build.MANUFACTURER;
		devversion = android.os.Build.VERSION.RELEASE;
		apiLevel = android.os.Build.VERSION.SDK;
		WifiManager wifiManager = (WifiManager)(this.con.getSystemService(this.con.WIFI_SERVICE));
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		
		ipAddress = wifiInfo.getIpAddress();
	}
	public String Calling_WS_EmailReport(String userid, String to,
			String subject, String body, String path, String string3) throws SocketException,
			IOException, NetworkErrorException, TimeoutException,
			XmlPullParserException, Exception {
		versionname = getdeviceversionname();
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("UserTypeID", 10);
		request.addProperty("AgentID", userid);
		request.addProperty("To", to);
		request.addProperty("Subject", subject);
		request.addProperty("Body", body);
		request.addProperty("Pdfpath", path);
		request.addProperty("ApplicationVersion",versionname);
		request.addProperty("DeviceName",model);
		request.addProperty("APILevel",apiLevel);
		System.out.println("Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}
	public void hidekeyboard(EditText editText){
        InputMethodManager imm = (InputMethodManager)con.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
	public SoapObject Calling_WS_GETADDRESSDETAILS(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Zipcode", string);
		System.out.println("Zipcode Request is " + request);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	public String Calling_WS_ViewCustomerPDF(String string, String string3)
			throws SocketException, IOException, NetworkErrorException,
			TimeoutException, XmlPullParserException, Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("SRID", string);
		System.out.println("ViewCustomerPDF request is " + request);
		envelope.setOutputSoapObject(request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		String result = envelope.getResponse().toString();
		return result;

	}
	public String getdeviceversionname() {
		// TODO Auto-generated method stub
		try {
			versionname = this.con.getPackageManager().getPackageInfo(
					this.con.getPackageName(), 0).versionName;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
		}
		return versionname;
	}
	public SoapObject Calling_WS_LOADVEHICLEDETAILS(String string,
			String string3) throws SocketException, IOException,
			NetworkErrorException, TimeoutException, XmlPullParserException,
			Exception {
		SoapObject request = new SoapObject(NAMESPACE, string3);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.dotNet = true;
		request.addProperty("Usertype", 10);
		request.addProperty("Userid", Integer.parseInt(string));
		envelope.setOutputSoapObject(request);
		System.out.println("LOADCUSTOMVEHICLEDETAILS request is "+request);
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		androidHttpTransport.call(NAMESPACE + string3, envelope);
		SoapObject result = (SoapObject) envelope.getResponse();
		return result;

	}
	
	public void Dynamic_Image_Changing(final ImageView iv) {
		// TODO Auto-generated method stub
		final Handler handler = new Handler();
	    Runnable runnable = new Runnable() 
	    {
	                int i=0;
	                public void run() 
	                {
	                    iv.setImageResource(mThumbIds[i]);
	                    i++;
	                    if(i>mThumbIds.length-1)
	                    {
	                    i=0;    
	                    }
	                    handler.postDelayed(this, 2000);  //for interval...
	                }

	    };
	    handler.postDelayed(runnable, 1000); //for initial delay..
	           
	}
	public void show_toast(String msg,int type)
	{
		String colorname = "#000000"; 
		switch(type)
		{
		case 0:
			colorname="#000000";
			break;
		case 1:
			colorname="#890200";
			break;
		case 2:
			colorname="#F3C3C3";
			break;
		}
		final Toast toast = new Toast(this.con);
		 LayoutInflater inflater = (LayoutInflater)this.con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				 View layout = inflater.inflate(R.layout.toast, null);
		
		TextView tv = (TextView) layout.findViewById(
				R.id.text);
		tv.setTextColor(Color.parseColor(colorname));
		toast.setGravity(Gravity.CENTER, 0, 0);
		tv.setText(msg);
		toast.setView(layout); 
		Thread t = new Thread() {
            public void run() {
                int count = 0;
                try {
                    while (true && count < 10) {
                        toast.show();
                        sleep(3);
                        count++;

                        // do some logic that breaks out of the while loop
                    }
                } catch (Exception e) {
                   
                }
            }
        };
        t.start();
		
	}
	public boolean eMailValidation(CharSequence stringemailaddress1) {
		// TODO Auto-generated method stub
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(stringemailaddress1);

		return matcher.matches();

	}

	public final boolean isInternetOn() {
		boolean chk = false;
		ConnectivityManager conMgr = (ConnectivityManager) this.con
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = conMgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			chk = true;
		} else {
			chk = false;
		}

		return chk;
	}
	public void show_ProgressDialog(String string) {
		// TODO Auto-generated method stub
		String source = "<b><font color=#00FF33>"+string+" Please wait...</font></b>";
		pd = ProgressDialog.show(this.con, "", Html.fromHtml(source), true);
	}
}
