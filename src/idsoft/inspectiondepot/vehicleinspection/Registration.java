package idsoft.inspectiondepot.vehicleinspection;

import idsoft.inspectiondepot.vehicleinspection.supportclass.CommonFunction;
import idsoft.inspectiondepot.vehicleinspection.supportclass.CustomTextWatcher;
import idsoft.inspectiondepot.vehicleinspection.supportclass.DataBaseHelper;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Registration extends Activity{
	CommonFunction cf;
	DataBaseHelper db;
    EditText reg_et_firstname,reg_et_lastname,reg_et_phnnum,reg_et_email,reg_username;
    int keyDel,id_len,show_handler;
    boolean phonevalidation;
    String status_userid="",userid,firstname,lastname,phonenum,username,password,email;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		cf = new CommonFunction(this);
		db = new DataBaseHelper(this);
		setContentView(R.layout.registration);
		
		reg_et_firstname = ((EditText)findViewById(R.id.reg_firstname));
		reg_et_lastname = ((EditText)findViewById(R.id.reg_lastname));
		reg_et_phnnum = ((EditText)findViewById(R.id.reg_phonenumber));
		reg_et_email = ((EditText)findViewById(R.id.reg_email));
		reg_username = ((EditText)findViewById(R.id.reg_username));
		
		reg_et_firstname.addTextChangedListener(new CustomTextWatcher(reg_et_firstname));
		reg_et_lastname.addTextChangedListener(new CustomTextWatcher(reg_et_lastname));
		reg_username.addTextChangedListener(new CustomTextWatcher(reg_username));
		reg_et_phnnum.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				if (reg_et_phnnum.getText().toString().startsWith(" "))
		        {
		            // Not allowed
					reg_et_phnnum.setText("");
		        }
				if (reg_et_phnnum.getText().toString().trim().matches("^0") )
	            {
	                // Not allowed
					reg_et_phnnum.setText("");
	            }
				
				reg_et_phnnum.setOnKeyListener(new OnKeyListener() {

					public boolean onKey(View v, int keyCode, KeyEvent event) {
						// TODO Auto-generated method stub
						if (keyCode == KeyEvent.KEYCODE_DEL)
						{
							keyDel = 1;
							
							String str = reg_et_phnnum.getText().toString();
							int len=str.length();
							
							if(len==9)
							{
								str = str.substring(0, str.length()-1);
								reg_et_phnnum.setText(str);
								reg_et_phnnum.setSelection(str.length());
							}
							if(len==5)
							{
								str = str.substring(0, str.length()-1);
								reg_et_phnnum.setText(str);
								reg_et_phnnum.setSelection(str.length());
							}
							if(len==1)
							{
								str = str.substring(0, str.length()-1);
								reg_et_phnnum.setText(str);
								reg_et_phnnum.setSelection(str.length());
							}
							
							return false;
						}
						else
						{
							keyDel = 0;
							return false;
						}
					}

					
				});

				if (keyDel == 0) {
					String a = "";
					String str = reg_et_phnnum.getText().toString();
					String replaced = str.replaceAll(Pattern.quote("("), "");
					replaced = replaced.replaceAll(Pattern.quote("-"), "");
					replaced = replaced.replaceAll(Pattern.quote(")"), "");
					char[] id_char = replaced.toCharArray();
					id_len = replaced.length();
					
					for (int i = 0; i < id_len; i++) {
						if (i == 0) {
							a = "(" + id_char[i];
						} else if (i == 2) {
							a += id_char[i] + ")";
						} else if (i == 5) {
							a += id_char[i] + "-";
						} else
							a += id_char[i];
							keyDel = 0;
					}
					reg_et_phnnum.removeTextChangedListener(this);
					reg_et_phnnum.setText(a);
					if (before > 0)
						reg_et_phnnum.setSelection(start);
					else
						reg_et_phnnum.setSelection(a.length());
					    reg_et_phnnum.addTextChangedListener(this);
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		reg_et_email.addTextChangedListener(new CustomTextWatcher(reg_et_email));
		
		((TextView)findViewById(R.id.checkavailablity)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(reg_username.getText().toString().trim().equals(""))
				{
					cf.show_toast("Please enter Username", 1);
				}
				else
				{
					if (cf.isInternetOn() == true) {
						cf.show_ProgressDialog("Checking availability...");
						new Thread() {
							public void run() {
								Looper.prepare();
								
								try
								{
									SoapObject request = new SoapObject(cf.NAMESPACE,"CHECKAVAILABILITY");
									SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
									envelope.dotNet = true;
									request.addProperty("Username",reg_username.getText().toString());
									envelope.setOutputSoapObject(request);
									System.out.println("CHECKAVAILABILITY request is " + request);
									
									HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);						
									androidHttpTransport.call(cf.NAMESPACE+ "CHECKAVAILABILITY", envelope);
									SoapObject result = (SoapObject) envelope.getResponse();
									
									System.out.println("CHECKAVAILABILITY result is"+ result);
									
									String status = String.valueOf(result.getProperty("Status"));
									System.out.println("status="+status);
									if(status.toLowerCase().equals("true"))
									{
										status_userid = String.valueOf(result.getProperty("Errormsg"));
										show_handler = 1;
										handler.sendEmptyMessage(0);
									}
									else
									{  
										status_userid = String.valueOf(result.getProperty("Errormsg"));
										show_handler = 2;
										handler.sendEmptyMessage(0);
										
									}
									
									
								}
								catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									show_handler = 3;
									handler.sendEmptyMessage(0);
								} catch (XmlPullParserException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									show_handler = 3;
									handler.sendEmptyMessage(0);
								}
								catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									show_handler = 4;
									handler.sendEmptyMessage(0);

								}
							}
							private Handler handler = new Handler() {
								@Override
								public void handleMessage(Message msg) {
									cf.pd.dismiss();
									if (show_handler == 3) {
										show_handler = 0;
										cf.show_toast("There is a problem on your Network. Please try again later with better Network",1);

									} else if (show_handler == 4) {
										show_handler = 0;
										cf.show_toast("There is a problem on your application. Please contact Paperless administrator",1);

									} else if (show_handler == 2) {
										show_handler = 0;
										cf.show_toast(status_userid,1);
									}else if (show_handler == 1) {
										show_handler = 0;
										cf.show_toast(status_userid,1);
									}
								}
							};
						}.start();
					}
					else
					{
						cf.show_toast("Internet connection not available", 1);
					}
				}
			}
		});
	
	}
	
	public void clicker(View v)
	{
		switch (v.getId()) {
		case R.id.submit:
			submit_validation();
			break;
       
		case R.id.clear:
			reg_et_firstname.setText("");
			reg_et_lastname.setText("");
			((EditText)findViewById(R.id.reg_phonenumber)).setText("");
			((EditText)findViewById(R.id.reg_email)).setText("");
			reg_username.setText("");
			reg_et_firstname.requestFocus();
			break;
			
		default:
			break;
		}
	}
	private void submit_validation() {
		// TODO Auto-generated method stub
		if(reg_et_firstname.getText().toString().trim().equals(""))
		{
			cf.show_toast("Please enter First Name", 1);
			reg_et_firstname.requestFocus();
		}
		else
		{
			if(reg_et_lastname.getText().toString().trim().equals(""))
			{
				cf.show_toast("Please enter Last Name", 1);
				reg_et_lastname.requestFocus();
			}
			else
			{
				if(reg_et_phnnum.getText().toString().equals(""))
				{
					cf.show_toast("Please enter Phone Number", 1);
					reg_et_phnnum.requestFocus();
				}
				else
				{
					if (reg_et_phnnum.getText().toString().trim().length() < 13)
					{
							cf.show_toast("Please enter valid Phone Number", 1);
					}
					else
					{
						if(reg_et_email.getText().toString().trim().equals(""))
						{
							cf.show_toast("Please enter Email", 1);
							reg_et_email.requestFocus();
						}
						else
						{
							boolean chkmail = cf.eMailValidation(reg_et_email.getText().toString());
							if(chkmail)
							{
								if(reg_username.getText().toString().trim().equals(""))
								{
									cf.show_toast("Please enter Username", 1);
									reg_username.requestFocus();
								}
								else
								{
									register();
								}
								
							}
							else
							{
								cf.show_toast("Please enter valid Email",1);
							}
						}
					}
				
				}
			}
		}
	}
	private void register()  {
		// TODO Auto-generated method stub
		if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Registering...");
			new Thread() {
				public void run() {
					Looper.prepare();
					
					try
					{
						SoapObject request = new SoapObject(cf.NAMESPACE,"USERREGISTRATION");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("Firstname",reg_et_firstname.getText().toString());
						request.addProperty("Lastname",reg_et_lastname.getText().toString());
						request.addProperty("Phoneno",reg_et_phnnum.getText().toString());
						request.addProperty("Email",reg_et_email.getText().toString());
						request.addProperty("Username",reg_username.getText().toString());
						envelope.setOutputSoapObject(request);
						System.out.println("USERREGISTRATION request is " + request);
						
						HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);						
						androidHttpTransport.call(cf.NAMESPACE+ "USERREGISTRATION", envelope);
						SoapObject result = (SoapObject) envelope.getResponse();
						
						System.out.println("USERREGISTRATION result is"+ result);
						
						String status = String.valueOf(result.getProperty("Status"));
						System.out.println("status="+status);
						if(status.toLowerCase().equals("true"))
						{
							db.CreateTable(1);
							 userid = String.valueOf(result.getProperty("Userid"));
							 firstname = String.valueOf(result.getProperty("FirstName"));
							 lastname = String.valueOf(result.getProperty("LastName"));
							 phonenum = String.valueOf(result.getProperty("Phoneno"));
							 email = String.valueOf(result.getProperty("Email"));
							 username = String.valueOf(result.getProperty("Username"));
							 password = String.valueOf(result.getProperty("Password"));
							
							try
							{
								db.vi_db.execSQL("INSERT INTO "
										+ db.Registration
										+ " (firstname,lastname,phonenum,email,userid,username,password)"
										+ " VALUES ('" + cf.encode(firstname) + "','"
										+ cf.encode(lastname) + "','"
										+ cf.encode(phonenum) + "','"
										+ cf.encode(email) + "','"
										+ userid + "','"
										+ cf.encode(username) + "','"
										+ cf.encode(password) + "')");
								show_handler = 1;
								handler.sendEmptyMessage(0);
								
							}catch (Exception e) {
								// TODO: handle exception
							}
							
						}
						else
						{  
							status_userid = String.valueOf(result.getProperty("Errormsg"));
							show_handler = 2;
							handler.sendEmptyMessage(0);
						}
						
						
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}
				}
				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network",1);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator",1);

						} else if (show_handler == 2) {
							show_handler = 0;
							cf.show_toast(status_userid,1);
						}else if (show_handler == 1) {
							show_handler = 0;
							showsucess_popup();
						}
					}
				};
			}.start();
		}
		else
		{
			cf.show_toast("Internet connection not available", 1);
		}
	}

	private void showsucess_popup() {
		// TODO Auto-generated method stub
		
		final Dialog add_dialog = new Dialog(Registration.this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		add_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//add_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.LTGRAY));
		add_dialog.setCancelable(false);
		add_dialog.getWindow().setContentView(R.layout.registrationcustom);
		 
		((TextView)add_dialog.findViewById(R.id.redirect)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(Registration.this,LoginActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.putExtra("status", "register");
				startActivity(intent);
				finish();
		
			}
		});
		
		add_dialog.setCancelable(false);
		add_dialog.show();
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(Registration.this,LoginActivity.class));
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
