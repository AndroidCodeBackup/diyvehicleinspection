package idsoft.inspectiondepot.vehicleinspection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;

public class Webview extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewpdffile);
		
		WebView webView = (WebView) findViewById(R.id.viewpdffile_webView1);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl("http://www.inspectiondepot.com");
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			startActivity(new Intent(Webview.this,Info.class));
			finish();
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
