package idsoft.inspectiondepot.vehicleinspection;

import idsoft.inspectiondepot.vehicleinspection.supportclass.CommonFunction;
import idsoft.inspectiondepot.vehicleinspection.supportclass.CustomTextWatcher;
import idsoft.inspectiondepot.vehicleinspection.supportclass.DataBaseHelper;

import java.io.IOException;
import java.util.Set;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;

public class LoginActivity extends Activity {
	CommonFunction cf;
	DataBaseHelper db;
	EditText etpassword;
	int show_handler;
	AutoCompleteTextView autousername;
	String username="", password="",userid="", firstname="",lastname="",phonenum="",email="",status_userid="",
			strusername="",strpassword="",chkstatus="",forpassword="",forusername="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		System.out.println("enter");
		Bundle bundle=getIntent().getExtras();
		
		if(bundle!=null)
		{
			if(bundle.getString("status")!=null)
			{
		  chkstatus=bundle.getString("status");
		  if(chkstatus.equals("forgot"))
		  {
			  forpassword=bundle.getString("retpass");
			  forusername=bundle.getString("retuser");
		  }
			}
		}
		System.out.println("chkstatus="+chkstatus);
		cf = new CommonFunction(this);
		db= new DataBaseHelper(this);
		declaration();
		Parse_Code();
		
		if(chkstatus.equals("register"))
		{
			Cursor cur1 = db.vi_db.rawQuery("select * from " + db.Registration, null);
			if(cur1.getCount() >= 1)
			{
				cur1.moveToFirst();
				System.out.println("username="+cur1.getString(cur1.getColumnIndex("username")));
				autousername.setText(cf.decode(cur1.getString(cur1.getColumnIndex("username"))));
				etpassword.setText(cf.decode(cur1.getString(cur1.getColumnIndex("password"))));
			}
		}
		else if(chkstatus.equals("forgot"))
		{
			autousername.setText(cf.decode(forusername));
			etpassword.setText(cf.decode(forpassword));
		}
		else
		{
		       check_logintable();
		}
	}
	
	private void Parse_Code()
	{
		//Parse code for receiving notifications
		
        Parse.initialize(this, "bPATR7MUQNk8x61ssKg4etb7xZUsFFHZnZmqF1Yn", "bWOgIMz7Zin6W4o8VrzaDYnfwX7R0b90CtfcpdOl");

		ParseAnalytics.trackAppOpened(getIntent());

		// inform the Parse Cloud that it is ready for notifications
		db.CreateTable(1);
		Cursor cur = db.vi_db.rawQuery("select * from " + db.Registration, null);
		if (cur.getCount() >= 1) {
			PushService.setDefaultPushCallback(this, HomeScreen.class);
		}
		else
		{
			PushService.setDefaultPushCallback(this, LoginActivity.class);
		}
//		PushService.setDefaultPushCallback(this, HomeScreen.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		
		//Parse code for receiving notifications
	}


	private void check_logintable() {
		// TODO Auto-generated method stub
		db.CreateTable(1);
		Cursor cur = db.vi_db.rawQuery("select * from " + db.Registration, null);
		if(cur.getCount() >= 1)
		{
			startActivity(new Intent(this, HomeScreen.class));
			finish();
		}
		
		cur.close();
	}

	private void declaration() {
		// TODO Auto-generated method stub
		etpassword = (EditText) findViewById(R.id.loginpage_etpassword);
		autousername = (AutoCompleteTextView) findViewById(R.id.loginpage_autocomtvusername);
		
		
		autousername.addTextChangedListener(new CustomTextWatcher(autousername));
        etpassword.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				 if (etpassword.getText().toString().startsWith(" "))
			        {
			            // Not allowed
					 etpassword.setText("");
			        }
				 String result = s.toString().replaceAll(" ", "");
				    if (!s.toString().equals(result)) {
				         etpassword.setText(result);
				         etpassword.setSelection(result.length());
				       
				    }
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
			}
		});
        
        ((TextView)findViewById(R.id.loginpage_forgotpassword)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,ForgotPassword.class));
				finish();
			}
		});
        
        ((TextView)findViewById(R.id.loginpage_register)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(LoginActivity.this,Registration.class));
				finish();
			}
		});
	}

	public void clicker(View v) {
		switch (v.getId()) {

		case R.id.loginpage_btnlogin:
			check_username_password_notempty();
			break;

		case R.id.loginpage_btncancel:
			db.Delete_all_tables();
			finish();
			break;
	
		}
	}

	private void check_username_password_notempty() {
		username = autousername.getText().toString();
		password = etpassword.getText().toString();
		if (username.trim().equals("") && password.trim().equals("")) {
			cf.show_toast("Please enter Username and Password", 1);
			autousername.requestFocus();
			autousername.setText("");
			etpassword.setText("");
		} else {
			if (username.trim().equals("")) {
				cf.show_toast("Please enter Username", 1);
				autousername.requestFocus();
				autousername.setText("");
			} else {
				if (password.trim().equals("")) {
					cf.show_toast("Please enter Password", 1);
					etpassword.requestFocus();
					etpassword.setText("");
				} else {
					login();
				}
			}
		}
	}

	private void login() {
		// TODO Auto-generated method stub
		
		if (cf.isInternetOn() == true) {
			cf.show_ProgressDialog("Checking user authentication..");
			new Thread() {
				String chkauth;
				public void run() {
					Looper.prepare();
					try
					{
						SoapObject request = new SoapObject(cf.NAMESPACE,"USERAUTHENTICATION");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("UserName",username);
						request.addProperty("Password",password);
						envelope.setOutputSoapObject(request);
						System.out.println("USERAUTHENTICATION request is " + request);
						
						HttpTransportSE androidHttpTransport = new HttpTransportSE(cf.URL);						
						androidHttpTransport.call(cf.NAMESPACE+ "USERAUTHENTICATION", envelope);
						SoapObject result = (SoapObject) envelope.getResponse();
						
						System.out.println("USERAUTHENTICATION result is"+ result);
						String status = String.valueOf(result.getProperty("Status"));
						System.out.println("status="+status);
						if(status.toLowerCase().equals("true"))
						{
							 db.CreateTable(1);
							 db.vi_db.execSQL("delete from " + db.Registration);
							 userid = String.valueOf(result.getProperty("Userid"));
							 firstname = String.valueOf(result.getProperty("FirstName"));
							 lastname = String.valueOf(result.getProperty("LastName"));
							 phonenum = String.valueOf(result.getProperty("Phoneno"));
							 email = String.valueOf(result.getProperty("Email"));
							 strusername = String.valueOf(result.getProperty("Username"));
							 strpassword = String.valueOf(result.getProperty("Password"));
							
							try
							{
								db.vi_db.execSQL("INSERT INTO "
										+ db.Registration
										+ " (firstname,lastname,phonenum,email,userid,username,password)"
										+ " VALUES ('" + cf.encode(firstname) + "','"
										+ cf.encode(lastname) + "','"
										+ cf.encode(phonenum) + "','"
										+ cf.encode(email) + "','"
										+ userid + "','"
										+ cf.encode(strusername) + "','"
										+ cf.encode(strpassword) + "')");
								show_handler = 1;
								handler.sendEmptyMessage(0);
								
							}catch (Exception e) {
								// TODO: handle exception
							}
							
						}
						else
						{  
							status_userid = String.valueOf(result.getProperty("Errormsg"));
							show_handler = 2;
							handler.sendEmptyMessage(0);
						}
					}catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
					catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}
				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network",1);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator",1);

						} else if (show_handler == 2) {
							show_handler = 0;
							cf.show_toast(status_userid, 1);
						}else if (show_handler == 1) {
							show_handler = 0;
							startActivity(new Intent(LoginActivity.this, HomeScreen.class));
							finish();
						}
					}
				};

			}.start();
		} else {
			cf.show_toast("Internet connection not available", 1);
	
		}
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// replaces the default 'Back' button action
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
