package idsoft.inspectiondepot.vehicleinspection;

import idsoft.inspectiondepot.vehicleinspection.supportclass.CommonFunction;
import idsoft.inspectiondepot.vehicleinspection.supportclass.CustomTextWatcher;
import idsoft.inspectiondepot.vehicleinspection.supportclass.DataBaseHelper;
import idsoft.inspectiondepot.vehicleinspection.supportclass.DataBaseHelper_statecounty;
import idsoft.inspectiondepot.vehicleinspection.supportclass.MakeandModel_DataBaseHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.SocketException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;


import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class VehicleInspection extends Activity {
	ArrayList<String> arraylistfilepah = new ArrayList<String>();
    int arraylistfilepahlength=0;
	RadioGroup rgpaintcondition, rgdents, rgrustproblems,
			rgappeartobeinaccident, rgdamagetotheframe, rgdamagetowindshield,
			rganyfluidleaks, rgengineoil, rgtransmissionfluid, rgdifferential,
			rgcoolant, rgbrakefluid, rgpowersteeringfluid, rgconditionoftires,
			rgconditionofvalvestems, rgcaster, rganyproblemwithsuspension,
			rganyproblemwithalignment, rgujoints, rgcvboots, rgshocks,
			rgballjoints, rgbushings, rglinkpins, rgrockandpinion, rgidler,
			rgcenterlink, rgexhaustsystemcomplete, rgemissioncontrolintact,
			rgdoallbelts, rgconditionofsparkplugs, rgdothebrakesgrab,
			rgdoescarshudder, rgarebrakelinings, rgconditionofbrakepads,
			rganyproblemswithhudraulicsystem, rgdoesparkingbrake,
			rgisbrakepedalpressureokay, rgmastercylinder, rgdrumsanddiscs,
			rgwheelcylinder, rgcalipters, rginstruments, rginteriorlights,
			rgheadlights, rgtaillights, rgturnsignals, rgbackuplights,
			rgbrakelights,rgemergencylights, rgsystemfailure,
			rgairconditioningsystem, rgheatingsystem, rgconditionofbattery,
			rgdoorlocks, rgdoallpoweroptionswork, rganydelaybetweenenginespeed,
			rgautomatictransmission;
	ProgressDialog pd;
	String strpaintcondition="", strdents="", strrustproblems="",
			strappeartobeinaccident="", strdamagetotheframe="",
			strdamagetowindshield="", stranyfluidleaks="", strengineoil="",
			strtransmissionfluid="", strdifferential="", strcoolant="", strbrakefluid="",
			strpowersteeringfluid="", strconditionoftires="",
			strconditionofvalvestems="", strcaster="", stranyproblemwithsuspension="",
			stranyproblemwithalignment="", strujoints="", strcvboots="", strshocks="",
			strballjoints="", strbushings="", strlinkpins="", strrockandpinion="",
			stridler="", strcenterlink="", strexhaustsystemcomplete="",
			stremissioncontrolintact="", strdoallbelts="", strconditionofsparkplugs="",
			strdothebrakesgrab="", strdoescarshudder="", strarebrakelinings="",
			strconditionofbrakepads="", stranyproblemswithhudraulicsystem="",
			strdoesparkingbrake="", strisbrakepedalpressureokay="",
			strmastercylinder="", strdrumsanddiscs="", strwheelcylinder="",
			strcalipters="", strinstruments="", strinteriorlights="", strheadlights="",
			strtaillights="", strturnsignals="", strbackuplights="", strbrakelights="",
			stremergencylights="", strsystemfailure="", strairconditioningsystem="",
			strheatingsystem="", strconditionofbattery="", strdoorlocks="",
			strdoallpoweroptionswork="", stranydelaybetweenenginespeed="",
			strautomatictransmission="", strwhowaspresentatinspection="",
			strdateofinspection="",	strinsurancecompany="", strvehiclenumber="", strvin="", strlicenseno="",
			strexpires="", strmodel="", strmake="", strvehicletype="",strdateoflastoilchange="",strdate;
	LinearLayout llgeneralinfoborder, llbodyandframe, llfluidlevels,
			llsuspension, llengineperformance, llbrakesystems,
			lloperationalaccessories, lladdaimage, llcoverpagelogo,lladdress,lldisplaypdf,llothermodel,llimagecount;
	ImageView plus, minus, plus1, minus1, plus3, minus3, plus4, minus4, plus5,
			minus5, plus6, minus6, plus7, minus7, plus8, minus8, plus9, minus9,plus10,minus10;
	RadioButton rbcar, rbbus, rbmotor, rbtruck, rbvan, rbvehicletypeother;
	EditText etfirstname, etlastname, etdateofinspection, etinsurancecompany,
			etvehicleno, etvin, etlicenseno, etexpires,
			etdateoflastoilchange, etcoverpagelogo, etinspectionaddress1,
			etinspectionaddress2, etzip, etcity,etpresentatinspectionother,etvehicletypeother,etname,etothermodel;
	EditText otherpresentatinspection, othervehicletype, otherpaintcondition,
			otherdents, otherrustproblems, otherengineoil,
			othertransmissionfluid, otherdifferential, othercoolant,
			otherbrakefluid, otherpowersteeringfluid, othercaster,
			otherujoints, othercvboots, othershocks, otherballjoints,
			otherbushings, otherlinkpins, otherrockandpinion, otheridler,
			othercenterlink, otherexhaustsystem, otheremissioncontrol,
			othermastercylinder, otherdrumsanddiscs, otherwheelcylinders,
			othercalipters, otherinstruments, otherinteriorlights,
			otherheadlights, othertaillights, otherturnsignals,
			otherbackuplights, otherbrakelights, otheremergencylights,
			othersystemfailurewarninglights, otherairconditioningsystem,
			otherheatingsystem, otherdoorlocks;     
	LinearLayout llpresentatinspection, llvehicletype, llpaintcondition,
			lldents, llrustproblems, llengineoil, lltransmissionfluid,
			lldifferential, llcoolant, llbrakefluid, llpowersteeringfluid,
			llcaster, llujoints, llcvboots, llshocks, llballjoints, llbushings,
			lllinkpins, llrockandpinion, llidler, llcenterlink,
			llexhaustsystem, llemissioncontrol, llmastercylinder,
			lldrumsanddiscs, llwheelcylinders, llcalipters, llinstruments,
			llinteriorlights, llheadlights, lltaillights, llturnsignals,
			llbackuplights, llbrakelights, llemergencylights,
			llsystemfailurewarninglights, llairconditioningsystem,
			llheatingsystem, lldoorlocks;
	Spinner spinnerstate,spinnercounty,spinnermake,spinnermodel;
    CommonFunction cf;
	String currentdate, statevalue = "false", strstate, strstateid, strcounty,
			strcountyid, state, stateid, county, countyid, city,
			imageidentifier, filePath2 = "", strelevationvalue,
			strcaptionvalue, tagfilepath, updatefilepath,
			strcoverpagelogofilename, tagelevation,tagcaption,order_result,makevalue = "false";
	ArrayAdapter<String> stateadapter,countyadapter,makeadapter,modeladapter;
	String[] arraystatename,arraystateid,arraycountyname,arraycountyid;
	DataBaseHelper db;
	Button ivcoverpagelogoclose;
	int show_handler,flag,propertycount,i;
	boolean countysetselection=false;
	public Uri CapturedImageURI;
	ImageView ivimageedit,ivcoverpagelogo,ivcoverpagelogoselectedimage;
	byte[] bytecoverpagelogo;
	MarshalBase64 marshal;
	Bitmap bitmapdb,bitmap;
	String[] array_elevation = { "--Select--", "Front View",
			"Right View", "Rear View", "Left View", "Interior View" };
	
	TableLayout.LayoutParams tlparams;
	TableRow.LayoutParams trparams;
	LinearLayout.LayoutParams rlparams, llparams;
	RelativeLayout.LayoutParams rlimageparams;
	TableLayout tldynamiclist;
	GraphicsView1 view;
	TextView tvdate,tvothermodel,tvimagecount;
	String UserName,UserId,UserEmail,path,image_result,modelid,makeid,strinspectionaddress1,strinspectionaddress2,strcity,strzip;
	String[] data,datasend,pdfpath,elevation_name,caption_name,file_name,arraymakeid,arraymakename,arraymodelid,arraymodelname;
	ScrollView sv,svchild;
	RelativeLayout rlnote;
	static String reportpath;
	CheckBox cbowner,cbagent,cbrepresentative,cbwhowaspresentother,cbaddressna,cbdateoflastoilchange;
	static VehicleInspection vl;
	String curdate,curtime;
	AlertDialog alertDialog;
	DataBaseHelper_statecounty db_sc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.vehicle_inspection);
		
		cf=new CommonFunction(this);
		db=new DataBaseHelper(this);
		db_sc=new DataBaseHelper_statecounty(this);
		
		vl=this;
		
		final Calendar c = Calendar.getInstance();
		final int year1 = c.get(Calendar.YEAR);
		int month1 = c.get(Calendar.MONTH);
		int day1 = c.get(Calendar.DAY_OF_MONTH);
		currentdate = (month1 + 1) + "/" + day1 + "/" + year1;
		System.out.println("The current date is" + currentdate);
		
		db.CreateTable(4);
		db.vi_db.execSQL("delete from " + db.AddAImage);
		
		db.CreateTable(1);
		Cursor cur = db.vi_db.rawQuery("select * from " + db.Registration,
				null);
		cur.moveToFirst();
		if (cur.getCount() >= 1) {
			UserName = cf.decode(cur.getString(cur.getColumnIndex("firstname")));
			UserName += " "+cf.decode(cur.getString(cur.getColumnIndex("lastname")));
			UserId = cf.decode(cur.getString(cur.getColumnIndex("userid")));
			UserEmail = cf.decode(cf.decode(cur.getString(cur
					.getColumnIndex("email"))));
	
		}
		cur.close();
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		curdate = dateFormat.format(date);
		System.out.println();
	 
		DateFormat dateFormat1 = new SimpleDateFormat("HH:mm:ss");
		   //get current date time with Calendar()
		Calendar cal = Calendar.getInstance();
		curtime = dateFormat1.format(cal.getTime());
		
		
		tldynamiclist = (TableLayout) findViewById(R.id.vehicleinspection_tablelayoutdynamic);
		view=(GraphicsView1) findViewById(R.id.vehicleinspection_signature);
		view.setDrawingCacheEnabled(true);
		tvdate=(TextView)findViewById(R.id.vehicleinspection_tvdate);
		tvimagecount=(TextView)findViewById(R.id.vehicleinspection_tvimagecount);
		tvothermodel=(TextView)findViewById(R.id.vehicleinspection_tvothermodel);
		etname=(EditText)findViewById(R.id.vehicleinspection_etname);
		etothermodel=(EditText)findViewById(R.id.vehicleinspection_etothermodel);
		llothermodel=(LinearLayout)findViewById(R.id.vehicleinspection_llothermodel);
		llimagecount=(LinearLayout)findViewById(R.id.vehicleinspection_linearlayoutnoofimages);
		
		tvdate.setText(currentdate);
		
		
		lldisplaypdf = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutdiaplaypdf);
		llgeneralinfoborder = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutgeneralinfoborder);
		lladdress = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutaddress);
		llbodyandframe = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutbodyandframe);
		llfluidlevels = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutfluidlevels);
		llsuspension = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutsuapension);
		llengineperformance = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutengineperformance);
		llbrakesystems = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutbrakesystems);
		lloperationalaccessories = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutoperationalaccessories);
		lladdaimage = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutaddaimage);
		llcoverpagelogo = (LinearLayout) findViewById(R.id.vehicleinspection_linearlayoutcoverpagelogo);
		llpresentatinspection = (LinearLayout) findViewById(R.id.vehicleinspection_llpresentatinspection);
		llvehicletype = (LinearLayout) findViewById(R.id.vehicleinspection_llvehicetypeother);
		llpaintcondition = (LinearLayout) findViewById(R.id.vehicleinspection_llpaintconditionother);
		lldents = (LinearLayout) findViewById(R.id.vehicleinspection_lldentsother);
		llrustproblems = (LinearLayout) findViewById(R.id.vehicleinspection_llrustproblemother);
		llengineoil = (LinearLayout) findViewById(R.id.vehicleinspection_llengineoilother);
		lltransmissionfluid = (LinearLayout) findViewById(R.id.vehicleinspection_lltransmissionfluidother);
		lldifferential = (LinearLayout) findViewById(R.id.vehicleinspection_lldifferentialother);
		llcoolant = (LinearLayout) findViewById(R.id.vehicleinspection_llcoolantother);
		llbrakefluid = (LinearLayout) findViewById(R.id.vehicleinspection_llbrakefluidother);
		llpowersteeringfluid = (LinearLayout) findViewById(R.id.vehicleinspection_llpowersteeringfluidother);
		llcaster = (LinearLayout) findViewById(R.id.vehicleinspection_llcasterother);
		llujoints = (LinearLayout) findViewById(R.id.vehicleinspection_llujointsother);
		llcvboots = (LinearLayout) findViewById(R.id.vehicleinspection_llcvbootsother);
		llshocks = (LinearLayout) findViewById(R.id.vehicleinspection_llshocksother);
		llballjoints = (LinearLayout) findViewById(R.id.vehicleinspection_llballjointsother);
		llbushings = (LinearLayout) findViewById(R.id.vehicleinspection_llbushingsother);
		lllinkpins = (LinearLayout) findViewById(R.id.vehicleinspection_lllinkpinspther);
		llrockandpinion = (LinearLayout) findViewById(R.id.vehicleinspection_llrockandpinionother);
		llidler = (LinearLayout) findViewById(R.id.vehicleinspection_llidlerother);
		llcenterlink = (LinearLayout) findViewById(R.id.vehicleinspection_llcenterlinkother);
		llexhaustsystem = (LinearLayout) findViewById(R.id.vehicleinspection_llexhaustsystemother);
		llemissioncontrol = (LinearLayout) findViewById(R.id.vehicleinspection_llemissioncontrolother);
		llmastercylinder = (LinearLayout) findViewById(R.id.vehicleinspection_llmastercylinderother);
		lldrumsanddiscs = (LinearLayout) findViewById(R.id.vehicleinspection_lldrumsanddiscsother);
		llwheelcylinders = (LinearLayout) findViewById(R.id.vehicleinspection_llwheelcylinderdrumbrakeother);
		llcalipters = (LinearLayout) findViewById(R.id.vehicleinspection_llcalipersother);
		llinstruments = (LinearLayout) findViewById(R.id.vehicleinspection_llinstrumentsother);
		llinteriorlights = (LinearLayout) findViewById(R.id.vehicleinspection_llinteriorlightsother);
		llheadlights = (LinearLayout) findViewById(R.id.vehicleinspection_llheadlightsother);
		lltaillights = (LinearLayout) findViewById(R.id.vehicleinspection_lltaillightsother);
		llturnsignals = (LinearLayout) findViewById(R.id.vehicleinspection_llturnsignalsother);
		llbackuplights = (LinearLayout) findViewById(R.id.vehicleinspection_llbackuplightsother);
		llbrakelights = (LinearLayout) findViewById(R.id.vehicleinspection_llbrakelightsother);
		llemergencylights = (LinearLayout) findViewById(R.id.vehicleinspection_llemergencyother);
		llsystemfailurewarninglights = (LinearLayout) findViewById(R.id.vehicleinspection_llsystemfailureother);
		llairconditioningsystem = (LinearLayout) findViewById(R.id.vehicleinspection_llairconditioningother);
		llheatingsystem = (LinearLayout) findViewById(R.id.vehicleinspection_llheatingsystemother);
		lldoorlocks = (LinearLayout) findViewById(R.id.vehicleinspection_lldoorlocksother);
		
		etfirstname= (EditText) findViewById(R.id.vehicleinspection_etfirstname);
		etlastname= (EditText) findViewById(R.id.vehicleinspection_etlastname);
		etdateofinspection= (EditText) findViewById(R.id.vehicleinspection_etdateofinspection);
		etinsurancecompany= (EditText) findViewById(R.id.vehicleinspection_etinsurancecompany);
		etvehicleno= (EditText) findViewById(R.id.vehicleinspection_etvehicleno);
		etvin= (EditText) findViewById(R.id.vehicleinspection_etvin);
		etlicenseno= (EditText) findViewById(R.id.vehicleinspection_etlicenseno);
		etexpires= (EditText) findViewById(R.id.vehicleinspection_etexpires);
		etdateoflastoilchange= (EditText) findViewById(R.id.vehicleinspection_etdateoflastoilchange);
		etcoverpagelogo= (EditText) findViewById(R.id.vehicleinspection_etcoverpagelogo);
		etinspectionaddress1= (EditText) findViewById(R.id.vehicleinspection_etinspaddress1);
		etinspectionaddress2= (EditText) findViewById(R.id.vehicleinspection_etinspaddress2);
		etcity= (EditText) findViewById(R.id.vehicleinspection_etcity);
		etzip= (EditText) findViewById(R.id.vehicleinspection_etzip);
		etpresentatinspectionother= (EditText) findViewById(R.id.vehicleinspection_etpresentatinspectionother);
		etvehicletypeother= (EditText) findViewById(R.id.vehicleinspection_etvehicletypeother);
		
		spinnerstate=(Spinner)findViewById(R.id.vehicleinspection_spinnerstate);
		spinnercounty=(Spinner)findViewById(R.id.vehicleinspection_spinnercounty);
		spinnermake=(Spinner)findViewById(R.id.vehicleinspection_spinnermake);
		spinnermodel=(Spinner)findViewById(R.id.vehicleinspection_spinnermodel);
		
		otherpresentatinspection = (EditText) findViewById(R.id.vehicleinspection_etpresentatinspectionother);
		othervehicletype = (EditText) findViewById(R.id.vehicleinspection_etvehicletypeother);
		otherpaintcondition = (EditText) findViewById(R.id.vehicleinspection_etpaintconditionother);
		otherdents = (EditText) findViewById(R.id.vehicleinspection_etdentsother);
		otherrustproblems = (EditText) findViewById(R.id.vehicleinspection_etrustproblemother);
		otherengineoil = (EditText) findViewById(R.id.vehicleinspection_etengineoilother);
		othertransmissionfluid = (EditText) findViewById(R.id.vehicleinspection_ettransmissionfluidother);
		otherdifferential = (EditText) findViewById(R.id.vehicleinspection_etdifferentialother);
		othercoolant = (EditText) findViewById(R.id.vehicleinspection_etcoolantother);
		otherbrakefluid = (EditText) findViewById(R.id.vehicleinspection_etbrakefluidother);
		otherpowersteeringfluid = (EditText) findViewById(R.id.vehicleinspection_etpowersteeringfluidother);
		othercaster = (EditText) findViewById(R.id.vehicleinspection_etcasterother);
		otherujoints = (EditText) findViewById(R.id.vehicleinspection_etujointsother);
		othercvboots = (EditText) findViewById(R.id.vehicleinspection_etcvbootsother);
		othershocks = (EditText) findViewById(R.id.vehicleinspection_etshocksother);
		otherballjoints = (EditText) findViewById(R.id.vehicleinspection_etballjointsother);
		otherbushings = (EditText) findViewById(R.id.vehicleinspection_etbushingsother);
		otherlinkpins = (EditText) findViewById(R.id.vehicleinspection_etlinkpinsother);
		otherrockandpinion = (EditText) findViewById(R.id.vehicleinspection_etrockandpinionother);
		otheridler = (EditText) findViewById(R.id.vehicleinspection_etidlerother);
		othercenterlink = (EditText) findViewById(R.id.vehicleinspection_etcenterlinkother);
		otherexhaustsystem = (EditText) findViewById(R.id.vehicleinspection_etexhaustsystemother);
		otheremissioncontrol = (EditText) findViewById(R.id.vehicleinspection_etemissioncontrolother);
		othermastercylinder = (EditText) findViewById(R.id.vehicleinspection_etmastercylinderother);
		otherdrumsanddiscs = (EditText) findViewById(R.id.vehicleinspection_etdrumsanddiscsother);
		otherwheelcylinders = (EditText) findViewById(R.id.vehicleinspection_etwheelcylinderdrumbrakeother);
		othercalipters = (EditText) findViewById(R.id.vehicleinspection_etcalipersother);
		otherinstruments = (EditText) findViewById(R.id.vehicleinspection_etinstrumentsother);
		otherinteriorlights = (EditText) findViewById(R.id.vehicleinspection_etinteriorlightsother);
		otherheadlights = (EditText) findViewById(R.id.vehicleinspection_etheadlightsother);
		othertaillights = (EditText) findViewById(R.id.vehicleinspection_ettaillightsother);
		otherturnsignals = (EditText) findViewById(R.id.vehicleinspection_etturnsignalsother);
		otherbackuplights = (EditText) findViewById(R.id.vehicleinspection_etbackuplightsother);
		otherbrakelights = (EditText) findViewById(R.id.vehicleinspection_etbrakelightsother);
		otheremergencylights = (EditText) findViewById(R.id.vehicleinspection_etemergencyother);
		othersystemfailurewarninglights = (EditText) findViewById(R.id.vehicleinspection_etsystemfailureother);
		otherairconditioningsystem = (EditText) findViewById(R.id.vehicleinspection_etairconditioningother);
		otherheatingsystem = (EditText) findViewById(R.id.vehicleinspection_etheatingsystemother);
		otherdoorlocks = (EditText) findViewById(R.id.vehicleinspection_etdoorlocksother);

		plus = (ImageView) findViewById(R.id.vehicleinspection_plus);
		minus = (ImageView) findViewById(R.id.vehicleinspection_minus);
		plus1 = (ImageView) findViewById(R.id.vehicleinspection_plus1);
		minus1 = (ImageView) findViewById(R.id.vehicleinspection_minus1);
		plus3 = (ImageView) findViewById(R.id.vehicleinspection_plus3);
		minus3 = (ImageView) findViewById(R.id.vehicleinspection_minus3);
		plus4 = (ImageView) findViewById(R.id.vehicleinspection_plus4);
		minus4 = (ImageView) findViewById(R.id.vehicleinspection_minus4);
		plus5 = (ImageView) findViewById(R.id.vehicleinspection_plus5);
		minus5 = (ImageView) findViewById(R.id.vehicleinspection_minus5);
		plus6 = (ImageView) findViewById(R.id.vehicleinspection_plus6);
		minus6 = (ImageView) findViewById(R.id.vehicleinspection_minus6);
		plus7 = (ImageView) findViewById(R.id.vehicleinspection_plus7);
		minus7 = (ImageView) findViewById(R.id.vehicleinspection_minus7);
		plus8 = (ImageView) findViewById(R.id.vehicleinspection_plus8);
		minus8 = (ImageView) findViewById(R.id.vehicleinspection_minus8);
		plus9 = (ImageView) findViewById(R.id.vehicleinspection_plus9);
		minus9 = (ImageView) findViewById(R.id.vehicleinspection_minus9);
		plus10 = (ImageView) findViewById(R.id.vehicleinspection_plus10);
		minus10 = (ImageView) findViewById(R.id.vehicleinspection_minus10);
		ivcoverpagelogo=(ImageView)findViewById(R.id.vehicleinspection_btncoverpagelogo);
		ivcoverpagelogoclose=(Button)findViewById(R.id.vehicleinspection_btncoverpagelogovlose);
		ivcoverpagelogoselectedimage=(ImageView)findViewById(R.id.vehicleinspection_coverpagelogoimage);
		
		cbowner=(CheckBox)findViewById(R.id.vehicleinspection_cbowber);
		cbrepresentative=(CheckBox)findViewById(R.id.vehicleinspection_cbrep);
		cbagent=(CheckBox)findViewById(R.id.vehicleinspection_cbagent);
		cbwhowaspresentother=(CheckBox)findViewById(R.id.vehicleinspection_cbwhowaspresentatinspectionother);
		cbaddressna=(CheckBox)findViewById(R.id.vehicleinspection_cbaddressna);
		cbdateoflastoilchange=(CheckBox)findViewById(R.id.vehicleinspection_cbdateoflastoil);
		rbcar=(RadioButton)findViewById(R.id.vehicleinspection_rbcar);
		rbbus=(RadioButton)findViewById(R.id.vehicleinspection_rbbus);
		rbmotor=(RadioButton)findViewById(R.id.vehicleinspection_rbmotor);
		rbtruck=(RadioButton)findViewById(R.id.vehicleinspection_rbtruck);
		rbvan=(RadioButton)findViewById(R.id.vehicleinspection_rbvan);
		rbvehicletypeother=(RadioButton)findViewById(R.id.vehicleinspection_rbvehicletypeothet);
		
		rgpaintcondition=(RadioGroup)findViewById(R.id.vehicleinspection_rbpaintcondition);
		rgdents=(RadioGroup)findViewById(R.id.vehicleinspection_rbdents);
		rgrustproblems=(RadioGroup)findViewById(R.id.vehicleinspection_rbrustproblem);
		rgappeartobeinaccident=(RadioGroup)findViewById(R.id.vehicleinspection_rbaccident);
		rgdamagetotheframe=(RadioGroup)findViewById(R.id.vehicleinspection_rbdamagetoframe);
		rgdamagetowindshield=(RadioGroup)findViewById(R.id.vehicleinspection_rbdamagetowindshield);
		rganyfluidleaks=(RadioGroup)findViewById(R.id.vehicleinspection_rbanyfluidleaks);
		rgengineoil=(RadioGroup)findViewById(R.id.vehicleinspection_rbengineoil);
		rgtransmissionfluid=(RadioGroup)findViewById(R.id.vehicleinspection_rbtransmissionfluid);
		rgdifferential=(RadioGroup)findViewById(R.id.vehicleinspection_rbdifferential);
		rgcoolant=(RadioGroup)findViewById(R.id.vehicleinspection_rbcoolant);
		rgbrakefluid=(RadioGroup)findViewById(R.id.vehicleinspection_rbbrakefluid);
		rgpowersteeringfluid=(RadioGroup)findViewById(R.id.vehicleinspection_rbpowersteeringfluid);
		rgconditionoftires=(RadioGroup)findViewById(R.id.vehicleinspection_rbconditionoftires);
		rgconditionofvalvestems=(RadioGroup)findViewById(R.id.vehicleinspection_rbconditionofvalvestems);
		rgcaster=(RadioGroup)findViewById(R.id.vehicleinspection_rbcaster);
		rganyproblemwithsuspension=(RadioGroup)findViewById(R.id.vehicleinspection_rbproblemswithsuspension);
		rganyproblemwithalignment=(RadioGroup)findViewById(R.id.vehicleinspection_rbproblemwithalignment);
		rgujoints=(RadioGroup)findViewById(R.id.vehicleinspection_rbujoints);
		rgcvboots=(RadioGroup)findViewById(R.id.vehicleinspection_rbcvboots);
		rgshocks=(RadioGroup)findViewById(R.id.vehicleinspection_rbshocks);
		rgballjoints=(RadioGroup)findViewById(R.id.vehicleinspection_rbballjoints);
		rgbushings=(RadioGroup)findViewById(R.id.vehicleinspection_rbbushings);
		rglinkpins=(RadioGroup)findViewById(R.id.vehicleinspection_rblinkpins);
		rgrockandpinion=(RadioGroup)findViewById(R.id.vehicleinspection_rbrockandpinion);
		rgidler=(RadioGroup)findViewById(R.id.vehicleinspection_rbidler);
		rgcenterlink=(RadioGroup)findViewById(R.id.vehicleinspection_rbcenterlink);
		rgexhaustsystemcomplete=(RadioGroup)findViewById(R.id.vehicleinspection_rbexhaustsystem);
		rgemissioncontrolintact=(RadioGroup)findViewById(R.id.vehicleinspection_rbemissioncontrol);
		rgdoallbelts=(RadioGroup)findViewById(R.id.vehicleinspection_rbdoallbelts);
		rgconditionofsparkplugs=(RadioGroup)findViewById(R.id.vehicleinspection_rbconditionofspark);
		rgdothebrakesgrab=(RadioGroup)findViewById(R.id.vehicleinspection_rbdoallbrakesgrab);
		rgdoescarshudder=(RadioGroup)findViewById(R.id.vehicleinspection_rbdoescarshudder);
		rgarebrakelinings=(RadioGroup)findViewById(R.id.vehicleinspection_rbarebrakeliningswithin);
		rgconditionofbrakepads=(RadioGroup)findViewById(R.id.vehicleinspection_rbconditionofbrakepods);
		rganyproblemswithhudraulicsystem=(RadioGroup)findViewById(R.id.vehicleinspection_rbanyproblemswithhydraulicsystems);
		rgdoesparkingbrake=(RadioGroup)findViewById(R.id.vehicleinspection_rbdoesparkingbrake);
		rgisbrakepedalpressureokay=(RadioGroup)findViewById(R.id.vehicleinspection_rbisbreakpedalpressureok);
		rgmastercylinder=(RadioGroup)findViewById(R.id.vehicleinspection_rbmastercylinder);
		rgdrumsanddiscs=(RadioGroup)findViewById(R.id.vehicleinspection_rbdrumsanddiscs);
		rgwheelcylinder=(RadioGroup)findViewById(R.id.vehicleinspection_rbwheelcylinderdrumbrakes);
		rgcalipters=(RadioGroup)findViewById(R.id.vehicleinspection_rbcalipers);
		rginstruments=(RadioGroup)findViewById(R.id.vehicleinspection_rbinstruments);
		rginteriorlights=(RadioGroup)findViewById(R.id.vehicleinspection_rbinteriorlights);
		rgheadlights=(RadioGroup)findViewById(R.id.vehicleinspection_rbheadlights);
		rgtaillights=(RadioGroup)findViewById(R.id.vehicleinspection_rbtaillights);
		rgturnsignals=(RadioGroup)findViewById(R.id.vehicleinspection_rbturnsignals);
		rgbackuplights=(RadioGroup)findViewById(R.id.vehicleinspection_rbbackuplights);
		rgbrakelights=(RadioGroup)findViewById(R.id.vehicleinspection_rbbrakelights);
		rgemergencylights=(RadioGroup)findViewById(R.id.vehicleinspection_rbemergency);
		rgsystemfailure=(RadioGroup)findViewById(R.id.vehicleinspection_rbsystemfailure);
		rgairconditioningsystem=(RadioGroup)findViewById(R.id.vehicleinspection_rbairconditioning);
		rgheatingsystem=(RadioGroup)findViewById(R.id.vehicleinspection_rbheatingsystem);
		rgconditionofbattery=(RadioGroup)findViewById(R.id.vehicleinspection_rbconditionofbattery);
		rgdoorlocks=(RadioGroup)findViewById(R.id.vehicleinspection_rbdoorlocks);
		rgdoallpoweroptionswork=(RadioGroup)findViewById(R.id.vehicleinspection_rbdoallpoweroptions);
		rganydelaybetweenenginespeed=(RadioGroup)findViewById(R.id.vehicleinspection_rbanydelaybetweenenginespeed);
		rgautomatictransmission=(RadioGroup)findViewById(R.id.vehicleinspection_rbautomatictransmission);
		
		sv=(ScrollView)findViewById(R.id.vehicleinspection_parentscrollview);
		svchild=(ScrollView)findViewById(R.id.vehicleinspection_childscrollview);
		rlnote=(RelativeLayout)findViewById(R.id.vehicleinspection_rlnote);
	/*	sv.scrollTo(0,0);
		sv.fullScroll(ScrollView.FOCUS_UP);*/
		
		view.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				sv.requestDisallowInterceptTouchEvent(true);
				svchild.requestDisallowInterceptTouchEvent(true);
				return false;
			}
		});
		
		rgpaintcondition.setOnCheckedChangeListener(new check(1));
		rgdents.setOnCheckedChangeListener(new check(2));
		rgrustproblems.setOnCheckedChangeListener(new check(3));
		rgappeartobeinaccident.setOnCheckedChangeListener(new check(4));
		rgdamagetotheframe.setOnCheckedChangeListener(new check(5));
		rgdamagetowindshield.setOnCheckedChangeListener(new check(6));
		rganyfluidleaks.setOnCheckedChangeListener(new check(7));
		rgengineoil.setOnCheckedChangeListener(new check(8));
		rgtransmissionfluid.setOnCheckedChangeListener(new check(9));
		rgdifferential.setOnCheckedChangeListener(new check(10));
		rgcoolant.setOnCheckedChangeListener(new check(11));
		rgbrakefluid.setOnCheckedChangeListener(new check(12));
		rgpowersteeringfluid.setOnCheckedChangeListener(new check(13));
		rgconditionoftires.setOnCheckedChangeListener(new check(14));
		rgconditionofvalvestems.setOnCheckedChangeListener(new check(15));
		rgcaster.setOnCheckedChangeListener(new check(16));
		rganyproblemwithsuspension.setOnCheckedChangeListener(new check(17));
		rganyproblemwithalignment.setOnCheckedChangeListener(new check(18));
		rgujoints.setOnCheckedChangeListener(new check(19));
		rgcvboots.setOnCheckedChangeListener(new check(20));
		rgshocks.setOnCheckedChangeListener(new check(21));
		rgballjoints.setOnCheckedChangeListener(new check(22));
		rgbushings.setOnCheckedChangeListener(new check(23));
		rglinkpins.setOnCheckedChangeListener(new check(24));
		rgrockandpinion.setOnCheckedChangeListener(new check(25));
		rgidler.setOnCheckedChangeListener(new check(26));
		rgcenterlink.setOnCheckedChangeListener(new check(27));
		rgexhaustsystemcomplete.setOnCheckedChangeListener(new check(28));
		rgemissioncontrolintact.setOnCheckedChangeListener(new check(29));
		rgdoallbelts.setOnCheckedChangeListener(new check(30));
		rgconditionofsparkplugs.setOnCheckedChangeListener(new check(31));
		rgdothebrakesgrab.setOnCheckedChangeListener(new check(32));
		rgdoescarshudder.setOnCheckedChangeListener(new check(33));
		rgarebrakelinings.setOnCheckedChangeListener(new check(34));
		rgconditionofbrakepads.setOnCheckedChangeListener(new check(35));
		rganyproblemswithhudraulicsystem.setOnCheckedChangeListener(new check(36));
		rgdoesparkingbrake.setOnCheckedChangeListener(new check(37));
		rgisbrakepedalpressureokay.setOnCheckedChangeListener(new check(38));
		rgmastercylinder.setOnCheckedChangeListener(new check(39));
		rgdrumsanddiscs.setOnCheckedChangeListener(new check(40));
		rgwheelcylinder.setOnCheckedChangeListener(new check(41));
		rgcalipters.setOnCheckedChangeListener(new check(42));
		rginstruments.setOnCheckedChangeListener(new check(43));
		rginteriorlights.setOnCheckedChangeListener(new check(44));
		rgheadlights.setOnCheckedChangeListener(new check(45));
		rgtaillights.setOnCheckedChangeListener(new check(46));
		rgturnsignals.setOnCheckedChangeListener(new check(47));
		rgbackuplights.setOnCheckedChangeListener(new check(48));
		rgbrakelights.setOnCheckedChangeListener(new check(49));
		rgemergencylights.setOnCheckedChangeListener(new check(50));
		rgsystemfailure.setOnCheckedChangeListener(new check(51));
		rgairconditioningsystem.setOnCheckedChangeListener(new check(52));
		rgheatingsystem.setOnCheckedChangeListener(new check(53));
		rgconditionofbattery.setOnCheckedChangeListener(new check(54));
		rgdoorlocks.setOnCheckedChangeListener(new check(55));
		rgdoallpoweroptionswork.setOnCheckedChangeListener(new check(56));
		rganydelaybetweenenginespeed.setOnCheckedChangeListener(new check(57));
		rgautomatictransmission.setOnCheckedChangeListener(new check(58));
		
		statevalue = LoadState();

		if (statevalue == "true") {
			stateadapter = new ArrayAdapter<String>(VehicleInspection.this,
					android.R.layout.simple_spinner_item, arraystatename);
			stateadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerstate.setAdapter(stateadapter);
		}

		spinnerstate.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strstate = spinnerstate.getSelectedItem().toString();
				int stateid = spinnerstate.getSelectedItemPosition();
				strstateid = arraystateid[stateid];
				if (!strstate.equals("--Select--")) {
					LoadCounty(strstateid);
					spinnercounty.setEnabled(true);
					
				} else {
					System.out.println("inside spinner state else");
					// spinnercounty.setAdapter(null);
					spinnercounty.setEnabled(false);
					arraycountyname = new String[0];
					countyadapter = new ArrayAdapter<String>(
							VehicleInspection.this,
							android.R.layout.simple_spinner_item,
							arraycountyname);
					countyadapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnercounty.setAdapter(countyadapter);
					
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		spinnercounty.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strcounty = spinnercounty.getSelectedItem().toString();
				int countyid = spinnercounty.getSelectedItemPosition();
				strcountyid = arraycountyid[countyid];
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		makevalue = LoadMake();

		if (makevalue == "true") {
			makeadapter = new ArrayAdapter<String>(VehicleInspection.this,
					android.R.layout.simple_spinner_item, arraymakename);
			makeadapter
					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnermake.setAdapter(makeadapter);
		}

		spinnermake.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strmake = spinnermake.getSelectedItem().toString();
				int id = spinnermake.getSelectedItemPosition();
				makeid = arraymakeid[id];
				if (!strmake.equals("--Select--")) {
					LoadModel(makeid);
					spinnermodel.setEnabled(true);
					
				} else {
					spinnermodel.setEnabled(false);
					arraymodelname = new String[0];
					modeladapter = new ArrayAdapter<String>(
							VehicleInspection.this,
							android.R.layout.simple_spinner_item,
							arraymodelname);
					modeladapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinnermodel.setAdapter(modeladapter);
					
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		spinnermodel.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				strmodel = spinnermodel.getSelectedItem().toString();
				int id = spinnermodel.getSelectedItemPosition();
				modelid = arraymodelid[id];
				if(strmodel.startsWith("Other"))
				{
					llothermodel.setVisibility(View.VISIBLE);
					etothermodel.setText("");
					tvothermodel.setText(strmodel);
				}
				else
				{
					llothermodel.setVisibility(View.GONE);
					etothermodel.setText("");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
		etzip.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				if(etzip.getText().toString().trim().length()==5)
				{
					spinnerstate.setSelection(0);
					Load_State_County_City(etzip);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});

	etfirstname.addTextChangedListener(new CustomTextWatcher(etfirstname));
	etlastname.addTextChangedListener(new CustomTextWatcher(etlastname));
	etinsurancecompany.addTextChangedListener(new CustomTextWatcher(etinsurancecompany));
	etpresentatinspectionother.addTextChangedListener(new CustomTextWatcher(etpresentatinspectionother));
	etvehicleno.addTextChangedListener(new CustomTextWatcher(etvehicleno));
	etvin.addTextChangedListener(new CustomTextWatcher(etvin));
	etlicenseno.addTextChangedListener(new CustomTextWatcher(etlicenseno));
	etexpires.addTextChangedListener(new CustomTextWatcher(etexpires));
	etvehicletypeother.addTextChangedListener(new CustomTextWatcher(etvehicletypeother));
	etinspectionaddress1.addTextChangedListener(new CustomTextWatcher(etinspectionaddress1));
	etinspectionaddress2.addTextChangedListener(new CustomTextWatcher(etinspectionaddress2));
	etcity.addTextChangedListener(new CustomTextWatcher(etcity));
	otherpaintcondition.addTextChangedListener(new CustomTextWatcher(otherpaintcondition));
	otherdents.addTextChangedListener(new CustomTextWatcher(otherdents));
	otherrustproblems.addTextChangedListener(new CustomTextWatcher(otherrustproblems));
	otherengineoil.addTextChangedListener(new CustomTextWatcher(otherengineoil));
	othertransmissionfluid.addTextChangedListener(new CustomTextWatcher(othertransmissionfluid));
	otherdifferential.addTextChangedListener(new CustomTextWatcher(otherdifferential));
	othercoolant.addTextChangedListener(new CustomTextWatcher(othercoolant));
	otherbrakefluid.addTextChangedListener(new CustomTextWatcher(otherbrakefluid));
	otherpowersteeringfluid.addTextChangedListener(new CustomTextWatcher(otherpowersteeringfluid));
	othercaster.addTextChangedListener(new CustomTextWatcher(othercaster));
	otherujoints.addTextChangedListener(new CustomTextWatcher(otherujoints));
	othercvboots.addTextChangedListener(new CustomTextWatcher(othercvboots));
	othershocks.addTextChangedListener(new CustomTextWatcher(othershocks));
	otherballjoints.addTextChangedListener(new CustomTextWatcher(otherballjoints));
	otherbushings.addTextChangedListener(new CustomTextWatcher(otherbushings));
	otherlinkpins.addTextChangedListener(new CustomTextWatcher(otherlinkpins));
	otherrockandpinion.addTextChangedListener(new CustomTextWatcher(otherrockandpinion));
	otheridler.addTextChangedListener(new CustomTextWatcher(otheridler));
	othercenterlink.addTextChangedListener(new CustomTextWatcher(othercenterlink));
	otherexhaustsystem.addTextChangedListener(new CustomTextWatcher(otherexhaustsystem));
	otheremissioncontrol.addTextChangedListener(new CustomTextWatcher(otheremissioncontrol));
	othermastercylinder.addTextChangedListener(new CustomTextWatcher(othermastercylinder));
	otherdrumsanddiscs.addTextChangedListener(new CustomTextWatcher(otherdrumsanddiscs));
	otherwheelcylinders.addTextChangedListener(new CustomTextWatcher(otherwheelcylinders));
	othercalipters.addTextChangedListener(new CustomTextWatcher(othercalipters));
	otherinstruments.addTextChangedListener(new CustomTextWatcher(otherinstruments));
	otherinteriorlights.addTextChangedListener(new CustomTextWatcher(otherinteriorlights));
	otherheadlights.addTextChangedListener(new CustomTextWatcher(otherheadlights));
	othertaillights.addTextChangedListener(new CustomTextWatcher(othertaillights));
	otherturnsignals.addTextChangedListener(new CustomTextWatcher(otherturnsignals));
	otherbackuplights.addTextChangedListener(new CustomTextWatcher(otherbackuplights));
	otherbrakelights.addTextChangedListener(new CustomTextWatcher(otherbrakelights));
	otheremergencylights.addTextChangedListener(new CustomTextWatcher(otheremergencylights));
	othersystemfailurewarninglights.addTextChangedListener(new CustomTextWatcher(othersystemfailurewarninglights));
	otherairconditioningsystem.addTextChangedListener(new CustomTextWatcher(otherairconditioningsystem));
	otherheatingsystem.addTextChangedListener(new CustomTextWatcher(otherheatingsystem));
	otherdoorlocks.addTextChangedListener(new CustomTextWatcher(otherdoorlocks));
	etname.addTextChangedListener(new CustomTextWatcher(etname));
	etothermodel.addTextChangedListener(new CustomTextWatcher(etothermodel));
		
	}
	/*@Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
         sv.scrollTo(0, 0);
    }*/
	class check implements OnCheckedChangeListener {
		int i;

		public check(int i) {
			// TODO Auto-generated constructor stub
			this.i = i;
		}

		public void onCheckedChanged(RadioGroup group, int checkedId) {
			// TODO Auto-generated method stub
			RadioButton checkedRadioButton = (RadioButton) group
					.findViewById(checkedId);
			boolean isChecked = checkedRadioButton.isChecked();
			if (isChecked) {
				switch (i) {
				case 1:
					strpaintcondition = checkedRadioButton.getText()
							.toString().trim();
					if(strpaintcondition.equals("Good"))
					{
						llpaintcondition.setVisibility(View.GONE);
						otherpaintcondition.setText("");
					}
					else if(strpaintcondition.equals("N/R"))
					{
						llpaintcondition.setVisibility(View.VISIBLE);
						otherpaintcondition.setText("");
					}
					else
					{
						llpaintcondition.setVisibility(View.GONE);
						otherpaintcondition.setText("");
					}
					break;
					
				case 2://Group for rgdents
					strdents = checkedRadioButton.getText()
							.toString().trim();
					if(strdents.equals("Good"))
					{
						lldents.setVisibility(View.GONE);
						otherdents.setText("");
					}
					else if(strdents.equals("N/R"))
					{
						lldents.setVisibility(View.VISIBLE);
						otherdents.setText("");
					}
					else
					{
						lldents.setVisibility(View.GONE);
						otherdents.setText("");
					}
					break;
					
				case 3:
					strrustproblems = checkedRadioButton.getText()
							.toString().trim();
					if(strrustproblems.equals("Present"))
					{
						llrustproblems.setVisibility(View.GONE);
						otherrustproblems.setText("");
					}
					else if(strrustproblems.equals("N/R"))
					{
						otherrustproblems.setText("");
						llrustproblems.setVisibility(View.VISIBLE);
					}
					else
					{
						llrustproblems.setVisibility(View.GONE);
						otherrustproblems.setText("");
					}
					break;
					
				case 4:
					strappeartobeinaccident = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 5:
					strdamagetotheframe = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 6:
					strdamagetowindshield = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 7:
					stranyfluidleaks = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 8:
					strengineoil = checkedRadioButton.getText()
							.toString().trim();
					if(strengineoil.equals("Good"))
					{
						llengineoil.setVisibility(View.GONE);
						otherengineoil.setText("");
					}
					else if(strengineoil.equals("N/R"))
					{
						llengineoil.setVisibility(View.VISIBLE);
						otherengineoil.setText("");
					}
					else
					{
						llengineoil.setVisibility(View.GONE);
						otherengineoil.setText("");
					}
					break;
					
				case 9:
					strtransmissionfluid = checkedRadioButton.getText()
							.toString().trim();
					if(strtransmissionfluid.equals("Good"))
					{
						lltransmissionfluid.setVisibility(View.GONE);
						othertransmissionfluid.setText("");
					}
					else if(strtransmissionfluid.equals("N/R"))
					{
						lltransmissionfluid.setVisibility(View.VISIBLE);
						othertransmissionfluid.setText("");
					}
					else
					{
						lltransmissionfluid.setVisibility(View.GONE);
						othertransmissionfluid.setText("");
					}
					break;
					
				case 10:
					strdifferential = checkedRadioButton.getText()
							.toString().trim();
					if(strdifferential.equals("Good"))
					{
						lldifferential.setVisibility(View.GONE);
						otherdifferential.setText("");
					}
					else if(strdifferential.equals("N/R"))
					{
						lldifferential.setVisibility(View.VISIBLE);
						otherdifferential.setText("");
					}
					else
					{
						lldifferential.setVisibility(View.GONE);
						otherdifferential.setText("");
					}
					break;
					
				case 11:
					strcoolant = checkedRadioButton.getText()
							.toString().trim();
					if(strcoolant.equals("Good"))
					{
						llcoolant.setVisibility(View.GONE);
						othercoolant.setText("");
					}
					else if(strcoolant.equals("N/R"))
					{
						llcoolant.setVisibility(View.VISIBLE);
						othercoolant.setText("");
					}
					else
					{
						llcoolant.setVisibility(View.GONE);
						othercoolant.setText("");
					}
					break;
					
				case 12:
					strbrakefluid = checkedRadioButton.getText()
							.toString().trim();
					if(strbrakefluid.equals("Good"))
					{
						llbrakefluid.setVisibility(View.GONE);
						otherbrakefluid.setText("");
					}
					else if(strbrakefluid.equals("N/R"))
					{
						llbrakefluid.setVisibility(View.VISIBLE);
						otherbrakefluid.setText("");
					}
					else
					{
						llbrakefluid.setVisibility(View.GONE);
						otherbrakefluid.setText("");
					}
					break;
					
				case 13:
					strpowersteeringfluid = checkedRadioButton.getText()
							.toString().trim();
					if(strpowersteeringfluid.equals("Good"))
					{
						llpowersteeringfluid.setVisibility(View.GONE);
						otherpowersteeringfluid.setText("");
					}
					else if(strpowersteeringfluid.equals("N/R"))
					{
						llpowersteeringfluid.setVisibility(View.VISIBLE);
						otherpowersteeringfluid.setText("");
					}
					else
					{
						llpowersteeringfluid.setVisibility(View.GONE);
						otherpowersteeringfluid.setText("");
					}
					break;
					
				case 14:
					strconditionoftires = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 15:
					strconditionofvalvestems = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 16:
					strcaster = checkedRadioButton.getText()
							.toString().trim();
					othercaster.setText("");
					if(strcaster.equals("Good"))
					{
						llcaster.setVisibility(View.GONE);
					}
					else if(strcaster.equals("N/R"))
					{
						llcaster.setVisibility(View.VISIBLE);
					}
					else
					{
						llcaster.setVisibility(View.GONE);
					}
					break;
					
				case 17:
					stranyproblemwithsuspension = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 18:
					stranyproblemwithalignment = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 19:
					strujoints = checkedRadioButton.getText()
							.toString().trim();
					otherujoints.setText("");
					if(strujoints.equals("Good"))
					{
						llujoints.setVisibility(View.GONE);
					}
					else if(strujoints.equals("N/R"))
					{
						llujoints.setVisibility(View.VISIBLE);
					}
					else
					{
						llujoints.setVisibility(View.GONE);
					}
					break;
					
				case 20:
					strcvboots = checkedRadioButton.getText()
							.toString().trim();
					othercvboots.setText("");
					if(strcvboots.equals("Good"))
					{
						llcvboots.setVisibility(View.GONE);
					}
					else if(strcvboots.equals("N/R"))
					{
						llcvboots.setVisibility(View.VISIBLE);
					}
					else
					{
						llcvboots.setVisibility(View.GONE);
					}
					break;
					
				case 21:
					strshocks = checkedRadioButton.getText()
							.toString().trim();
					othershocks.setText("");
					if(strshocks.equals("Good"))
					{
						llshocks.setVisibility(View.GONE);
					}
					else if(strshocks.equals("N/R"))
					{
						llshocks.setVisibility(View.VISIBLE);
					}
					else
					{
						llshocks.setVisibility(View.GONE);
					}
					break;
					
				case 22:
					strballjoints = checkedRadioButton.getText()
							.toString().trim();
					otherballjoints.setText("");
					if(strballjoints.equals("Good"))
					{
						llballjoints.setVisibility(View.GONE);
					}
					else if(strballjoints.equals("N/R"))
					{
						llballjoints.setVisibility(View.VISIBLE);
					}
					else
					{
						llballjoints.setVisibility(View.GONE);
					}
					break;
					
				case 23:
					strbushings = checkedRadioButton.getText()
							.toString().trim();
					otherbushings.setText("");
					if(strbushings.equals("Good"))
					{
						llbushings.setVisibility(View.GONE);
					}
					else if(strbushings.equals("N/R"))
					{
						llbushings.setVisibility(View.VISIBLE);
					}
					else
					{
						llbushings.setVisibility(View.GONE);
					}
					break;
					
				case 24:
					strlinkpins = checkedRadioButton.getText()
							.toString().trim();
					otherlinkpins.setText("");
					if(strlinkpins.equals("Good"))
					{
						lllinkpins.setVisibility(View.GONE);
					}
					else if(strlinkpins.equals("N/R"))
					{
						lllinkpins.setVisibility(View.VISIBLE);
					}
					else
					{
						lllinkpins.setVisibility(View.GONE);
					}
					break;
					
				case 25:
					strrockandpinion = checkedRadioButton.getText()
							.toString().trim();
					otherrockandpinion.setText("");
					if(strrockandpinion.equals("Good"))
					{
						llrockandpinion.setVisibility(View.GONE);
					}
					else if(strrockandpinion.equals("N/R"))
					{
						llrockandpinion.setVisibility(View.VISIBLE);
					}
					else
					{
						llrockandpinion.setVisibility(View.GONE);
					}
					break;
					
				case 26:
					stridler = checkedRadioButton.getText()
							.toString().trim();
					otheridler.setText("");
					if(stridler.equals("Good"))
					{
						llidler.setVisibility(View.GONE);
					}
					else if(stridler.equals("N/R"))
					{
						llidler.setVisibility(View.VISIBLE);
					}
					else
					{
						llidler.setVisibility(View.GONE);
					}
					break;
					
				case 27:
					strcenterlink = checkedRadioButton.getText()
							.toString().trim();
					othercenterlink.setText("");
					if(strcenterlink.equals("Good"))
					{
						llcenterlink.setVisibility(View.GONE);
					}
					else if(strcenterlink.equals("N/R"))
					{
						llcenterlink.setVisibility(View.VISIBLE);
					}
					else
					{
						llcenterlink.setVisibility(View.GONE);
					}
					break;
					
				case 28:
					strexhaustsystemcomplete = checkedRadioButton.getText()
							.toString().trim();
					otherexhaustsystem.setText("");
					if(strexhaustsystemcomplete.equals("Good"))
					{
						llexhaustsystem.setVisibility(View.GONE);
					}
					else if(strexhaustsystemcomplete.equals("N/R"))
					{
						llexhaustsystem.setVisibility(View.VISIBLE);
					}
					else
					{
						llexhaustsystem.setVisibility(View.GONE);
					}
					break;
					
				case 29:
					stremissioncontrolintact = checkedRadioButton.getText()
							.toString().trim();
					otheremissioncontrol.setText("");
					if(stremissioncontrolintact.equals("Good"))
					{
						llemissioncontrol.setVisibility(View.GONE);
					}
					else if(stremissioncontrolintact.equals("N/R"))
					{
						llemissioncontrol.setVisibility(View.VISIBLE);
					}
					else
					{
						llemissioncontrol.setVisibility(View.GONE);
					}
					break;
					
				case 30:
					strdoallbelts = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 31:
					strconditionofsparkplugs = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 32:
					strdothebrakesgrab = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 33:
					strdoescarshudder = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 34:
					strarebrakelinings = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 35:
					strconditionofbrakepads = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 36:
					stranyproblemswithhudraulicsystem = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 37:
					strdoesparkingbrake = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 38:
					strisbrakepedalpressureokay = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 39:
					strmastercylinder = checkedRadioButton.getText()
							.toString().trim();
					othermastercylinder.setText("");
					if(strmastercylinder.equals("Good"))
					{
						llmastercylinder.setVisibility(View.GONE);
					}
					else if(strmastercylinder.equals("N/R"))
					{
						llmastercylinder.setVisibility(View.VISIBLE);
					}
					else
					{
						llmastercylinder.setVisibility(View.GONE);
					}
					break;
					
				case 40:
					strdrumsanddiscs = checkedRadioButton.getText()
							.toString().trim();
					otherdrumsanddiscs.setText("");
					if(strdrumsanddiscs.equals("Good"))
					{
						lldrumsanddiscs.setVisibility(View.GONE);
					}
					else if(strdrumsanddiscs.equals("N/R"))
					{
						lldrumsanddiscs.setVisibility(View.VISIBLE);
					}
					else
					{
						lldrumsanddiscs.setVisibility(View.GONE);
					}
					break;
					
				case 41:
					strwheelcylinder = checkedRadioButton.getText()
							.toString().trim();
					otherwheelcylinders.setText("");
					if(strwheelcylinder.equals("Good"))
					{
						llwheelcylinders.setVisibility(View.GONE);
					}
					else if(strwheelcylinder.equals("N/R"))
					{
						llwheelcylinders.setVisibility(View.VISIBLE);
					}
					else
					{
						llwheelcylinders.setVisibility(View.GONE);
					}
					break;
					
				case 42:
					strcalipters = checkedRadioButton.getText()
							.toString().trim();
					othercalipters.setText("");
					if(strcalipters.equals("Good"))
					{
						llcalipters.setVisibility(View.GONE);
					}
					else if(strcalipters.equals("N/R"))
					{
						llcalipters.setVisibility(View.VISIBLE);
					}
					else
					{
						llcalipters.setVisibility(View.GONE);
					}
					break;
					
				case 43:
					strinstruments = checkedRadioButton.getText()
							.toString().trim();
					otherinstruments.setText("");
					if(strinstruments.equals("Good"))
					{
						llinstruments.setVisibility(View.GONE);
					}
					else if(strinstruments.equals("N/R"))
					{
						llinstruments.setVisibility(View.VISIBLE);
					}
					else
					{
						llinstruments.setVisibility(View.GONE);
					}
					break;
					
				case 44:
					strinteriorlights = checkedRadioButton.getText()
							.toString().trim();
					otherinteriorlights.setText("");
					if(strinteriorlights.equals("Good"))
					{
						llinteriorlights.setVisibility(View.GONE);
					}
					else if(strinteriorlights.equals("N/R"))
					{
						llinteriorlights.setVisibility(View.VISIBLE);
					}
					else
					{
						llinteriorlights.setVisibility(View.GONE);
					}
					break;
					
				case 45:
					strheadlights = checkedRadioButton.getText()
							.toString().trim();
					otherheadlights.setText("");
					if(strheadlights.equals("Good"))
					{
						llheadlights.setVisibility(View.GONE);
					}
					else if(strheadlights.equals("N/R"))
					{
						llheadlights.setVisibility(View.VISIBLE);
					}
					else
					{
						llheadlights.setVisibility(View.GONE);
					}
					break;
					
				case 46:
					strtaillights = checkedRadioButton.getText()
							.toString().trim();
					othertaillights.setText("");
					if(strtaillights.equals("Good"))
					{
						lltaillights.setVisibility(View.GONE);
					}
					else if(strtaillights.equals("N/R"))
					{
						lltaillights.setVisibility(View.VISIBLE);
					}
					else
					{
						lltaillights.setVisibility(View.GONE);
					}
					break;
					
				case 47:
					strturnsignals = checkedRadioButton.getText()
							.toString().trim();
					otherturnsignals.setText("");
					if(strturnsignals.equals("Good"))
					{
						llturnsignals.setVisibility(View.GONE);
					}
					else if(strturnsignals.equals("N/R"))
					{
						llturnsignals.setVisibility(View.VISIBLE);
					}
					else
					{
						llturnsignals.setVisibility(View.GONE);
					}
					break;
					
				case 48:
					strbackuplights = checkedRadioButton.getText()
							.toString().trim();
					otherbackuplights.setText("");
					if(strbackuplights.equals("Good"))
					{
						llbackuplights.setVisibility(View.GONE);
					}
					else if(strbackuplights.equals("N/R"))
					{
						llbackuplights.setVisibility(View.VISIBLE);
					}
					else
					{
						llbackuplights.setVisibility(View.GONE);
					}
					break;
					
				case 49:
					strbrakelights = checkedRadioButton.getText()
							.toString().trim();
					otherbrakelights.setText("");
					if(strbrakelights.equals("Good"))
					{
						llbrakelights.setVisibility(View.GONE);
					}
					else if(strbrakelights.equals("N/R"))
					{
						llbrakelights.setVisibility(View.VISIBLE);
					}
					else
					{
						llbrakelights.setVisibility(View.GONE);
					}
					break;
					
				case 50:
					stremergencylights = checkedRadioButton.getText()
							.toString().trim();
					otheremergencylights.setText("");
					if(stremergencylights.equals("Good"))
					{
						llemergencylights.setVisibility(View.GONE);
					}
					else if(stremergencylights.equals("N/R"))
					{
						llemergencylights.setVisibility(View.VISIBLE);
					}
					else
					{
						llemergencylights.setVisibility(View.GONE);
					}
					break;
					
				case 51:
					strsystemfailure = checkedRadioButton.getText()
							.toString().trim();
					othersystemfailurewarninglights.setText("");
					if(strsystemfailure.equals("Good"))
					{
						llsystemfailurewarninglights.setVisibility(View.GONE);
					}
					else if(strsystemfailure.equals("N/R"))
					{
						llsystemfailurewarninglights.setVisibility(View.VISIBLE);
					}
					else
					{
						llsystemfailurewarninglights.setVisibility(View.GONE);
					}
					break;
					
				case 52:
					strairconditioningsystem = checkedRadioButton.getText()
							.toString().trim();
					otherairconditioningsystem.setText("");
					if(strairconditioningsystem.equals("Good"))
					{
						llairconditioningsystem.setVisibility(View.GONE);
					}
					else if(strairconditioningsystem.equals("N/R"))
					{
						llairconditioningsystem.setVisibility(View.VISIBLE);
					}
					else
					{
						llairconditioningsystem.setVisibility(View.GONE);
					}
					break;
					
				case 53:
					strheatingsystem = checkedRadioButton.getText()
							.toString().trim();
					otherheatingsystem.setText("");
					if(strheatingsystem.equals("Good"))
					{
						llheatingsystem.setVisibility(View.GONE);
					}
					else if(strheatingsystem.equals("N/R"))
					{
						llheatingsystem.setVisibility(View.VISIBLE);
					}
					else
					{
						llheatingsystem.setVisibility(View.GONE);
					}
					break;
					
				case 54:
					strconditionofbattery = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 55:
					strdoorlocks = checkedRadioButton.getText()
							.toString().trim();
					otherdoorlocks.setText("");
					if(strdoorlocks.equals("Good"))
					{
						lldoorlocks.setVisibility(View.GONE);
					}
					else if(strdoorlocks.equals("N/R"))
					{
						lldoorlocks.setVisibility(View.VISIBLE);
					}
					else
					{
						lldoorlocks.setVisibility(View.GONE);
					}
					break;
					
				case 56:
					strdoallpoweroptionswork = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 57:
					stranydelaybetweenenginespeed = checkedRadioButton.getText()
							.toString().trim();
					break;
					
				case 58:
					strautomatictransmission = checkedRadioButton.getText()
							.toString().trim();
					break;
				}
			}
		}
	}

	public void clicker(View v) {
		switch (v.getId()) {
		case R.id.vehicleinspection_home:
			
			db.CreateTable(4);
			db.vi_db.execSQL("delete from " + db.AddAImage);
			
			Intent intenthome = new Intent(VehicleInspection.this,
					HomeScreen.class);
			startActivity(intenthome);
			finish();
			break;

		case R.id.vehicleinspection_submit:
			check_validation();
			break;
			
		case R.id.vehicleinspection_clearimage:
			view.setDrawingCacheEnabled(true);
			view.clear();
			view.setDrawingCacheEnabled(false);
			GraphicsView1.value=0;
			break;

		case R.id.vehicleinspection_plus:
			llgeneralinfoborder.setVisibility(View.VISIBLE);
			plus.setVisibility(View.GONE);
			minus.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus:
			llgeneralinfoborder.setVisibility(View.GONE);
			plus.setVisibility(View.VISIBLE);
			minus.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus1:
			llbodyandframe.setVisibility(View.VISIBLE);
			plus1.setVisibility(View.GONE);
			minus1.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus1:
			llbodyandframe.setVisibility(View.GONE);
			plus1.setVisibility(View.VISIBLE);
			minus1.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus3:
			llfluidlevels.setVisibility(View.VISIBLE);
			plus3.setVisibility(View.GONE);
			minus3.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus3:
			llfluidlevels.setVisibility(View.GONE);
			plus3.setVisibility(View.VISIBLE);
			minus3.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus4:
			llsuspension.setVisibility(View.VISIBLE);
			plus4.setVisibility(View.GONE);
			minus4.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus4:
			llsuspension.setVisibility(View.GONE);
			plus4.setVisibility(View.VISIBLE);
			minus4.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus5:
			llengineperformance.setVisibility(View.VISIBLE);
			plus5.setVisibility(View.GONE);
			minus5.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus5:
			llengineperformance.setVisibility(View.GONE);
			plus5.setVisibility(View.VISIBLE);
			minus5.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus6:
			llbrakesystems.setVisibility(View.VISIBLE);
			plus6.setVisibility(View.GONE);
			minus6.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus6:
			llbrakesystems.setVisibility(View.GONE);
			plus6.setVisibility(View.VISIBLE);
			minus6.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus7:
			lloperationalaccessories.setVisibility(View.VISIBLE);
			plus7.setVisibility(View.GONE);
			minus7.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus7:
			lloperationalaccessories.setVisibility(View.GONE);
			plus7.setVisibility(View.VISIBLE);
			minus7.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus8:
			lladdaimage.setVisibility(View.VISIBLE);
			plus8.setVisibility(View.GONE);
			minus8.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus8:
			lladdaimage.setVisibility(View.GONE);
			plus8.setVisibility(View.VISIBLE);
			minus8.setVisibility(View.GONE);
			break;

		case R.id.vehicleinspection_plus9:
			llcoverpagelogo.setVisibility(View.VISIBLE);
			plus9.setVisibility(View.GONE);
			minus9.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus9:
			llcoverpagelogo.setVisibility(View.GONE);
			plus9.setVisibility(View.VISIBLE);
			minus9.setVisibility(View.GONE);
			break;
			
		case R.id.vehicleinspection_plus10:
			lladdress.setVisibility(View.VISIBLE);
			plus10.setVisibility(View.GONE);
			minus10.setVisibility(View.VISIBLE);
			break;

		case R.id.vehicleinspection_minus10:
			lladdress.setVisibility(View.GONE);
			plus10.setVisibility(View.VISIBLE);
			minus10.setVisibility(View.GONE);
			break;
			
		case R.id.vehicleinspection_btndateofinspection:
			showDialogDate(etdateofinspection);
			break;
			
		case R.id.vehicleinspection_btnexpires:
			showDialogDate(etexpires);
			break;
			
		case R.id.vehicleinspection_btndateoflastoilchange:
			showDialogDate(etdateoflastoilchange);
			break;
			
		case R.id.vehicleinspection_cbowber:
			System.out.println("owner");

			break;
			
		case R.id.vehicleinspection_cbrep:
//			if(cbrepresentative.isChecked())
//			{
//				cbrepresentative.setChecked(false);
//			}
//			else
//			{
//				cbrepresentative.setChecked(true);
//			}
			break;
			
		case R.id.vehicleinspection_cbagent:
//			if(cbagent.isChecked())
//			{
//				cbagent.setChecked(false);
//			}
//			else
//			{
//				cbagent.setChecked(true);
//			}
			break;
			
		case R.id.vehicleinspection_cbwhowaspresentatinspectionother:
			if(cbwhowaspresentother.isChecked())
			{
//				cbwhowaspresentother.setChecked(false);
				llpresentatinspection.setVisibility(View.VISIBLE);
			}
			else
			{
//				cbwhowaspresentother.setChecked(true);
				llpresentatinspection.setVisibility(View.GONE);
				etpresentatinspectionother.setText("");
			}
			break;
			
		case R.id.vehicleinspection_cbaddressna:
			if(((CompoundButton) findViewById(R.id.vehicleinspection_cbaddressna)).isChecked())
			{
				etinspectionaddress1.setText("");
				etinspectionaddress2.setText("");
				etzip.setText("");
				etcity.setText("");
				spinnerstate.setSelection(0);
				lladdress.setVisibility(View.GONE);
				plus1.setVisibility(View.VISIBLE);
				minus1.setVisibility(View.GONE);
			}
			else
			{
				lladdress.setVisibility(View.VISIBLE);
				plus1.setVisibility(View.GONE);
				minus1.setVisibility(View.VISIBLE);
			}
			break;
			
		case R.id.vehicleinspection_cbdateoflastoil:
			if(cbdateoflastoilchange.isChecked())
			{
				etdateoflastoilchange.setText("Not Determined");
			}
			else
			{
				etdateoflastoilchange.setText("");
			}
			break;
			
		case R.id.vehicleinspection_rbcar:
			strvehicletype="Car";
			llvehicletype.setVisibility(View.GONE);
			rbcar.setChecked(true);
			rbbus.setChecked(false);
			rbmotor.setChecked(false);
			rbtruck.setChecked(false);
			rbvan.setChecked(false);
			rbvehicletypeother.setChecked(false);
			etvehicletypeother.setText("");
			break;
			
		case R.id.vehicleinspection_rbbus:
			strvehicletype="Bus";
			llvehicletype.setVisibility(View.GONE);
			rbcar.setChecked(false);
			rbbus.setChecked(true);
			rbmotor.setChecked(false);
			rbtruck.setChecked(false);
			rbvan.setChecked(false);
			rbvehicletypeother.setChecked(false);
			etvehicletypeother.setText("");
			break;
			
		case R.id.vehicleinspection_rbmotor:
			strvehicletype="Motor";
			llvehicletype.setVisibility(View.GONE);
			rbcar.setChecked(false);
			rbbus.setChecked(false);
			rbmotor.setChecked(true);
			rbtruck.setChecked(false);
			rbvan.setChecked(false);
			rbvehicletypeother.setChecked(false);
			etvehicletypeother.setText("");
			break;
			
		case R.id.vehicleinspection_rbtruck:
			strvehicletype="Truck";
			llvehicletype.setVisibility(View.GONE);
			rbcar.setChecked(false);
			rbbus.setChecked(false);
			rbmotor.setChecked(false);
			rbtruck.setChecked(true);
			rbvan.setChecked(false);
			rbvehicletypeother.setChecked(false);
			etvehicletypeother.setText("");
			break;
			
		case R.id.vehicleinspection_rbvan:
			strvehicletype="Van";
			llvehicletype.setVisibility(View.GONE);
			rbcar.setChecked(false);
			rbbus.setChecked(false);
			rbmotor.setChecked(false);
			rbtruck.setChecked(false);
			rbvan.setChecked(true);
			rbvehicletypeother.setChecked(false);
			etvehicletypeother.setText("");
			break;
			
		case R.id.vehicleinspection_rbvehicletypeothet:
			strvehicletype="Other";
			llvehicletype.setVisibility(View.VISIBLE);
			rbcar.setChecked(false);
			rbbus.setChecked(false);
			rbmotor.setChecked(false);
			rbtruck.setChecked(false);
			rbvan.setChecked(false);
			rbvehicletypeother.setChecked(true);
			etvehicletypeother.setText("");
			break;
			
		case R.id.vehicleinspection_btncoverpagelogo:
			Gallery_Camera_Dialog();
			break;
			
		case R.id.vehicleinspection_btncoverpagelogovlose:
			final Dialog dialog3 = new Dialog(VehicleInspection.this);
			dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog3.setContentView(R.layout.confirm_delete);
			dialog3.setCancelable(false);
			Button btnyes = (Button) dialog3
					.findViewById(R.id.confirmdelete_yes);
			Button btnno = (Button) dialog3
					.findViewById(R.id.confirmdelete_no);
			TextView tvtext = (TextView) dialog3
					.findViewById(R.id.confirmdelete_tvtext);
			
			tvtext.setText("the Cover page logo?");
			
			btnyes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					ivcoverpagelogo.setVisibility(View.VISIBLE);
					ivcoverpagelogoclose.setVisibility(View.GONE);
					etcoverpagelogo.setText("");
					ivcoverpagelogoselectedimage.setImageBitmap(null);
					ivcoverpagelogoselectedimage.setVisibility(View.GONE);
					dialog3.dismiss();
					cf.show_toast("Cover page logo deleted successfully",1);
				}
			});

			btnno.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dialog3.dismiss();
				}
			});

			dialog3.show();
			break;
			
		case R.id.vehicleinspection_galleryaddaimage:
			db.CreateTable(4);
			Cursor cur2=db.vi_db.rawQuery("select * from "+db.AddAImage, null);
			int count=cur2.getCount();
			System.out.println("count"+count);
			if(count==30)
			{
				cf.show_toast("You cannot upload images more than 30",1);
			}
			else
			{
				Gallery_Open();
			}
			break;

		case R.id.vehicleinspection_cameraaddaimage:
			db.CreateTable(4);
			Cursor cur1=db.vi_db.rawQuery("select * from "+db.AddAImage, null);
			int count1=cur1.getCount();
			System.out.println("cameracount"+count1);
			if(count1==30)
			{
				cf.show_toast("You cannot upload images more than 30",1);
			}
			else
			{
				Camera_Open();
			}
			break;
			
		}

	}
	
	
	
	private void Gallery_Open() {
		imageidentifier = "addaimage";
		Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
		i.setType("image/*");
		startActivityForResult(i, 0);
	}

	private void Camera_Open() {
		imageidentifier = "addaimage";
		String fileName = "temp.jpg";
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE, fileName);
		CapturedImageURI = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
		startActivityForResult(intent, 1);
	}
	
	private void Gallery_Camera_Dialog() {
		System.out.println("coverpage logo clicked");
		final Dialog dialog = new Dialog(VehicleInspection.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alert);
		dialog.setCancelable(false);
		// dialog.setCanceledOnTouchOutside(true);
		Button btnchoosefromgallery = (Button) dialog
				.findViewById(R.id.alert_choosefromgallery);
		Button btntakeapicturefromcamera = (Button) dialog
				.findViewById(R.id.alert_takeapicturefromcamera);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.alert_helpclose);
		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		btnchoosefromgallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				imageidentifier = "uploadaphoto";
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
				i.setType("image/*");
				startActivityForResult(i, 0);

			}
		});
		btntakeapicturefromcamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				imageidentifier = "uploadaphoto";
				String fileName = "temp.jpg";
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				CapturedImageURI = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, CapturedImageURI);
				startActivityForResult(intent, 1);
			}
		});
		dialog.show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
//		String selectedImagePath;
//		System.gc();
		if (requestCode == 0 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String filePath = cursor.getString(columnIndex);

			File f = new File(filePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			System.out.println("The file length is" + length);
			System.out.println("The file length in kb is" + kb);
			System.out.println("The file length in mb is" + mb);

			if (mb >= 2) {
				cf.show_toast("File size exceeds!! Too large to attach",1);
			} else {
				if (imageidentifier == "uploadaphoto") {
					Bitmap bm = decodeFile(filePath);
					
					System.out.println("The bitmap is "+bm);
					if(bm==null)
					{
						System.out.println("Inside if");
						cf.show_toast("File corrupted!! Cant able to attach",1);
					}
					else
					{
						Call_UploadPhoto_Dialog(filePath);
					}
					

				}

				else if (imageidentifier == "addaimage") {
				try
				{
					boolean filecheck=false;
					arraylistfilepahlength=arraylistfilepah.size();
					System.out.println("array list file length "+arraylistfilepahlength);
					if(arraylistfilepahlength==0)
					{
						filecheck=true;
					}
					else
					{
						if(arraylistfilepah.contains(filePath))
						{
							System.out.println("inside arraylist contains if");
							filecheck=false;
						}
						else
						{
							System.out.println("inside arraylist contains else");
							filecheck=Check_Duplicate_Image(filePath);
						}
					}
					
					System.out.println("filecheck value is "+filecheck);
					
					if(filecheck)
					{
						bitmapdb = decodeFile(filePath);
						
						System.out.println("The bitmap is "+bitmapdb);
						if(bitmapdb==null)
						{
							System.out.println("Inside if");
							cf.show_toast("File corrupted!! Cant able to attach",1);
						}
						else
						{
							
								Call_Elevationdialog(filePath);

						}
					}
					else
					{
						cf.show_toast("Image already selected, Please choose different one",1);
					}
			}
			catch (OutOfMemoryError e) {
				// TODO: handle exception
				System.out.println("Out of memory error "+e.getMessage());
				cf.show_toast("File size exceeds!! Too large to attach",1);
			}
			}
			
		}

		} else if (requestCode == 1 && resultCode == RESULT_OK) {
			Bitmap bitmapdb=null;
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = managedQuery(CapturedImageURI, projection, null,
					null, null);
			int column_index_data = cursor
					.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			String capturedImageFilePath = cursor.getString(column_index_data);
			String selectedImagePath = capturedImageFilePath;

			File f = new File(selectedImagePath);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			System.out.println("The file length is" + length);
			System.out.println("The file length in kb is" + kb);
			System.out.println("The file length in mb is" + mb);

			if (mb >= 2) {
				cf.show_toast("File size exceeds!! Too large to attach",1);
			} else {
//				if(bitmapdb!=null)
//				{
//					bitmapdb.recycle();
//				}
				try
				{
//					bitmapdb = BitmapFactory.decodeFile(selectedImagePath);
					bitmapdb = decodeFile(selectedImagePath);
//				}
//				catch (OutOfMemoryError e) {
//					// TODO: handle exception
//					toast=new ShowToast(AgentInspection2.this, "You cannot upload this image");
//				}
				if(bitmapdb==null)
				{
					cf.show_toast("File corrupted!! Cant able to attach",1);
				}
				else
				{
					if (imageidentifier == "uploadaphoto") {
//						ivcoverpagelogoclose.setVisibility(View.VISIBLE);
//						ivcoverpagelogoselectedimage.setVisibility(View.VISIBLE);
						Call_UploadPhoto_Dialog(selectedImagePath);

					} else if (imageidentifier == "addaimage") {
						Call_Elevationdialog(selectedImagePath);
					}
				}
//				if (!bitmapdb.isRecycled()) {
//					bitmapdb.recycle();
//				}
//				bitmapdb=null;
//				System.gc();
			}
			catch (OutOfMemoryError e) {
				// TODO: handle exception
				cf.show_toast("File size exceeds!! Too large to attach",1);
			}
			}
		}
		else if(requestCode==2&&resultCode==RESULT_OK)
		{
			Bitmap bitmapdb=null;
			Uri selectedImage = data.getData();
			String[] filePathColumn = { MediaStore.Images.Media.DATA };

			Cursor cursor = getContentResolver().query(selectedImage,
					filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			filePath2 = cursor.getString(columnIndex);

			File f = new File(filePath2);
			double length = f.length();
			double kb = length / 1024;
			double mb = kb / 1024;
			System.out.println("The file length is" + length);
			System.out.println("The file length in kb is" + kb);
			System.out.println("The file length in mb is" + mb);

			if (mb >= 2) {
				cf.show_toast("File size exceeds!! Too large to attach",1);
			} 			 else {
				try
				{
					boolean filecheck=false;
					arraylistfilepahlength=arraylistfilepah.size();
					System.out.println("array list file length "+arraylistfilepahlength);
					if(arraylistfilepahlength==0)
					{
						filecheck=true;
					}
					else
					{
						if(arraylistfilepah.contains(filePath2))
						{
							System.out.println("inside arraylist contains if");
							filecheck=false;
						}
						else
						{
							System.out.println("inside arraylist contains else");
							filecheck=Check_Duplicate_Image(filePath2);
						}
					}
					
					System.out.println("filecheck value is "+filecheck);
					
					if(filecheck)
					{
						bitmapdb = decodeFile(filePath2);
						if(bitmapdb==null)
						{
							cf.show_toast("File corrupted!! Cant able to attach",1);
							filePath2=tagfilepath;
							System.out.println("filepath2 is "+filePath2);
							Bitmap bm = decodeFile(tagfilepath);
							ivimageedit.setImageBitmap(bm);
						}
						else
						{
							System.out.println("Inside else request 2");
							ivimageedit.setImageBitmap(bitmapdb);
							db.vi_db.execSQL("update AddAImage set filePath='"+cf.encode(filePath2)+"' where filePath='"+cf.encode(tagfilepath)+"'");
						}
					}
					else
					{
						cf.show_toast("Image already selected, Please choose different one",1);
					}
			}
			catch (OutOfMemoryError e) {
				// TODO: handle exception
				System.out.println("Out of memory error "+e.getMessage());
				cf.show_toast("File size exceeds!! Too large to attach",1);
			}
			
			}

			
		}

	}
	private boolean Check_Duplicate_Image(String filePath)
	{
		String res="";
		for(int i=0;i<arraylistfilepah.size();i++)
		{
			String filepath2=arraylistfilepah.get(i);
			Bitmap b1 = decodeFile(filePath);
			Bitmap b2 = decodeFile(filepath2);
			boolean result=imagesAreEqual(b1,b2);
			if(result==false)
			{
				res+="true";
				System.out.println("res="+res);
			}
			else
			{
				res+="false";
				System.out.println("res="+res);
			}
			
		}
		if(res.contains("false"))
		{
			return false;
		}
		else
		{
			return true;
		}
		
	}

    public boolean imagesAreEqual(Bitmap i1, Bitmap i2)
    {
        if (i1.getHeight() != i2.getHeight())
         return false;
        if (i1.getWidth() != i2.getWidth()) return false;

        for (int y = 0; y < i1.getHeight(); ++y)
           for (int x = 0; x < i1.getWidth(); ++x)
                if (i1.getPixel(x, y) != i2.getPixel(x, y)) return false;

        return true;
    }
	
	

	private void Call_UploadPhoto_Dialog(final String filePath) {
//		final Bitmap bitmapdb = BitmapFactory.decodeFile(filePath);
		final Bitmap bitmapdb = decodeFile(filePath);

		final Dialog dialog = new Dialog(VehicleInspection.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.upload_photo_alert);
		dialog.setCancelable(false);

		ImageView ivimage = (ImageView) dialog
				.findViewById(R.id.upload_photo_alertimage);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.upload_photo_alert_close);
		Button btnsave = (Button) dialog
				.findViewById(R.id.upload_photo_alert_save);
		Button btncancel = (Button) dialog
				.findViewById(R.id.upload_photo_alert_cancel);

		ivimage.setImageBitmap(bitmapdb);

		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				ivcoverpagelogoclose.setVisibility(View.VISIBLE);
				ivcoverpagelogoselectedimage.setVisibility(View.VISIBLE);
				ivcoverpagelogoselectedimage.setImageBitmap(bitmapdb);
				etcoverpagelogo.setText(filePath);
				findViewById(R.id.vehicleinspection_btncoverpagelogo).setVisibility(
						View.GONE);
				
				System.out.println("The filepath is "+filePath);;
				
				String[] filenamesplit = filePath
						.split("/");
				strcoverpagelogofilename = filenamesplit[filenamesplit.length - 1];
				System.out
						.println("The File Name is "
								+ strcoverpagelogofilename);
				
				Bitmap bitmap = cf.ShrinkBitmap(filePath, 400, 400);

				System.out.println(" bimap "+bitmap);
				marshal = new MarshalBase64();
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				bitmap.compress(CompressFormat.PNG, 100, out);
				bytecoverpagelogo = out.toByteArray();
				System.out.println("bytecoverpagelogo length is "+bytecoverpagelogo.length);

				cf.show_toast("Logo saved successfully",1);
			}
		});

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});
		dialog.show();

	}
	
	private void Call_Elevationdialog(final String filePath) {
		
		System.out.println("Inside call elevation dialog");
		
		final Dialog dialog = new Dialog(VehicleInspection.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.elevationtype_and_caption1);
		dialog.setCancelable(false);

		final Spinner spinnerelevation = (Spinner) dialog
				.findViewById(R.id.elevation_elevationtype);
		final EditText edittextcaption = (EditText) dialog
				.findViewById(R.id.elevation_caption);
		final ImageView ivimage = (ImageView) dialog
				.findViewById(R.id.elevation_image);
		final Button btnsave = (Button) dialog
				.findViewById(R.id.elevation_save);
		final Button btncancel = (Button) dialog
				.findViewById(R.id.elevation_cancel);
		ImageView ivclose = (ImageView) dialog
				.findViewById(R.id.elevation_close);
		final LinearLayout llcaption=(LinearLayout)dialog.findViewById(R.id.elevation_llcaption);
		
		System.out.println("Before bitmap");
//		BitmapFactory.Options options = new BitmapFactory.Options();
//		options.inSampleSize = 8;
//		bitmapdb = BitmapFactory.decodeFile(filePath, options);
//		bitmapdb = BitmapFactory.decodeFile(filePath);
		bitmapdb = decodeFile(filePath);
		ivimage.setImageBitmap(bitmapdb);
		System.out.println("After bitmap");
		
		
//		cf.CreateTable(16);
//		Cursor cur = cf.db.rawQuery("select * from " + cf.LoadCaptionValue,
//				null);
//		cur.moveToFirst();
//		if (cur.getCount() >= 1) {
//			arraylistcaption.clear();
//			arraylistcaption.add("--Select--");
//			arraylistcaption.add("Add photo caption");
//			do {
//				String captionvalue = cf.decode(cur.getString(cur
//						.getColumnIndex("caption")));
//				arraylistcaption.add(captionvalue);
//
//			} while (cur.moveToNext());
//			ArrayAdapter<String> captionadapter = new ArrayAdapter<String>(
//					AgentInspection2.this,
//					android.R.layout.simple_spinner_item, arraylistcaption);
//			captionadapter
//					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//			spinnercaption.setAdapter(captionadapter);
//		}
//		else {
//			arraylistcaption.clear();
//			arraylistcaption.add("--Select--");
//			arraylistcaption.add("Add photo caption");
//			captionadapter = new ArrayAdapter<String>(AgentInspection2.this,
//					android.R.layout.simple_spinner_item, array_caption);
//			captionadapter
//					.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//			spinnercaption.setAdapter(captionadapter);
//		}
//		cur.close();

		ArrayAdapter<String> elevationadapter = new ArrayAdapter<String>(
				VehicleInspection.this, android.R.layout.simple_spinner_item,
				array_elevation);
		elevationadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerelevation.setAdapter(elevationadapter);

		spinnerelevation
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int arg2, long arg3) {
						// TODO Auto-generated method stub
						strelevationvalue = spinnerelevation.getSelectedItem()
								.toString();
						
						db.CreateTable(4);
						
						if(strelevationvalue.equals("--Select--"))
						{
							llcaption.setVisibility(View.GONE);
						}
						else
						{
							llcaption.setVisibility(View.VISIBLE);
							if(strelevationvalue.equals("Front View"))
							{
								Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Front%'", null);
								int count=cur.getCount();
								edittextcaption.setText("Front View Photograph"+(count+1));
							}
							else if(strelevationvalue.equals("Rear View"))
							{
								Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Rear%'", null);
								int count=cur.getCount();
								edittextcaption.setText("Rear View Photograph"+(count+1));
							}
							else if(strelevationvalue.equals("Left View"))
							{
								Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Left%'", null);
								int count=cur.getCount();
								edittextcaption.setText("Left View Photograph"+(count+1));
							}
							else if(strelevationvalue.equals("Right View"))
							{
								Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Right%'", null);
								int count=cur.getCount();
								edittextcaption.setText("Right View Photograph"+(count+1));
							}
							else if(strelevationvalue.equals("Interior View"))
							{
								Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Interior%'", null);
								int count=cur.getCount();
								edittextcaption.setText("Interior View Photograph"+(count+1));
							}
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});


		btnsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strcaptionvalue=edittextcaption.getText().toString().trim();
				if (!strelevationvalue.equals("--Select--")) {
					if (!strcaptionvalue.equals("")) {
						dialog.dismiss();
						Dynamic_image_list(strelevationvalue, strcaptionvalue, filePath);
						
						Cursor cur1=db.vi_db.rawQuery("select * from "+db.AddAImage, null);
						System.out.println("Add a Image count is before insert "+cur1.getCount());
						arraylistfilepah.add(filePath);
						db.CreateTable(4);
						db.vi_db.execSQL("insert into "
								+ db.AddAImage
								+ " (caption,elevation,filepath) values('"
								+ cf.encode(strcaptionvalue) + "','" + cf.encode(strelevationvalue)
								+ "','" + cf.encode(filePath) + "')");
						
						Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage, null);
						System.out.println("Add a Image count is "+cur.getCount());
						
						cf.show_toast(strelevationvalue+" saved successfully",1);
						
						int rows=cur.getCount();
						llimagecount.setVisibility(View.VISIBLE);
						tvimagecount.setText((rows)+"/30");
						
					} else {
						cf.show_toast("Please enter Caption",1);
					}
				} else {
					cf.show_toast("Please select Type",1);
				}
			}
		});

		btncancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

		ivclose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

		dialog.show();
	}

private void Dynamic_image_list(String elevation, String caption, final String filePath) {
		
		
		Bitmap bitmap = null;
		try
		{
		final int rows=tldynamiclist.getChildCount();
		System.out.println("layout child count is "+rows);
		
		final TableRow[] tr;
		final LinearLayout[] rlelevation, rlcaption, llbutton;
		final RelativeLayout[] rlimage;
		final TextView[] tvelevation;
		final TextView[] tvcaption;
		final ImageView[] ivimage;
		final Button[] btnedit, btndelete;

		bitmap = decodeFile(filePath);
		
		tr=new TableRow[rows];
		rlelevation=new LinearLayout[rows];
		rlcaption=new LinearLayout[rows];
		llbutton=new LinearLayout[rows];
		rlimage=new RelativeLayout[rows];
		tvelevation=new TextView[rows];
		tvcaption=new TextView[rows];
		ivimage=new ImageView[rows];
		btnedit=new Button[rows];
		btndelete=new Button[rows];
		
		updatefilepath=filePath;
		tldynamiclist.setVisibility(View.VISIBLE);

		Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics displayMetrics = new DisplayMetrics();
		display.getMetrics(displayMetrics);

		int width = displayMetrics.widthPixels;
		int height = displayMetrics.heightPixels;

		
			tlparams = new TableLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			tlparams.setMargins(2, 0, 2, 2);

			trparams = new TableRow.LayoutParams(215, 100);
			trparams.setMargins(1, 1, 1, 1);

			rlparams = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			rlparams.setMargins(20, 0, 0, 0);
			// rlparams.addRule(RelativeLayout.CENTER_IN_PARENT);
			rlparams.gravity = Gravity.CENTER;

			rlimageparams = new RelativeLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			rlimageparams.setMargins(10, 10, 10, 10);
			rlimageparams.addRule(RelativeLayout.CENTER_IN_PARENT);

			llparams = new LinearLayout.LayoutParams(80,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			llparams.setMargins(20, 30, 0, 0);

			tr[rows-1] = new TableRow(this);
			tr[rows-1].setLayoutParams(tlparams);
			tr[rows-1].setBackgroundColor(0xff000000);
			tldynamiclist.addView(tr[rows-1]);

			rlelevation[rows-1] = new LinearLayout(this);
			rlelevation[rows-1].setLayoutParams(trparams);
			rlelevation[rows-1].setBackgroundColor(0xff989898);
			tr[rows-1].addView(rlelevation[rows-1]);

			tvelevation[rows-1] = new TextView(this);
			tvelevation[rows-1].setLayoutParams(rlparams);
			tvelevation[rows-1].setText(elevation);
			tvelevation[rows-1].setTextSize(16);
			tvelevation[rows-1].setTypeface(null, Typeface.BOLD);
			rlelevation[rows-1].addView(tvelevation[rows-1]);

			rlimage[rows-1] = new RelativeLayout(this);
			rlimage[rows-1].setLayoutParams(trparams);
			rlimage[rows-1].setBackgroundColor(0xff989898);
			tr[rows-1].addView(rlimage[rows-1]);

			ivimage[rows-1] = new ImageView(this);
			ivimage[rows-1].setLayoutParams(rlimageparams);
			ivimage[rows-1].setImageBitmap(bitmap);
			rlimage[rows-1].addView(ivimage[rows-1]);

			rlcaption[rows-1] = new LinearLayout(this);
			rlcaption[rows-1].setLayoutParams(trparams);
			rlcaption[rows-1].setBackgroundColor(0xff989898);
			tr[rows-1].addView(rlcaption[rows-1]);

			tvcaption[rows-1] = new TextView(this);
			tvcaption[rows-1].setLayoutParams(rlparams);
			tvcaption[rows-1].setText(caption);
			tvcaption[rows-1].setTextSize(16);
			tvcaption[rows-1].setTypeface(null, Typeface.BOLD);
			rlcaption[rows-1].addView(tvcaption[rows-1]);

			llbutton[rows-1] = new LinearLayout(this);
			llbutton[rows-1].setLayoutParams(trparams);
			llbutton[rows-1].setBackgroundColor(0xff989898);
			tr[rows-1].addView(llbutton[rows-1]);

			btnedit[rows-1] = new Button(this);
			btnedit[rows-1].setLayoutParams(llparams);
			btnedit[rows-1].setText("Edit");
			btnedit[rows-1].setTag(caption+"&#40"+elevation+"&#40"+filePath);
			System.out.println("caption="+caption+" elevation="+elevation+" filepath="+filePath);
			btnedit[rows-1].setBackgroundResource(R.drawable.blackbutton);
			btnedit[rows-1].setTextColor(0xffffffff);
			llbutton[rows-1].addView(btnedit[rows-1]);

			btndelete[rows-1] = new Button(this);
			btndelete[rows-1].setLayoutParams(llparams);
			btndelete[rows-1].setText("Delete");
			btndelete[rows-1].setTag(caption+"&#40"+elevation+"&#40"+filePath);
			btndelete[rows-1].setBackgroundResource(R.drawable.blackbutton);
			btndelete[rows-1].setTextColor(0xffffffff);
			llbutton[rows-1].addView(btndelete[rows-1]);

		
		

		btnedit[rows-1].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Call_Elevationdialog2(tvelevation[rows-1].getText().toString(),tvcaption[rows-1].getText().toString(),bitmap);
				
				filePath2="";
				
				String tagvalue=v.getTag().toString();
				String[] arraytag=tagvalue.split("&#40");
				tagcaption=arraytag[0];
				tagelevation=arraytag[1];
				tagfilepath=arraytag[2];
				
				System.out.println("Tag Caption "+tagcaption);
				System.out.println("Tag elevation "+tagelevation);
				System.out.println("Tag file path "+tagfilepath);
				
				final Dialog dialog2 = new Dialog(VehicleInspection.this);
				dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog2.setContentView(R.layout.elevationtype_and_caption1);
				dialog2.setCancelable(false);

				final Spinner spinnerelevation = (Spinner) dialog2
						.findViewById(R.id.elevation_elevationtype);
				final EditText edittextcaption = (EditText) dialog2
						.findViewById(R.id.elevation_caption);
				ivimageedit = (ImageView) dialog2
						.findViewById(R.id.elevation_image);
				final Button btnsave = (Button) dialog2
						.findViewById(R.id.elevation_save);
				final Button btncancel = (Button) dialog2
						.findViewById(R.id.elevation_cancel);
				ImageView ivclose = (ImageView) dialog2
						.findViewById(R.id.elevation_close);
				final LinearLayout llcaption=(LinearLayout)dialog2.findViewById(R.id.elevation_llcaption);
				
//				Bitmap bm=BitmapFactory.decodeFile(tagfilepath);
				System.out.println("Tagfile path ="+tagfilepath);
				Bitmap bm = decodeFile(tagfilepath);
				ivimageedit.setImageBitmap(bm);

//				ivimageedit.setImageBitmap(bitmap);
				
				ivimageedit.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent i = new Intent(Intent.ACTION_PICK,
								android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
						i.setType("image/*");
						startActivityForResult(i, 2);
					}
				});
				
				final String oldcaptionvalue=tvcaption[rows-1].getText().toString();
				final String oldelevationvalue=tvelevation[rows-1].getText().toString();
				

				ArrayAdapter<String> elevationadapter = new ArrayAdapter<String>(
						VehicleInspection.this,
						android.R.layout.simple_spinner_item, array_elevation);
				elevationadapter
						.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinnerelevation.setAdapter(elevationadapter);

				spinnerelevation.setSelection(elevationadapter
						.getPosition(tvelevation[rows-1].getText().toString()));
//				edittextcaption.setText(tvcaption[rows-1].getText().toString());

				spinnerelevation
						.setOnItemSelectedListener(new OnItemSelectedListener() {

							@Override
							public void onItemSelected(AdapterView<?> arg0,
									View arg1, int arg2, long arg3) {
								// TODO Auto-generated method stub
								strelevationvalue = spinnerelevation
										.getSelectedItem().toString();
								
								db.CreateTable(4);
								
								if(strelevationvalue.equals("--Select--"))
								{
									llcaption.setVisibility(View.GONE);
								}
								else
								{
									if(strelevationvalue.equals(tvelevation[rows-1].getText().toString()))
									{
										llcaption.setVisibility(View.VISIBLE);
										edittextcaption.setText(tvcaption[rows-1].getText().toString());
									}
									else
									{
										llcaption.setVisibility(View.VISIBLE);
										if(strelevationvalue.equals("Front View"))
										{
											Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Front%'", null);
											int count=cur.getCount();
											edittextcaption.setText("Front View Photograph"+(count+1));
										}
										else if(strelevationvalue.equals("Rear View"))
										{
											Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Rear%'", null);
											int count=cur.getCount();
											edittextcaption.setText("Rear View Photograph"+(count+1));
										}
										else if(strelevationvalue.equals("Left View"))
										{
											Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Left%'", null);
											int count=cur.getCount();
											edittextcaption.setText("Left View Photograph"+(count+1));
										}
										else if(strelevationvalue.equals("Right View"))
										{
											Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Right%'", null);
											int count=cur.getCount();
											edittextcaption.setText("Right View Photograph"+(count+1));
										}
										else if(strelevationvalue.equals("Interior View"))
										{
											Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage+" where caption like 'Interior%'", null);
											int count=cur.getCount();
											edittextcaption.setText("Interior View Photograph"+(count+1));
										}
									}
								}
							}

							@Override
							public void onNothingSelected(AdapterView<?> arg0) {
								// TODO Auto-generated method stub

							}
						});
				
				
				btnsave.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						strcaptionvalue=edittextcaption.getText().toString().trim();
						if (!strelevationvalue.equals("--Select--")) {
							if (!strcaptionvalue.equals("")) {
								dialog2.dismiss();
								tvelevation[rows-1].setText(strelevationvalue);
								tvcaption[rows-1].setText(strcaptionvalue);
								if(filePath2.equals(""))
								{
									filePath2=tagfilepath;
								}
								
									Bitmap bitmapdb = decodeFile(filePath2);
									System.out.println("bitmap is " + bitmapdb);
									ivimage[rows - 1].setImageBitmap(bitmapdb);

									System.out.println("filepath 2 is "
											+ filePath2);
									arraylistfilepah.remove(tagfilepath);
									arraylistfilepah.add(filePath2);

									db.CreateTable(4);
									System.out.println("strcaptionvalue"+strcaptionvalue);
									db.vi_db.execSQL("update " + db.AddAImage
											+ " set caption='" + cf.encode(strcaptionvalue)
											+ "',elevation='" + cf.encode(strelevationvalue)+"',filepath='"+cf.encode(filePath2)
											+ "' where filepath='" + cf.encode(filePath2) + "'");
								System.out.println("update " + db.AddAImage
											+ " set caption='" + cf.encode(strcaptionvalue)
											+ "',elevation='" + cf.encode(strelevationvalue)+"',filepath='"+cf.encode(filePath2)
											+ "' where filepath='" + cf.encode(filePath2) + "'");
								Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage, null);
								System.out.println("Add a Image count is "+cur.getCount());
								
								btnedit[rows-1].setTag(strcaptionvalue+"&#40"+strelevationvalue+"&#40"+filePath2);
								btndelete[rows-1].setTag(strcaptionvalue+"&#40"+strelevationvalue+"&#40"+filePath2);
								
								if(!oldcaptionvalue.equals(strcaptionvalue)||!oldelevationvalue.equals(strelevationvalue)||!tagfilepath.equals(filePath2))
								{
									cf.show_toast(strelevationvalue+" updated successfully",1);
								}

							} else {
								cf.show_toast("Please enter Caption",1);
							}
						} else {
							cf.show_toast("Please select Type",1);
						}
					}
				});

				btncancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog2.dismiss();
					}
				});

				ivclose.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog2.dismiss();
					}
				});

				dialog2.show();

			}
		});

		btndelete[rows-1].setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String tagvalue=v.getTag().toString();
				String[] arraytag=tagvalue.split("&#40");
				tagcaption=arraytag[0];
				tagelevation=arraytag[1];
				tagfilepath=arraytag[2];
				
				System.out.println("Rows value is "+rows);
				
				final Dialog dialog3 = new Dialog(VehicleInspection.this);
				dialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog3.setContentView(R.layout.confirm_delete);
				dialog3.setCancelable(false);
				Button btnyes = (Button) dialog3
						.findViewById(R.id.confirmdelete_yes);
				Button btnno = (Button) dialog3
						.findViewById(R.id.confirmdelete_no);
				TextView tvtext = (TextView) dialog3
						.findViewById(R.id.confirmdelete_tvtext);
				
				tvtext.setText(tagelevation+"?");
				
				btnyes.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						tldynamiclist.removeView(tr[rows-1]);
						if (tldynamiclist.getChildCount() < 2) {
							tldynamiclist.setVisibility(View.GONE);
						}
						dialog3.dismiss();
						arraylistfilepah.remove(tagfilepath);
						db.CreateTable(4);
							db.vi_db.execSQL("delete from " + db.AddAImage
								+ " where oid='"+(rows-1)+"'");
							System.out.println("delete from " + db.AddAImage
								+ " where oid='"+(rows-1)+"'");
						cf.show_toast("Deleted successfully", 1);
						Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage, null);
						System.out.println("Add a Image count is "+cur.getCount());
						
						int rows=cur.getCount();
						llimagecount.setVisibility(View.VISIBLE);
						tvimagecount.setText((rows)+"/30");
						
						if(cur.getCount()==0)
						{
							llimagecount.setVisibility(View.GONE);
							tldynamiclist.setVisibility(View.GONE);
						}

					}
				});

				btnno.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog3.dismiss();
					}
				});

				dialog3.show();
			}
		});
	}
	catch (OutOfMemoryError e) {
		// TODO: handle exception
		System.out.println("Out of memory error "+e.getMessage());
		cf.show_toast("File size exceeds!! Too large to attach",1);
	}
	}
	
	private static Bitmap decodeFile(String file) {
	    try {
	    	
	    	File f=new File(file);
	    	
	        // Decode image size
	        BitmapFactory.Options o = new BitmapFactory.Options();
	        o.inJustDecodeBounds = true;
	        BitmapFactory.decodeStream(new FileInputStream(f), null, o);

	        // The new size we want to scale to
	        final int REQUIRED_SIZE = 150;

	        // Find the correct scale value. It should be the power of 2.
	        int scale = 1;
	        while (o.outWidth / scale / 2 >= REQUIRED_SIZE
	                && o.outHeight / scale / 2 >= REQUIRED_SIZE)
	            scale *= 2;

	        // Decode with inSampleSize
	        BitmapFactory.Options o2 = new BitmapFactory.Options();
	        o2.inSampleSize = scale;
	        o.inJustDecodeBounds = false;
	        return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	    } catch (FileNotFoundException e) {
	    }

	    return null;
	}
	
	public void showDialogDate(EditText edt) {
		// TODO Auto-generated method stub
		// getCalender();
		Calendar c = Calendar.getInstance();
		int mYear = c.get(Calendar.YEAR);
		int mMonth = c.get(Calendar.MONTH);
		int mDay = c.get(Calendar.DAY_OF_MONTH);
		System.out.println("the selected " + mDay);
		DatePickerDialog dialog = new DatePickerDialog(VehicleInspection.this,
				new mDateSetListener(edt), mYear, mMonth, mDay);
		dialog.show();
	}

	class mDateSetListener implements DatePickerDialog.OnDateSetListener {
		EditText v;

		mDateSetListener(EditText v) {
			this.v = v;
		}

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			// getCalender();
			int mYear = year;
			int mMonth = monthOfYear;
			int mDay = dayOfMonth;
			v.setText(new StringBuilder()
					// Month is 0 based so add 1
					.append(mMonth + 1).append("/").append(mDay).append("/")
					.append(mYear).append(" "));
			System.out.println(v.getText().toString());

			Date date1 = null, date2 = null;
			try {
				System.out.println("Inside try");
				String formatString = "MM/dd/yyyy";
				SimpleDateFormat df = new SimpleDateFormat(formatString);
				date1 = df.parse(currentdate);
				date2 = df.parse(v.getText().toString());
				System.out.println("current date " + date1);
				System.out.println("selected date " + date2);
				
				if(v==findViewById(R.id.vehicleinspection_etdateofinspection))
				{
					if (date2.compareTo(date1) < 0) {
						System.out.println("inside date if");
						cf.show_toast("Please select Future Date",1);
						v.setText("");
					} else {
						System.out.println("inside date else");
						
					}
				}
				else if(v==findViewById(R.id.vehicleinspection_etexpires))
				{
					if (date2.compareTo(date1) < 0) {
						System.out.println("inside date if");
						cf.show_toast("Please select Future Date",1);
						v.setText("");
					} else {
						System.out.println("inside date else");
					}
				}
				else
				{
					if (date2.compareTo(date1) > 0) {
						System.out.println("inside date if");
						cf.show_toast("Please select Past Date",1);
						v.setText("");
						
					} else {
						System.out.println("inside date else");
						cbdateoflastoilchange.setChecked(false);
					}
				}
				
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

	}
	
	public String LoadState() {
		try {
			db_sc = new DataBaseHelper_statecounty(VehicleInspection.this);

			db_sc.createDataBase();
			SQLiteDatabase newDB = db_sc.openDataBase();
			db_sc.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from State_Table order by statename",
					null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraystateid = new String[rows + 1];
			arraystatename = new String[rows + 1];
			arraystateid[0] = "0";
			arraystatename[0] = "--Select--";
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String ID = cf.decode(cur.getString(cur
							.getColumnIndex("stateid")));
					String Category = cf.decode(cur.getString(cur
							.getColumnIndex("statename")));
					arraystateid[i] = ID;
					arraystatename[i] = Category;
					i++;

				} while (cur.moveToNext());
			}
			cur.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
		return "true";
	}

	public void LoadCounty(final String stateid) {
		try {
			db_sc = new DataBaseHelper_statecounty(VehicleInspection.this);

			db_sc.createDataBase();
			SQLiteDatabase newDB = db_sc.openDataBase();
			db_sc.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from County_Table"
					+ " where stateid='" + cf.encode(stateid) + "' order by countyname", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraycountyid = new String[rows + 1];
			arraycountyname = new String[rows + 1];
			arraycountyid[0] = "--Select--";
			arraycountyname[0] = "--Select--";
			System.out.println("LoadCounty count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("countyid")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("countyname")));
					arraycountyid[i] = id;
					arraycountyname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadCountyData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadCountyData() {
		countyadapter = new ArrayAdapter<String>(VehicleInspection.this,
				android.R.layout.simple_spinner_item, arraycountyname);
		countyadapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnercounty.setAdapter(countyadapter);
		
		if(countysetselection)
		{
			spinnercounty.setSelection(countyadapter.getPosition(county));
			countysetselection=false;
		}
		
	}
	
	public String LoadMake() {
		try {
			MakeandModel_DataBaseHelper dbh = new MakeandModel_DataBaseHelper(VehicleInspection.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from make", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraymakeid = new String[rows + 1];
			arraymakename = new String[rows + 1];
			arraymakeid[0] = "--Select--";
			arraymakename[0] = "--Select--";
			System.out.println("LoadMake count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("id")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("title")));
					arraymakeid[i] = id;
					arraymakename[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		return "true";
	}
	
	public void LoadModel(final String makeid) {
		try {
			MakeandModel_DataBaseHelper dbh = new MakeandModel_DataBaseHelper(VehicleInspection.this);

			dbh.createDataBase();
			SQLiteDatabase newDB = dbh.openDataBase();
			dbh.getReadableDatabase();
			Cursor cur = newDB.rawQuery("select * from model"
					+ " where make_id='" + makeid+"'", null);
			cur.moveToFirst();
			int rows = cur.getCount();
			arraymodelid = new String[rows + 1];
			arraymodelname = new String[rows + 1];
			arraymodelid[0] = "--Select--";
			arraymodelname[0] = "--Select--";
			System.out.println("LoadModel count is " + rows);
			cur.moveToFirst();
			if (cur.getCount() >= 1) {
				int i = 1;
				do {
					String id = cf.decode(cur.getString(cur
							.getColumnIndex("id")));
					String Name = cf.decode(cur.getString(cur
							.getColumnIndex("title")));
					arraymodelid[i] = id;
					arraymodelname[i] = Name;
					i++;
				} while (cur.moveToNext());

			}
			LoadModelData();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(getClass().getSimpleName(),
					"Could not create or Open the database1");
		} catch (SQLiteException e) {
			// TODO: handle exception
			System.out.println("Exception " + e.getMessage());
		}
		
	}

	private void LoadModelData() {
		modeladapter = new ArrayAdapter<String>(VehicleInspection.this,
				android.R.layout.simple_spinner_item, arraymodelname);
		modeladapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnermodel.setAdapter(modeladapter);
		
	}
	
	
	
	private void Load_State_County_City(final EditText et)
	{

		if (cf.isInternetOn() == true) {
			// show_ProgressDialog("Processing");
			String source = "<b><font color=#00FF33>" + "Processing"
					+ " . Please wait...</font></b>";
			final ProgressDialog pd = ProgressDialog.show(VehicleInspection.this,
					"", Html.fromHtml(source), true);
			new Thread() {
				SoapObject chklogin1;
				public void run() {
					Looper.prepare();
					try {
						chklogin1 = cf
								.Calling_WS_GETADDRESSDETAILS(et.getText().toString(),"GETADDRESSDETAILS");
						System.out.println("response GETADDRESSDETAILS" + chklogin1);
						
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.",1);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

						} else if (show_handler == 5) {
							show_handler = 0;
							if (chklogin1.toString().equals("anyType{}"))
							{
								et.setText("");
								cf.hidekeyboard(etzip);
								cf.show_toast("Please enter a valid Zip",1);
							}
							else
							{
								Load_State_County_City(chklogin1);
							}
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available",1);

		}
	
	}
	
	private void Load_State_County_City(SoapObject objInsert)
	{
		    
		    cf.hidekeyboard(etzip);
			SoapObject obj = (SoapObject) objInsert.getProperty(0);
			state=String.valueOf(obj.getProperty("s_state"));
			stateid=String.valueOf(obj.getProperty("i_state"));
			county=String.valueOf(obj.getProperty("A_County"));
			countyid=String.valueOf(obj.getProperty("i_County"));
			city=String.valueOf(obj.getProperty("city"));
			
			System.out.println("State :"+state);
			System.out.println("County :"+county);
			System.out.println("City :"+city);
			
				spinnerstate.setSelection(stateadapter.getPosition(state));
				countysetselection=true;
				etcity.setText(city);
	}

	private void check_validation() {
		boolean presentcondition;
//		if(llpresentatinspection.getVisibility()==View.VISIBLE)
//		{
//			if(otherpresentatinspection.getText().toString().trim().equals(""))
//			{
//				strwhowaspresentatinspection="";
//			}
//			else
//			{
//				strwhowaspresentatinspection=otherpresentatinspection.getText().toString().trim();
//			}
//		}
		
		if(!cbowner.isChecked()&&!cbagent.isChecked()&&!cbrepresentative.isChecked()&&!cbwhowaspresentother.isChecked())
		{
			presentcondition=false;
		}
		else
		{
			if(cbwhowaspresentother.isChecked())
			{
				if(etpresentatinspectionother.getText().toString().trim().equals(""))
				{
					presentcondition=false;
				}
				else
				{
					presentcondition=true;
				}
			}
			else
			{
				presentcondition=true;
			}
		}
		
		if(llvehicletype.getVisibility()==View.VISIBLE)
		{
			if(othervehicletype.getText().toString().trim().equals(""))
			{
				strvehicletype="";
			}
			else
			{
				strvehicletype=othervehicletype.getText().toString().trim();
			}
		}
		
		if(llpaintcondition.getVisibility()==View.VISIBLE)
		{
			if(otherpaintcondition.getText().toString().trim().equals(""))
			{
				strpaintcondition="";
			}
			else
			{
				strpaintcondition=otherpaintcondition.getText().toString().trim();
			}
		}
		
		if(lldents.getVisibility()==View.VISIBLE)
		{
			if(otherdents.getText().toString().trim().equals(""))
			{
				strdents="";
			}
			else
			{
				strdents=otherdents.getText().toString().trim();
			}
		}
		
		if(llrustproblems.getVisibility()==View.VISIBLE)
		{
			if(otherrustproblems.getText().toString().trim().equals(""))
			{
				strrustproblems="";
			}
			else
			{
				strrustproblems=otherrustproblems.getText().toString().trim();
			}
		}
		
		if(llengineoil.getVisibility()==View.VISIBLE)
		{
			if(otherengineoil.getText().toString().trim().equals(""))
			{
				strengineoil="";
			}
			else
			{
				strengineoil=otherengineoil.getText().toString().trim();
			}
		}
		
		if(lltransmissionfluid.getVisibility()==View.VISIBLE)
		{
			if(othertransmissionfluid.getText().toString().trim().equals(""))
			{
				strtransmissionfluid="";
			}
			else
			{
				strtransmissionfluid=othertransmissionfluid.getText().toString().trim();
			}
		}
		
		if(lldifferential.getVisibility()==View.VISIBLE)
		{
			if(otherdifferential.getText().toString().trim().equals(""))
			{
				strdifferential="";
			}
			else
			{
				strdifferential=otherdifferential.getText().toString().trim();
			}
		}
		
		if(llcoolant.getVisibility()==View.VISIBLE)
		{
			if(othercoolant.getText().toString().trim().equals(""))
			{
				strcoolant="";
			}
			else
			{
				strcoolant=othercoolant.getText().toString().trim();
			}
		}
		
		if(llbrakefluid.getVisibility()==View.VISIBLE)
		{
			if(otherbrakefluid.getText().toString().trim().equals(""))
			{
				strbrakefluid="";
			}
			else
			{
				strbrakefluid=otherbrakefluid.getText().toString().trim();
			}
		}
		
		if(llpowersteeringfluid.getVisibility()==View.VISIBLE)
		{
			if(otherpowersteeringfluid.getText().toString().trim().equals(""))
			{
				strpowersteeringfluid="";
			}
			else
			{
				strpowersteeringfluid=otherpowersteeringfluid.getText().toString().trim();
			}
		}
		
		if(llcaster.getVisibility()==View.VISIBLE)
		{
			if(othercaster.getText().toString().trim().equals(""))
			{
				strcaster="";
			}
			else
			{
				strcaster=othercaster.getText().toString().trim();
			}
		}
		
		if(llujoints.getVisibility()==View.VISIBLE)
		{
			if(otherujoints.getText().toString().trim().equals(""))
			{
				strujoints="";
			}
			else
			{
				strujoints=otherujoints.getText().toString().trim();
			}
		}
		
		if(llcvboots.getVisibility()==View.VISIBLE)
		{
			if(othercvboots.getText().toString().trim().equals(""))
			{
				strcvboots="";
			}
			else
			{
				strcvboots=othercvboots.getText().toString().trim();
			}
		}
		
		if(llshocks.getVisibility()==View.VISIBLE)
		{
			if(othershocks.getText().toString().trim().equals(""))
			{
				strshocks="";
			}
			else
			{
				strshocks=othershocks.getText().toString().trim();
			}
		}
		
		if(llballjoints.getVisibility()==View.VISIBLE)
		{
			if(otherballjoints.getText().toString().trim().equals(""))
			{
				strballjoints="";
			}
			else
			{
				strballjoints=otherballjoints.getText().toString().trim();
			}
		}
		
		if(llbushings.getVisibility()==View.VISIBLE)
		{
			if(otherbushings.getText().toString().trim().equals(""))
			{
				strbushings="";
			}
			else
			{
				strbushings=otherbushings.getText().toString().trim();
			}
		}
		
		if(lllinkpins.getVisibility()==View.VISIBLE)
		{
			if(otherlinkpins.getText().toString().trim().equals(""))
			{
				strlinkpins="";
			}
			else
			{
				strlinkpins=otherlinkpins.getText().toString().trim();
			}
		}
		
		if(llrockandpinion.getVisibility()==View.VISIBLE)
		{
			if(otherrockandpinion.getText().toString().trim().equals(""))
			{
				strrockandpinion="";
			}
			else
			{
				strrockandpinion=otherrockandpinion.getText().toString().trim();
			}
		}
		
		if(llidler.getVisibility()==View.VISIBLE)
		{
			if(otheridler.getText().toString().trim().equals(""))
			{
				stridler="";
			}
			else
			{
				stridler=otheridler.getText().toString().trim();
			}
		}
		
		if(llcenterlink.getVisibility()==View.VISIBLE)
		{
			if(othercenterlink.getText().toString().trim().equals(""))
			{
				strcenterlink="";
			}
			else
			{
				strcenterlink=othercenterlink.getText().toString().trim();
			}
		}
		
		if(llexhaustsystem.getVisibility()==View.VISIBLE)
		{
			if(otherexhaustsystem.getText().toString().trim().equals(""))
			{
				strexhaustsystemcomplete="";
			}
			else
			{
				strexhaustsystemcomplete=otherexhaustsystem.getText().toString().trim();
			}
		}
		
		if(llemissioncontrol.getVisibility()==View.VISIBLE)
		{
			if(otheremissioncontrol.getText().toString().trim().equals(""))
			{
				stremissioncontrolintact="";
			}
			else
			{
				stremissioncontrolintact=otheremissioncontrol.getText().toString().trim();
			}
		}
		
		if(llmastercylinder.getVisibility()==View.VISIBLE)
		{
			if(othermastercylinder.getText().toString().trim().equals(""))
			{
				strmastercylinder="";
			}
			else
			{
				strmastercylinder=othermastercylinder.getText().toString().trim();
			}
		}
		
		if(lldrumsanddiscs.getVisibility()==View.VISIBLE)
		{
			if(otherdrumsanddiscs.getText().toString().trim().equals(""))
			{
				strdrumsanddiscs="";
			}
			else
			{
				strdrumsanddiscs=otherdrumsanddiscs.getText().toString().trim();
			}
		}
		
		if(llwheelcylinders.getVisibility()==View.VISIBLE)
		{
			if(otherwheelcylinders.getText().toString().trim().equals(""))
			{
				strwheelcylinder="";
			}
			else
			{
				strwheelcylinder=otherwheelcylinders.getText().toString().trim();
			}
		}
		
		if(llcalipters.getVisibility()==View.VISIBLE)
		{
			if(othercalipters.getText().toString().trim().equals(""))
			{
				strcalipters="";
			}
			else
			{
				strcalipters=othercalipters.getText().toString().trim();
			}
		}
		
		if(llinstruments.getVisibility()==View.VISIBLE)
		{
			if(otherinstruments.getText().toString().trim().equals(""))
			{
				strinstruments="";
			}
			else
			{
				strinstruments=otherinstruments.getText().toString().trim();
			}
		}
		
		if(llinteriorlights.getVisibility()==View.VISIBLE)
		{
			if(otherinteriorlights.getText().toString().trim().equals(""))
			{
				strinteriorlights="";
			}
			else
			{
				strinteriorlights=otherinteriorlights.getText().toString().trim();
			}
		}
		
		if(llheadlights.getVisibility()==View.VISIBLE)
		{
			if(otherheadlights.getText().toString().trim().equals(""))
			{
				strheadlights="";
			}
			else
			{
				strheadlights=otherheadlights.getText().toString().trim();
			}
		}
		
		if(lltaillights.getVisibility()==View.VISIBLE)
		{
			if(othertaillights.getText().toString().trim().equals(""))
			{
				strtaillights="";
			}
			else
			{
				strtaillights=othertaillights.getText().toString().trim();
			}
		}
		
		if(llturnsignals.getVisibility()==View.VISIBLE)
		{
			if(otherturnsignals.getText().toString().trim().equals(""))
			{
				strturnsignals="";
			}
			else
			{
				strturnsignals=otherturnsignals.getText().toString().trim();
			}
		}
		
		if(llbackuplights.getVisibility()==View.VISIBLE)
		{
			if(otherbackuplights.getText().toString().trim().equals(""))
			{
				strbackuplights="";
			}
			else
			{
				strbackuplights=otherbackuplights.getText().toString().trim();
			}
		}
		
		if(llbrakelights.getVisibility()==View.VISIBLE)
		{
			if(otherbrakelights.getText().toString().trim().equals(""))
			{
				strbrakelights="";
			}
			else
			{
				strbrakelights=otherbrakelights.getText().toString().trim();
			}
		}
		
		if(llemergencylights.getVisibility()==View.VISIBLE)
		{
			if(otheremergencylights.getText().toString().trim().equals(""))
			{
				stremergencylights="";
			}
			else
			{
				stremergencylights=otheremergencylights.getText().toString().trim();
			}
		}
		
		if(llsystemfailurewarninglights.getVisibility()==View.VISIBLE)
		{
			if(othersystemfailurewarninglights.getText().toString().trim().equals(""))
			{
				strsystemfailure="";
			}
			else
			{
				strsystemfailure=othersystemfailurewarninglights.getText().toString().trim();
			}
		}
		
		if(llairconditioningsystem.getVisibility()==View.VISIBLE)
		{
			if(otherairconditioningsystem.getText().toString().trim().equals(""))
			{
				strairconditioningsystem="";
			}
			else
			{
				strairconditioningsystem=otherairconditioningsystem.getText().toString().trim();
			}
		}
		
		if(llheatingsystem.getVisibility()==View.VISIBLE)
		{
			if(otherheatingsystem.getText().toString().trim().equals(""))
			{
				strheatingsystem="";
			}
			else
			{
				strheatingsystem=otherheatingsystem.getText().toString().trim();
			}
		}
		
		if(lldoorlocks.getVisibility()==View.VISIBLE)
		{
			if(otherdoorlocks.getText().toString().trim().equals(""))
			{
				strdoorlocks="";
			}
			else
			{
				strdoorlocks=otherdoorlocks.getText().toString().trim();
			}
		}
		
		boolean modelcheck;
		if(strmodel.equals("--Select--"))
		{
			modelcheck=false;
		}
		else
		{
			if(strmodel.startsWith("Other"))
			{
				if(etothermodel.getText().toString().trim().equals(""))
				{
					modelcheck=false;
				}
				else
				{
					modelcheck=true;
				}
			}
			else
			{
				modelcheck=true;
			}
		}
		
		
		
		
		if(!etfirstname.getText().toString().trim().equals(""))
		{
			if(!etlastname.getText().toString().trim().equals(""))
			{
				if(!etdateofinspection.getText().toString().trim().equals(""))
				{
//					if(!etinsurancecompany.getText().toString().trim().equals(""))
//					{
						if(presentcondition)
						{
							if(!etvehicleno.getText().toString().trim().equals(""))
							{
								if(!etvin.getText().toString().trim().equals(""))
								{
									if(!etlicenseno.getText().toString().trim().equals(""))
									{
										if(!etexpires.getText().toString().trim().equals(""))
										{
											if(!strmake.equals("--Select--"))
											{
												if(modelcheck)
												{
													if(!strvehicletype.equals(""))
													{
//														if(!etinspectionaddress1.getText().toString().trim().equals(""))
//														{
//															if(!etzip.getText().toString().trim().equals(""))
//															{
//																if((etzip.getText().toString().length()==5))
//																{
//																if(!etcity.getText().toString().trim().equals(""))
//																{
//																	if(!strstate.equals("--Select--"))
//																	{
//																		if(!strcounty.equals("--Select--"))
//																		{
																			if(!strpaintcondition.equals(""))
																			{
																				if(!strdents.equals(""))
																				{
																					if(!strrustproblems.equals(""))
																					{
																						if(!strappeartobeinaccident.equals(""))
																						{
																							if(!strdamagetotheframe.equals(""))
																							{
																								if(!strdamagetowindshield.equals(""))
																								{
																									if(!stranyfluidleaks.equals(""))
																									{
																										if(!etdateoflastoilchange.getText().toString().trim().equals(""))
																										{
																											if(!strengineoil.equals(""))
																											{
																												if(!strtransmissionfluid.equals(""))
																												{
																													if(!strdifferential.equals(""))
																													{
																														if(!strcoolant.equals(""))
																														{
																															if(!strbrakefluid.equals(""))
																															{
																																if(!strpowersteeringfluid.equals(""))
																																{
																																	if(!strconditionoftires.equals(""))
																																	{
																																		if(!strconditionofvalvestems.equals(""))
																																		{
																																			if(!strcaster.equals(""))
																																			{
																																				if(!stranyproblemwithsuspension.equals(""))
																																				{
																																					if(!stranyproblemwithalignment.equals(""))
																																					{
																																						if(!strujoints.equals(""))
																																						{
																																							if(!strcvboots.equals(""))
																																							{
																																								if(!strshocks.equals(""))
																																								{
																																									if(!strballjoints.equals(""))
																																									{
																																										if(!strbushings.equals(""))
																																										{
																																											if(!strlinkpins.equals(""))
																																											{
																																												if(!strrockandpinion.equals(""))
																																												{
																																													if(!stridler.equals(""))
																																													{
																																														if(!strcenterlink.equals(""))
																																														{
																																															if(!strexhaustsystemcomplete.equals(""))
																																															{
																																																if(!stremissioncontrolintact.equals(""))
																																																{
																																																	if(!strdoallbelts.equals(""))
																																																	{
																																																		if(!strconditionofsparkplugs.equals(""))
																																																		{
																																																			if(!strdothebrakesgrab.equals(""))
																																																			{
																																																				if(!strdoescarshudder.equals(""))
																																																				{
																																																					if(!strarebrakelinings.equals(""))
																																																					{
																																																						if(!strconditionofbrakepads.equals(""))
																																																						{
																																																							if(!stranyproblemswithhudraulicsystem.equals(""))
																																																							{
																																																								if(!strdoesparkingbrake.equals(""))
																																																								{
																																																									if(!strisbrakepedalpressureokay.equals(""))
																																																									{
																																																										if(!strmastercylinder.equals(""))
																																																										{
																																																											if(!strdrumsanddiscs.equals(""))
																																																											{
																																																												if(!strwheelcylinder.equals(""))
																																																												{
																																																												if(!strcalipters.equals(""))
																																																												{
																																																													if(!strinstruments.equals(""))
																																																													{
																																																														if(!strinteriorlights.equals(""))
																																																														{
																																																															if(!strheadlights.equals(""))
																																																															{
																																																																if(!strtaillights.equals(""))
																																																																{
																																																																	if(!strturnsignals.equals(""))
																																																																	{
																																																																		if(!strbackuplights.equals(""))
																																																																		{
																																																																			if(!strbrakelights.equals(""))
																																																																			{
																																																																				if(!stremergencylights.equals(""))
																																																																				{
																																																																					if(!strsystemfailure.equals(""))
																																																																					{
																																																																						if(!strairconditioningsystem.equals(""))
																																																																						{
																																																																							if(!strheatingsystem.equals(""))
																																																																							{
																																																																								if(!strconditionofbattery.equals(""))
																																																																								{
																																																																									if(!strdoorlocks.equals(""))
																																																																									{
																																																																										if(!strdoallpoweroptionswork.equals(""))
																																																																										{
																																																																											if(!stranydelaybetweenenginespeed.equals(""))
																																																																											{
																																																																												if(!strautomatictransmission.equals(""))
																																																																												{
																																																																													if(!etcoverpagelogo.getText().toString().trim().equals(""))
																																																																													{
//																																																																														Vehicle_Inspection();
																																																																														Call_VehicleInformationList();
																																																																													}
																																																																													else
																																																																													{
																																																																														cf.show_toast("Please upload Cover page logo",1);
																																																																													}
																																																																												}
																																																																												else
																																																																												{
																																																																													cf.show_toast("Please select the option for Automatic transmission shifting properly under Operational Accessories",1);
																																																																												}
																																																																											}
																																																																											else
																																																																											{
																																																																												cf.show_toast("Please select the option for Any delay between engine speed and speed of vehicle under Operational Accessories",1);
																																																																											}
																																																																										}
																																																																										else
																																																																										{
																																																																											cf.show_toast("Please select the option for Do all power options work under Operational Accessories",1);
																																																																										}
																																																																									}
																																																																									else
																																																																									{
																																																																										if(lldoorlocks.getVisibility()==View.VISIBLE)
																																																																										{
																																																																											cf.show_toast("Please enter the other text for Door locks/Door windows all operational under Operational Accessories",1);
																																																																											otherdoorlocks.setText("");
																																																																											otherdoorlocks.requestFocus();
																																																																										}
																																																																										else
																																																																										{
																																																																											cf.show_toast("Please select the option for Door locks/Door windows all operational under Operational Accessories",1);
																																																																										}
																																																																										
																																																																									}
																																																																								}
																																																																								else
																																																																								{
																																																																									cf.show_toast("Please select the option for Condition of battery under Operational Accessories",1);
																																																																								}
																																																																							}
																																																																							else
																																																																							{
																																																																								if(llheatingsystem.getVisibility()==View.VISIBLE)
																																																																								{
																																																																									cf.show_toast("Please enter the other text for Heating system under Operational Accessories",1);
																																																																									otherheatingsystem.setText("");
																																																																									otherheatingsystem.requestFocus();
																																																																								}
																																																																								else
																																																																								{
																																																																									cf.show_toast("Please select the option for Heating system under Operational Accessories",1);
																																																																								}
																																																																								
																																																																							}
																																																																						}
																																																																						else
																																																																						{
																																																																							if(llairconditioningsystem.getVisibility()==View.VISIBLE)
																																																																							{
																																																																								cf.show_toast("Please enter the other text for Air conditioning system under Operational Accessories",1);
																																																																								otherairconditioningsystem.setText("");
																																																																								otherairconditioningsystem.requestFocus();
																																																																							}
																																																																							else
																																																																							{
																																																																								cf.show_toast("Please select the option for Air conditioning system under Operational Accessories",1);
																																																																							}
																																																																							
																																																																						}
																																																																					}
																																																																					else
																																																																					{
																																																																						if(llsystemfailurewarninglights.getVisibility()==View.VISIBLE)
																																																																						{
																																																																							cf.show_toast("Please enter the other text for System failure warning lights under Operational Accessories",1);
																																																																							othersystemfailurewarninglights.setText("");
																																																																							othersystemfailurewarninglights.requestFocus();
																																																																						}
																																																																						else
																																																																						{
																																																																							cf.show_toast("Please select the option for System failure warning lights under Operational Accessories",1);
																																																																						}
																																																																						
																																																																					}
																																																																				}
																																																																				else
																																																																				{
																																																																					if(llemergencylights.getVisibility()==View.VISIBLE)
																																																																					{
																																																																						cf.show_toast("Please enter the other text for Emergency/Hazard lights under Operational Accessories",1);
																																																																						otheremergencylights.setText("");
																																																																						otheremergencylights.requestFocus();
																																																																					}
																																																																					else
																																																																					{
																																																																						cf.show_toast("Please select the option for Emergency/Hazard lights under Operational Accessories",1);
																																																																					}
																																																																					
																																																																				}
																																																																			}
																																																																			else
																																																																			{
																																																																				if(llbrakelights.getVisibility()==View.VISIBLE)
																																																																				{
																																																																					cf.show_toast("Please enter the other text for Brake lights under Operational Accessories",1);
																																																																					otherbrakelights.setText("");
																																																																					otherbrakelights.requestFocus();
																																																																				}
																																																																				else
																																																																				{
																																																																					cf.show_toast("Please select the option for Brake lights under Operational Accessories",1);
																																																																				}
																																																																				
																																																																			}
																																																																		}
																																																																		else
																																																																		{
																																																																			if(llbackuplights.getVisibility()==View.VISIBLE)
																																																																			{
																																																																				cf.show_toast("Please enter the other text for Back-up lights under Operational Accessories",1);
																																																																				otherbackuplights.setText("");
																																																																				otherbackuplights.requestFocus();
																																																																			}
																																																																			else
																																																																			{
																																																																				cf.show_toast("Please select the option for Back-up lights under Operational Accessories",1);
																																																																			}
																																																																			
																																																																		}
																																																																	}
																																																																	else
																																																																	{
																																																																		if(llturnsignals.getVisibility()==View.VISIBLE)
																																																																		{
																																																																			cf.show_toast("Please enter the other text for Turn signals under Operational Accessories",1);
																																																																			otherturnsignals.setText("");
																																																																			otherturnsignals.requestFocus();
																																																																		}
																																																																		else
																																																																		{
																																																																			cf.show_toast("Please select the option for Turn signals under Operational Accessories",1);
																																																																		}
																																																																		
																																																																	}
																																																																}
																																																																else
																																																																{
																																																																	if(lltaillights.getVisibility()==View.VISIBLE)
																																																																	{
																																																																		cf.show_toast("Please enter the other text for Tail lights under Operational Accessories",1);
																																																																		othertaillights.setText("");
																																																																		othertaillights.requestFocus();
																																																																	}
																																																																	else
																																																																	{
																																																																		cf.show_toast("Please select the option for Tail lights under Operational Accessories",1);
																																																																	}
																																																																	
																																																																}
																																																															}
																																																															else
																																																															{
																																																																if(llheadlights.getVisibility()==View.VISIBLE)
																																																																{
																																																																	cf.show_toast("Please enter the other text for Head lights under Operational Accessories",1);
																																																																	otherheadlights.setText("");
																																																																	otherheadlights.requestFocus();
																																																																}
																																																																else
																																																																{
																																																																	cf.show_toast("Please select the option for Head lights under Operational Accessories",1);
																																																																}
																																																																
																																																															}
																																																														}
																																																														else
																																																														{
																																																															if(llinteriorlights.getVisibility()==View.VISIBLE)
																																																															{
																																																																cf.show_toast("Please enter the other text for Interior lights under Operational Accessories",1);
																																																																otherinteriorlights.setText("");
																																																																otherinteriorlights.requestFocus();
																																																															}
																																																															else
																																																															{
																																																																cf.show_toast("Please select the option for Interior lights under Operational Accessories",1);
																																																															}
																																																															
																																																														}
																																																													}
																																																													else
																																																													{
																																																														if(llinstruments.getVisibility()==View.VISIBLE)
																																																														{
																																																															cf.show_toast("Please enter the other text for Instruments/Gauges under Operational Accessories",1);
																																																															otherinstruments.setText("");
																																																															otherinstruments.requestFocus();
																																																														}
																																																														else
																																																														{
																																																															cf.show_toast("Please select the option for Instruments/Gauges under Operational Accessories",1);
																																																														}
																																																														
																																																													}
																																																												}
																																																												else
																																																												{
																																																													if(llcalipters.getVisibility()==View.VISIBLE)
																																																													{
																																																														cf.show_toast("Please enter the other text for Calipters-Disc brake assembly under Brake Systems",1);
																																																														othercalipters.setText("");
																																																														othercalipters.requestFocus();
																																																													}
																																																													else
																																																													{
																																																														cf.show_toast("Please select the option for Calipters-Disc brake assembly under Brake Systems",1);
																																																													}
																																																													
																																																												}
																																																											}
																																																											else
																																																											{
																																																												if(llwheelcylinders.getVisibility()==View.VISIBLE)
																																																												{
																																																													cf.show_toast("Please enter the other text for Wheel Cylinders-drum brakes under Brake Systems",1);
																																																													otherwheelcylinders.setText("");
																																																													otherwheelcylinders.requestFocus();
																																																												}
																																																												else
																																																												{
																																																													cf.show_toast("Please select the option for Wheel Cylinders-drum brakes under Brake Systems",1);
																																																												}
																																																												
																																																											}
																																																											}
																																																											else
																																																											{
																																																												if(lldrumsanddiscs.getVisibility()==View.VISIBLE)
																																																												{
																																																													cf.show_toast("Please enter the other text for Drums and discs under Brake Systems",1);
																																																													otherdrumsanddiscs.setText("");
																																																													otherdrumsanddiscs.requestFocus();
																																																												}
																																																												else
																																																												{
																																																													cf.show_toast("Please select the option for Drums and discs under Brake Systems",1);
																																																												}
																																																												
																																																											}
																																																										}
																																																										else
																																																										{
																																																											if(llmastercylinder.getVisibility()==View.VISIBLE)
																																																											{
																																																												cf.show_toast("Please enter the other text for Master cylinder under Brake Systems",1);
																																																												othermastercylinder.setText("");
																																																												othermastercylinder.requestFocus();
																																																											}
																																																											else
																																																											{
																																																												cf.show_toast("Please select the option for Master cylinder under Brake Systems",1);
																																																											}
																																																											
																																																										}
																																																									}
																																																									else
																																																									{
																																																										cf.show_toast("Please select the option for Is brake pedal pressure ok under Brake Systems",1);
																																																									}
																																																								}
																																																								else
																																																								{
																																																									cf.show_toast("Please select the option for Does parking brake work correctly under Brake Systems",1);
																																																								}
																																																							}
																																																							else
																																																							{
																																																								cf.show_toast("Please select the option for Any problems with hydraulic systems under Brake Systems",1);
																																																							}
																																																						}
																																																						else
																																																						{
																																																							cf.show_toast("Please select the option for Condition of brake pods under Brake Systems",1);
																																																						}
																																																					}
																																																					else
																																																					{
																																																						cf.show_toast("Please select the option for Are brake linings within recommended thickness under Brake Systems",1);
																																																					}
																																																				}
																																																				else
																																																				{
																																																					cf.show_toast("Please select the option for Does car shudder when braking(rotors) under Brake Systems",1);
																																																				}
																																																			}
																																																			else
																																																			{
																																																				cf.show_toast("Please select the option for Do all brakes grab, or make noise under Brake Systems",1);
																																																			}
																																																		}
																																																		else
																																																		{
																																																			cf.show_toast("Please select the option for Condition of spark plugs?Any oil? under Engine Performance",1);
																																																		}
																																																	}
																																																	else
																																																	{
																																																		cf.show_toast("Please select the option for Do all belts and hoses appear intact under Engine Performance",1);
																																																	}
																																																}
																																																else
																																																{
																																																	if(llemissioncontrol.getVisibility()==View.VISIBLE)
																																																	{
																																																		cf.show_toast("Please enter the other text for Emissions control intact/functioning under Engine Performance",1);
																																																		otheremissioncontrol.setText("");
																																																		otheremissioncontrol.requestFocus();
																																																	}
																																																	else
																																																	{
																																																		cf.show_toast("Please select the option for Emissions control intact/functioning under Engine Performance",1);
																																																	}
																																																	
																																																}
																																															}
																																															else
																																															{
																																																if(llexhaustsystem.getVisibility()==View.VISIBLE)
																																																{
																																																	cf.show_toast("Please enter the other text for Exhaust system complete/functioning under Engine Performance",1);
																																																	otherexhaustsystem.setText("");
																																																	otherexhaustsystem.requestFocus();
																																																}
																																																else
																																																{
																																																	cf.show_toast("Please select the option for Exhaust system complete/functioning under Engine Performance",1);
																																																}
																																																
																																															}
																																														}
																																														else
																																														{
																																															if(llcenterlink.getVisibility()==View.VISIBLE)
																																															{
																																																cf.show_toast("Please enter the other text for Center link under Suspension / Alignment / Wheels / Steering",1);
																																																othercenterlink.setText("");
																																																othercenterlink.requestFocus();
																																															}
																																															else
																																															{
																																																cf.show_toast("Please select the option for Center link under Suspension / Alignment / Wheels / Steering",1);
																																															}
																																															
																																														}
																																													}
																																													else
																																													{
																																														if(llidler.getVisibility()==View.VISIBLE)
																																														{
																																															cf.show_toast("Please enter the other text for Idler/Pitman arm under Suspension / Alignment / Wheels / Steering",1);
																																															otheridler.setText("");
																																															otheridler.requestFocus();
																																														}
																																														else
																																														{
																																															cf.show_toast("Please select the option for Idler/Pitman arm under Suspension / Alignment / Wheels / Steering",1);
																																														}
																																														
																																													}
																																												}
																																												else
																																												{
																																													if(llrockandpinion.getVisibility()==View.VISIBLE)
																																													{
																																														cf.show_toast("Please enter the other text for Rock and pinion under Suspension / Alignment / Wheels / Steering",1);
																																														otherrockandpinion.setText("");
																																														otherrockandpinion.requestFocus();
																																													}
																																													else
																																													{
																																														cf.show_toast("Please select the option for Rock and pinion under Suspension / Alignment / Wheels / Steering",1);
																																													}
																																													
																																												}
																																											}
																																											else
																																											{
																																												if(lllinkpins.getVisibility()==View.VISIBLE)
																																												{
																																													cf.show_toast("Please enter the other text for Link pins under Suspension / Alignment / Wheels / Steering",1);
																																													otherlinkpins.setText("");
																																													otherlinkpins.requestFocus();
																																												}
																																												else
																																												{
																																													cf.show_toast("Please select the option for Link pins under Suspension / Alignment / Wheels / Steering",1);
																																												}
																																												
																																											}
																																										}
																																										else
																																										{
																																											if(llbushings.getVisibility()==View.VISIBLE)
																																											{
																																												cf.show_toast("Please enter the other text for Bushings under Suspension / Alignment / Wheels / Steering",1);
																																												otherbushings.setText("");
																																												otherbushings.requestFocus();
																																											}
																																											else
																																											{
																																												cf.show_toast("Please select the option for Bushings under Suspension / Alignment / Wheels / Steering",1);
																																											}
																																											
																																										}
																																									}
																																									else
																																									{
																																										if(llballjoints.getVisibility()==View.VISIBLE)
																																										{
																																											cf.show_toast("Please enter the other text for Ball joints-any wear under Suspension / Alignment / Wheels / Steering",1);
																																											otherballjoints.setText("");
																																											otherballjoints.requestFocus();
																																										}
																																										else
																																										{
																																											cf.show_toast("Please select the option for Ball joints-any wear under Suspension / Alignment / Wheels / Steering",1);
																																										}
																																										
																																									}
																																								}
																																								else
																																								{
																																									if(llshocks.getVisibility()==View.VISIBLE)
																																									{
																																										cf.show_toast("Please enter the other text for Shocks/Structs under Suspension / Alignment / Wheels / Steering",1);
																																										othershocks.setText("");
																																										othershocks.requestFocus();
																																									}
																																									else
																																									{
																																										cf.show_toast("Please select the option for Shocks/Structs under Suspension / Alignment / Wheels / Steering",1);
																																									}
																																									
																																								}
																																							}
																																							else
																																							{
																																								if(llcvboots.getVisibility()==View.VISIBLE)
																																								{
																																									cf.show_toast("Please enter the other text for CV boots/joints under Suspension / Alignment / Wheels / Steering",1);
																																									othercvboots.setText("");
																																									othercvboots.requestFocus();
																																								}
																																								else
																																								{
																																									cf.show_toast("Please select the option for CV boots/joints under Suspension / Alignment / Wheels / Steering",1);
																																								}
																																								
																																							}
																																						}
																																						else
																																						{
																																							if(llujoints.getVisibility()==View.VISIBLE)
																																							{
																																								cf.show_toast("Please enter the other text for U-joints under Suspension / Alignment / Wheels / Steering",1);
																																								otherujoints.setText("");
																																								otherujoints.requestFocus();
																																							}
																																							else
																																							{
																																								cf.show_toast("Please select the option for U-joints under Suspension / Alignment / Wheels / Steering",1);
																																							}
																																							
																																						}
																																					}
																																					else
																																					{
																																						cf.show_toast("Please select the option for Any problem with alignment under Suspension / Alignment / Wheels / Steering",1);
																																					}
																																				}
																																				else
																																				{
																																					cf.show_toast("Please select the option for Any problems with suspension under Suspension / Alignment / Wheels / Steering",1);
																																				}
																																			}
																																			else
																																			{
																																				if(llcaster.getVisibility()==View.VISIBLE)
																																				{
																																					cf.show_toast("Please enter the other text for caster,camber & toe-in of wheels within manufacturer's specification under Suspension / Alignment / Wheels / Steering",1);
																																					othercaster.setText("");
																																					othercaster.requestFocus();
																																				}
																																				else
																																				{
																																					cf.show_toast("Please select the option for caster,camber & toe-in of wheels within manufacturer's specification under Suspension / Alignment / Wheels / Steering",1);
																																				}
																																				
																																			}
																																		}
																																		else
																																		{
																																			cf.show_toast("Please select the option for Condition of Valve stems under Suspension / Alignment / Wheels / Steering",1);
																																		}
																																	}
																																	else
																																	{
																																		cf.show_toast("Please select the option for Condition of tires/tread wear under Suspension / Alignment / Wheels / Steering",1);
																																	}
																																}
																																else
																																{
																																	if(llpowersteeringfluid.getVisibility()==View.VISIBLE)
																																	{
																																		cf.show_toast("Please enter the other text for Power steering fluid under Fluid Levels",1);
																																		otherpowersteeringfluid.setText("");
																																		otherpowersteeringfluid.requestFocus();
																																	}
																																	else
																																	{
																																		cf.show_toast("Please select the option for Power steering fluid under Fluid Levels",1);
																																	}
																																	
																																}
																															}
																															else
																															{
																																if(llbrakefluid.getVisibility()==View.VISIBLE)
																																{
																																	cf.show_toast("Please enter the other text for Brake fluid under Fluid Levels",1);
																																	otherbrakefluid.setText("");
																																	otherbrakefluid.requestFocus();
																																}
																																else
																																{
																																	cf.show_toast("Please select the option for Brake fluid under Fluid Levels",1);
																																}
																																
																															}
																														}
																														else
																														{
																															if(llcoolant.getVisibility()==View.VISIBLE)
																															{
																																cf.show_toast("Please enter the other text for Coolant under Fluid Levels",1);
																																othercoolant.setText("");
																																othercoolant.requestFocus();
																															}
																															else
																															{
																																cf.show_toast("Please select the option for Coolant under Fluid Levels",1);
																															}
																															
																														}
																													}
																													else
																													{
																														if(lldifferential.getVisibility()==View.VISIBLE)
																														{
																															cf.show_toast("Please enter the other text for Differential under Fluid Levels",1);
																															otherdifferential.setText("");
																															otherdifferential.requestFocus();
																														}
																														else
																														{
																															cf.show_toast("Please select the option for Differential under Fluid Levels",1);
																														}
																														
																													}
																												}
																												else
																												{
																													if(lltransmissionfluid.getVisibility()==View.VISIBLE)
																													{
																														cf.show_toast("Please enter the other text for Transmission fluid under Fluid Levels",1);
																														othertransmissionfluid.setText("");
																														othertransmissionfluid.requestFocus();
																													}
																													else
																													{
																														cf.show_toast("Please select the option for Transmission fluid under Fluid Levels",1);
																													}
																													
																												}
																											}
																											else
																											{
																												if(llengineoil.getVisibility()==View.VISIBLE)
																												{
																													cf.show_toast("Please enter the other text for Engine oil under Fluid Levels",1);
																													otherengineoil.setText("");
																													otherengineoil.requestFocus();
																												}
																												else
																												{
																													cf.show_toast("Please select the option for Engine oil under Fluid Levels",1);
																												}
																												
																											}
																										}
																										else
																										{
																											cf.show_toast("Please enter Date of last oil change(if known) under Fluid Levels",1);
																											etdateoflastoilchange.setText("");
																											etdateoflastoilchange.requestFocus();
																										}
																									}
																									else
																									{
																										cf.show_toast("Please select the option for Any fluid leaks under Fluid Levels",1);
																									}
																								}
																								else
																								{
																									cf.show_toast("Please select the option for Damage to the windshield,windows or mirrors under Body and Frame",1);
																								}
																							}
																							else
																							{
																								cf.show_toast("Please select the option for Damage to the frame under Body and Frame",1);
																							}
																						}
																						else
																						{
																							cf.show_toast("Please select the option for Appears to have been in an accident under Body and Frame",1);
																						}
																					}
																					else
																					{
																						if(llrustproblems.getVisibility()==View.VISIBLE)
																						{
																							cf.show_toast("Please enter the other text for Rust problems under Body and Frame",1);
																							otherrustproblems.setText("");
																							otherrustproblems.requestFocus();
																						}
																						else
																						{
																							cf.show_toast("Please select the option for Rust problems under Body and Frame",1);
																						}
																						
																					}
																				}
																				else
																				{
																					if(lldents.getVisibility()==View.VISIBLE)
																					{
																						cf.show_toast("Please enter the other text for Dents or dings under Body and Frame",1);
																						otherdents.setText("");
																						otherdents.requestFocus();
																					}
																					else
																					{
																						cf.show_toast("Please select the option for Dents or dings under Body and Frame",1);
																					}
																					
																				}
																			}
																			else
																			{
																				if(llpaintcondition.getVisibility()==View.VISIBLE)
																				{
																					cf.show_toast("Please enter the other text for Paint condition under Body and Frame",1);
																					otherpaintcondition.setText("");
																					otherpaintcondition.requestFocus();
																				}
																				else
																				{
																					cf.show_toast("Please select the option for Paint condition under Body and Frame",1);
																				}
																				
																			}
//																		}
//																		else
//																		{
//																			toast=new ShowToast(VehicleInspection.this, "Please select County");
//																		}
//																	}
//																	else
//																	{
//																		toast=new ShowToast(VehicleInspection.this, "Please select State");
//																	}
//																}
//																else
//																{
//																	toast=new ShowToast(VehicleInspection.this, "Please enter City");
//																	etcity.setText("");
//																	etcity.requestFocus();
//																}
//															}
//															else
//															{
//																toast=new ShowToast(VehicleInspection.this, "Zip should be 5 Characters");
//																etzip.requestFocus();
//																etzip.setText("");
//															}
//															}
//															else
//															{
//																toast=new ShowToast(VehicleInspection.this, "Please enter Zip");
//																etzip.setText("");
//																etzip.requestFocus();
//															}
//														}
//														else
//														{
//															toast=new ShowToast(VehicleInspection.this, "Please enter Inspection Address1");
//															etinspectionaddress1.setText("");
//															etinspectionaddress1.requestFocus();
//														}
													}
													else
													{
														if(llvehicletype.getVisibility()==View.VISIBLE)
														{
															cf.show_toast("Please enter the other text for Vehicle type under General Information",1);
															othervehicletype.setText("");
															othervehicletype.requestFocus();
														}
														else
														{
															cf.show_toast("Please select the option for Vehicle type under General Information",1);
														}
													}
												}
												else
												{
													if(strmodel.equals("--Select--"))
													{
														cf.show_toast("Please select Model under General Information",1);
													}
													else
													{
														if(strmodel.startsWith("Other"))
														{
															if(etothermodel.getText().toString().trim().equals(""))
															{
																cf.show_toast("Please enter "+strmodel+" under General Information",1);
																etothermodel.setText("");
																etothermodel.requestFocus();
															}
														}
													}
													cf.show_toast("Please select Model under General Information",1);
												}
											}
											else
											{
												cf.show_toast("Please select Make under General Information",1);
											}
										}
										else
										{
											cf.show_toast("Please enter Expires under General Information",1);
											etexpires.setText("");
											etexpires.requestFocus();
										}
									}
									else
									{
										cf.show_toast("Please enter License no under General Information",1);
										etlicenseno.setText("");
										etlicenseno.requestFocus();
									}
								}
								else
								{
									cf.show_toast("Please enter V.I.N under General Information",1);
									etvin.setText("");
									etvin.requestFocus();
								}
							}
							else
							{
								cf.show_toast("Please enter Vehicle no under General Information",1);
								etvehicleno.setText("");
								etvehicleno.requestFocus();
							}
						}
						else
						{
//							if(llpresentatinspection.getVisibility()==View.VISIBLE)
//							{
//								toast=new ShowToast(VehicleInspection.this, "Please enter the other text for Who was present at inspection");
//								otherpresentatinspection.setText("");
//								otherpresentatinspection.requestFocus();
//							}
//							else
//							{
//								toast=new ShowToast(VehicleInspection.this, "Please select the option for Who was present at inspection");
//							}
							
							if(!cbowner.isChecked()&&!cbagent.isChecked()&&!cbrepresentative.isChecked()&&!cbwhowaspresentother.isChecked())
							{
								cf.show_toast("Please select the option for Who was present at inspection under General Information",1);
							}
							else
							{
								if(cbwhowaspresentother.isChecked())
								{
									if(etpresentatinspectionother.getText().toString().trim().equals(""))
									{
										cf.show_toast("Please enter the other text for Who was present at inspection under General Information",1);
										otherpresentatinspection.setText("");
										otherpresentatinspection.requestFocus();
									}
								}
							}
							
						}
//					}
//					else
//					{
//						toast=new ShowToast(VehicleInspection.this, "Please enter Insurance Company");
//						etinsurancecompany.setText("");
//						etinsurancecompany.requestFocus();
//					}
				}
				else
				{
					cf.show_toast("Please enter Date of inspection under General Information",1);
					etdateofinspection.setText("");
					etdateofinspection.requestFocus();
				}
			}
			else
			{
				cf.show_toast("Please enter Last name under General Information",1);
				etlastname.setText("");
				etlastname.requestFocus();
			}
		}
		else
		{
			cf.show_toast("Please enter First name under General Information",1);
			etfirstname.setText("");
			etfirstname.requestFocus();
		}
		
	}
	
	public void Call_VehicleInformationList() {
		if (cf.isInternetOn() == true) {
//			String source = "<b><font color=#00FF33>" + "Submitting Vehicle Inspection... Please wait."
//					+ "</font></b>";
//			final ProgressDialog pd = ProgressDialog.show(VehicleInspection.this,
//					"", Html.fromHtml(source), true);
			 cf.show_ProgressDialog("Submitting Vehicle Inspection...");
			
			new Thread() {
				SoapObject chklogin;
				public void run() {
					Looper.prepare();
					try {
						chklogin = cf
								.Calling_WS_LOADVEHICLEDETAILS(UserId,"LOADCUSTOMVEHICLEDETAILS");
						System.out.println("response LOADCUSTOMVEHICLEDETAILS"
								+ chklogin);
						show_handler = 5;
						handler.sendEmptyMessage(0);
					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (NetworkErrorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (TimeoutException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 4;
						handler.sendEmptyMessage(0);

					}

				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// pd.dismiss();
						
						if (show_handler == 3) {
							cf.pd.dismiss();
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.",1);

						} else if (show_handler == 4) {
							cf.pd.dismiss();
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

						} else if (show_handler == 5) {
							show_handler = 0;
							propertycount=chklogin.getPropertyCount();
							propertycount=propertycount+1;
							System.out.println("the property count is "+propertycount);
							Vehicle_Inspection();
							
						}
					}
				};
			}.start();

		} else {
			cf.show_toast("Internet connection not available",1);

		}
//	}
	}
	
	
	private void Vehicle_Inspection()
	{
		
		
		SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd/yyyy");
        strdateofinspection = timeFormat.format(Date.parse(etdateofinspection.getText().toString()));
        strexpires= timeFormat.format(Date.parse(etexpires.getText().toString()));
        strdate= timeFormat.format(Date.parse(tvdate.getText().toString()));
        if(!etdateoflastoilchange.getText().toString().trim().equals("Not Determined"))
        {
        	strdateoflastoilchange= timeFormat.format(Date.parse(etdateoflastoilchange.getText().toString()));
        }
        else
        {
        	strdateoflastoilchange= etdateoflastoilchange.getText().toString();
        }
		
		if(llothermodel.getVisibility()==View.VISIBLE)
		{
			strmodel="Other("+etothermodel.getText().toString().trim()+")";
		}
		
		strwhowaspresentatinspection="";
		if(cbagent.isChecked())
		{
			strwhowaspresentatinspection+="Agent"+",";
		}
		if(cbrepresentative.isChecked())
		{
			strwhowaspresentatinspection+="Representative"+",";
		}
		if(cbowner.isChecked())
		{
			strwhowaspresentatinspection+="Owner"+",";
		}
		if(cbwhowaspresentother.isChecked())
		{
			strwhowaspresentatinspection+="Other("+etpresentatinspectionother.getText().toString().trim()+"),";
		}
		
		strwhowaspresentatinspection=strwhowaspresentatinspection.substring(0,strwhowaspresentatinspection.length()-1);
		
		if(llvehicletype.getVisibility()==View.VISIBLE)
		{
			strvehicletype="Other("+etvehicletypeother.getText().toString().trim()+")";
		}
		
		if(cbaddressna.isChecked())
		{
			strinspectionaddress1="N/A";
			strinspectionaddress2="N/A";
			strstate="N/A";
			strcounty="N/A";
			strcity="N/A";
			strzip="N/A";
		}
		else
		{
			strinspectionaddress1=etinspectionaddress1.getText().toString().trim();
			if(etinspectionaddress2.getText().toString().trim().equals(""))
			{
				strinspectionaddress2="N/A";
			}
			else
			{
				strinspectionaddress2=etinspectionaddress2.getText().toString().trim();
			}
			strstate=spinnerstate.getSelectedItem().toString();
			strcounty=spinnercounty.getSelectedItem().toString();
			strcity=etcity.getText().toString().trim();
			strzip=etzip.getText().toString().trim();
		}
		
		view.setDrawingCacheEnabled(true);
		Bitmap bmsignature=Bitmap.createBitmap(view.getDrawingCache());
		
		ByteArrayOutputStream bos=new ByteArrayOutputStream();
		bmsignature.compress(Bitmap.CompressFormat.PNG, 0, bos);
		final byte[] signature=bos.toByteArray();
		view.setDrawingCacheEnabled(false);
		
		db.CreateTable(4);
		Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage, null);
		
		if(cur.getCount()<1)
		{
			flag=1;
		}
		else
		{
			flag=0;
		}
		cur.close();
		
		if (cf.isInternetOn() == true) {
//			cf.show_ProgressDialog("Submitting Vehicle Inspection... Please wait.");
			new Thread() {
				public void run() {
					Looper.prepare();
					try {
						SoapObject request = new SoapObject(cf.NAMESPACE,
								"CUSTOMVEHILCEINSPECTION");
						SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
								SoapEnvelope.VER11);
						envelope.dotNet = true;
						request.addProperty("ID",propertycount);
						request.addProperty("Usertype",10);
						request.addProperty("Userid", Integer.parseInt(UserId));
						request.addProperty("Firstname", etfirstname.getText().toString().trim());
						request.addProperty("Lastname", etlastname.getText().toString().trim());
						request.addProperty("Address1", strinspectionaddress1);
						request.addProperty("Address2", strinspectionaddress2);
						request.addProperty("State", strstate);
						request.addProperty("County", strcounty);
						request.addProperty("City", strcity);
						request.addProperty("Zip", strzip);
						request.addProperty("Dateofinspection", strdateofinspection);
						request.addProperty("Insurancecompany", etinsurancecompany.getText().toString().trim());
						request.addProperty("Presentatinspection", strwhowaspresentatinspection);
						request.addProperty("Vehicleno", etvehicleno.getText().toString().trim());
						request.addProperty("VIN", etvin.getText().toString().trim());
						request.addProperty("Licenseno", etlicenseno.getText().toString().trim());
						request.addProperty("Expires", strexpires);
						request.addProperty("Modal", strmodel);
						request.addProperty("Make", strmake);
						request.addProperty("Vehicletype", strvehicletype);
						request.addProperty("Paintcondition", strpaintcondition);
						request.addProperty("Dentsordings", strdents);
						request.addProperty("Rustproblems", strrustproblems);
						request.addProperty("Accident", strappeartobeinaccident);
						request.addProperty("Damagetotheframe", strdamagetotheframe);
						request.addProperty("Damagetowindows", strdamagetowindshield);
						request.addProperty("Fluidleaks", stranyfluidleaks);
						request.addProperty("Dateoflastoilchange", strdateoflastoilchange);
						request.addProperty("Engineoil", strengineoil);
						request.addProperty("TransmissionFluid", strtransmissionfluid);
						request.addProperty("Differential", strdifferential);
						request.addProperty("Coolant", strcoolant);
						request.addProperty("Brakefluid", strbrakefluid);
						request.addProperty("Powersteeringfluid", strpowersteeringfluid);
						request.addProperty("TiresorTreadwear", strconditionoftires);
						request.addProperty("Valvestems", strconditionofvalvestems);
						request.addProperty("Caster_camber_toe", strcaster);
						request.addProperty("Suspension", stranyproblemwithsuspension);
						request.addProperty("Alignment", stranyproblemwithalignment);
						request.addProperty("UJoints", strujoints);
						request.addProperty("CVBootsjoints", strcvboots);
						request.addProperty("Shocks_struts", strshocks);
						request.addProperty("Balljoints_anywear", strballjoints);
						request.addProperty("Bushings", strbushings);
						request.addProperty("Linkpins", strlinkpins);
						request.addProperty("Rocksandpnion", strrockandpinion);
						request.addProperty("Idlerpitmanarm", stridler);
						request.addProperty("Centerlink", strcenterlink);
						request.addProperty("Exhousetsystem", strexhaustsystemcomplete);
						request.addProperty("Emissionscontrol", stremissioncontrolintact);
						request.addProperty("Hosesappearintact", strdoallbelts);
						request.addProperty("Conditionofsparkplugs", strconditionofsparkplugs);
						request.addProperty("Breaksgrab_Lockup", strdothebrakesgrab);
						request.addProperty("Carshudderwhenbraking", strdoescarshudder);
						request.addProperty("Brakelinings", strarebrakelinings);
						request.addProperty("Brakelights", strbrakelights);
						request.addProperty("Turnsignals", strturnsignals);
						request.addProperty("Conditionofbrakepads", strconditionofbrakepads);
						request.addProperty("Hydroylicsystems", stranyproblemswithhudraulicsystem);
						request.addProperty("Parkingbrakework", strdoesparkingbrake);
						request.addProperty("Brakepedalpressure", strisbrakepedalpressureokay);
						request.addProperty("Mastercylinder", strmastercylinder);
						request.addProperty("Drumsanddiscus", strdrumsanddiscs);
						request.addProperty("Wheelcylinders", strwheelcylinder);
						request.addProperty("Calipers_discbrake", strcalipters);
						request.addProperty("Instruments_Gauges", strinstruments);
						request.addProperty("Interiorlights", strinteriorlights);
						request.addProperty("Headlights", strheadlights);
						request.addProperty("Toillights", strtaillights);
						request.addProperty("Backuplights", strbackuplights);
						request.addProperty("Emergencylights", stremergencylights);
						request.addProperty("Systemfailurewaringlight", strsystemfailure);
						request.addProperty("Airconditioningsystem", strairconditioningsystem);
						request.addProperty("Heatingsystem", strheatingsystem);
						request.addProperty("Conditionofbattery", strconditionofbattery);
						request.addProperty("Doorlocks_windows_operational", strdoorlocks);
						request.addProperty("Poweroptionwork", strdoallpoweroptionswork);
						request.addProperty("Speadofvehicle", stranydelaybetweenenginespeed);
						request.addProperty("Coverpagelogo", bytecoverpagelogo);
						request.addProperty("Coverpagename", (propertycount)+"Vehicle_Coverpagelogo"+UserId+".jpg");
						request.addProperty("Automatictransmission", strautomatictransmission);
						if(GraphicsView1.value==1)
						{
							request.addProperty("Signature", signature);
						}
						else
						{
							request.addProperty("Signature", "N/A");
						}
						request.addProperty("Agentname", etname.getText().toString().trim());
						request.addProperty("date", strdate);
						request.addProperty("Flag", flag);						
						
						request.addProperty("Createddate", curdate);
				        request.addProperty("Createdtime", curtime);
				        
						envelope.setOutputSoapObject(request);
						marshal.register(envelope);

//						System.out.println("Agent_Realtor_Vehicledetails request is " + request);
						Log.v("Vehicle Inspection", "CUSTOMVEHILCEINSPECTION request is " + request);
						HttpTransportSE androidHttpTransport = new HttpTransportSE(
								cf.URL);
						System.out.println("Before http call");
						androidHttpTransport.call(cf.NAMESPACE
								+ "CUSTOMVEHILCEINSPECTION", envelope);
						order_result = envelope.getResponse().toString();
						System.out.println("CUSTOMVEHILCEINSPECTION result is"
								+ order_result);
//						Log.v("VehicleInspection", "Agent_Realtor_Vehicledetails result is"
//								+ order_result);

						show_handler = 5;
						handler.sendEmptyMessage(0);

						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					} catch (XmlPullParserException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						show_handler = 3;
						handler.sendEmptyMessage(0);
					}
				}

				private Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						cf.pd.dismiss();
						if (show_handler == 3) {
							show_handler = 0;
							cf.show_toast("There is a problem on your Network. Please try again later with better Network.",1);

						} else if (show_handler == 4) {
							show_handler = 0;
							cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

						} else if (show_handler == 5) {
							show_handler = 0;
							
							Call_VehicleImages(order_result);
							

						}
					}
				};
			}.start();
		} else {
			cf.show_toast("Internet connection not available",1);
		}
	
	}
	
	private void Call_VehicleImages(String result)
	{
		if(flag==1&&result.toLowerCase().equals("true"))
		{
//			Call_AgentInformationList();
//			toast = new ShowToast(VehicleInspection.this,
//					"Vehicle Inspection submitted successfully");
			
			Toast toast = Toast.makeText(VehicleInspection.this, "Vehicle Inspection submitted successfully",
					Toast.LENGTH_LONG);
			LayoutInflater inflater = (LayoutInflater) VehicleInspection.this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View layout = inflater.inflate(R.layout.toast, null);

			TextView tv = (TextView) layout.findViewById(R.id.text);
			toast.setGravity(0, Gravity.CENTER, Gravity.CENTER + 50);
			tv.setText("Vehicle Inspection submitted successfully");
			toast.setView(layout);
			toast.show();
			
//			Intent intent=new Intent(AgentInspection2.this,AgentInspection2.class);
//			startActivity(intent);
//			finish();
			Intent intent = new Intent(VehicleInspection.this,OnlineList.class);
		    overridePendingTransition(0, 0);
		    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		    finish();

		    overridePendingTransition(0, 0);
		    startActivity(intent);
		}
		else if((flag==1||flag==0)&&result.toLowerCase().equals("false"))
		{
			cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);
		}
		else if(flag==0&&result.toLowerCase().equals("true"))
		{
//			cf.CreateTable(20);
//			cf.db.execSQL("insert into "+cf.AgentInspection_Pdf+"(filename) values('"+cf.encode(image_result)+"')");
			Save_VehicleInspection_Image1();
//			Call_Dynamic_Pdf_Display();
		}
		
	}
	
	private void Save_VehicleInspection_Image1()
	{
		db.CreateTable(4);
		Cursor cur=db.vi_db.rawQuery("select * from "+db.AddAImage, null);
		cur.moveToFirst();
		int cnt=cur.getCount();
		elevation_name=new String[cnt];
		caption_name=new String[cnt];
		file_name=new String[cnt];
		if(cnt>=1)
		{
			int i=0;
			do
			{
				final String elevation=cf.decode(cur.getString(cur.getColumnIndex("elevation")));
				final String caption=cf.decode(cur.getString(cur.getColumnIndex("caption")));
				String filepath=cf.decode(cur.getString(cur.getColumnIndex("filepath")));
				
				elevation_name[i]=elevation;
				caption_name[i]=caption;
				file_name[i]=filepath;
				
				i++;
				
			}while(cur.moveToNext());
		}
		cur.close();
		if(elevation_name.length>=1)
		{
			Save_VehicleInspection_Image();
		}
	}
	
	private void Save_VehicleInspection_Image()
	{
			if (cf.isInternetOn() == true) {
				cf.show_ProgressDialog("Submitting Vehicle Inspection...");
				new Thread() {
					public void run() {
						Looper.prepare();
						try {
							
							for(i=0;i<elevation_name.length;i++)
							{
								String[] filenamesplit = file_name[i]
										.split("/");
								final String filename = filenamesplit[filenamesplit.length - 1];
								System.out
										.println("The File Name is "
												+ filename);
								
								Bitmap bitmap = cf.ShrinkBitmap(file_name[i], 400, 400);

								System.out.println(" bimap "+bitmap);
								marshal = new MarshalBase64();
								ByteArrayOutputStream out = new ByteArrayOutputStream();
								bitmap.compress(CompressFormat.PNG, 100, out);
								final byte[] byteimage = out.toByteArray();
								if(i==elevation_name.length-1)
								{
									flag=1;
								}
								else
								{
									flag=0;
								}
							
							SoapObject request = new SoapObject(cf.NAMESPACE,
									"CUSTOMVEHICLEIMAGEUPLOAD");
							SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
									SoapEnvelope.VER11);
							envelope.dotNet = true;
							request.addProperty("AgentInfoPk",propertycount);
							request.addProperty("Usertype", 10);
							request.addProperty("Userid", Integer.parseInt(UserId));
							request.addProperty("ImageOrder", i);
							request.addProperty("ElevationType", elevation_name[i]);
							request.addProperty("Caption", caption_name[i]);
							request.addProperty("Image", byteimage);
//							request.addProperty("Imagename", filename);
							request.addProperty("Imagename", "AgVe_"+UserId+"i"+(propertycount)+i+".jpg");
							request.addProperty("Flag", flag);
														
							
							envelope.setOutputSoapObject(request);
							marshal.register(envelope);

							System.out.println("CUSTOMVEHICLEIMAGEUPLOAD request is " + request);
							HttpTransportSE androidHttpTransport = new HttpTransportSE(
									cf.URL);
							System.out.println("Before http call");
							androidHttpTransport.call(cf.NAMESPACE
									+ "CUSTOMVEHICLEIMAGEUPLOAD", envelope);
							image_result = envelope.getResponse().toString();
							System.out.println("CUSTOMVEHICLEIMAGEUPLOAD result is"
									+ image_result);
							}
							
							show_handler = 5;
							handler.sendEmptyMessage(0);

							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);
						} catch (XmlPullParserException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							show_handler = 3;
							handler.sendEmptyMessage(0);
						}
					}

					private Handler handler = new Handler() {
						@Override
						public void handleMessage(Message msg) {
							cf.pd.dismiss();
							if (show_handler == 3) {
								show_handler = 0;
								cf.show_toast("There is a problem on your Network. Please try again later with better Network.",1);

							} else if (show_handler == 4) {
								show_handler = 0;
								cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);

							} else if (show_handler == 5) {
								show_handler = 0;
								
								if(flag==1&&image_result.toLowerCase().equals("true"))
								{
									
									Toast toast = Toast.makeText(VehicleInspection.this, "Vehicle Inspection submitted successfully",
											Toast.LENGTH_LONG);
									LayoutInflater inflater = (LayoutInflater) VehicleInspection.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
									View layout = inflater.inflate(R.layout.toast, null);

									TextView tv = (TextView) layout.findViewById(R.id.text);
									toast.setGravity(0, Gravity.CENTER, Gravity.CENTER + 50);
									tv.setText("Vehicle Inspection submitted successfully");
									toast.setView(layout);
									toast.show();
									
									Intent intent = new Intent(VehicleInspection.this,OnlineList.class);
								    overridePendingTransition(0, 0);
								    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
								    finish();

								    overridePendingTransition(0, 0);
								    startActivity(intent);
								}
								else if(flag==1&&image_result.toLowerCase().equals("false"))
								{
									cf.show_toast("There is a problem on your application. Please contact Paperless administrator.",1);
								}

							}
						}
					};
				}.start();
			} else {
				cf.show_toast("Internet connection not available",1);
			}
		
	}
		
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		
		db.CreateTable(4);
		db.vi_db.execSQL("delete from " + db.AddAImage);
		
		Intent intent = new Intent(VehicleInspection.this, HomeScreen.class);
		startActivity(intent);
		finish();
	}
}
